import 'module-alias/register';
import {logger} from '@src/config/logger.config';
import {TelegramService} from "@src/services/TelegramService";
import {AttendanceService} from "@src/services/AttendanceService";
import {DbService} from "@src/services/DbService";
import {Authenticator} from "@src/services/AuthService";
import {NotificationService} from "@src/services/NotificationService";
import {WebService} from "@src/services/WebService";

const main = async () => {
  try {
    await WebService.start();
    await DbService.start();
    await Authenticator.register();
    NotificationService.start();
    if (process.env.NODE_ENV === "production" || process.env.NODE_ENV === "deployment") {
      await TelegramService.register();
      AttendanceService.register();
    }
  } catch (e) {
    logger.error(e.stack);
  }
}
main().then(_ => {
});