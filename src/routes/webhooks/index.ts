import express from "express";
import {handleIncomingEmail, handleSentEmail} from "@src/controllers/mailgun.controller";
import {handlePushEvent} from "@src/controllers/gitlab.controller";
import {} from "@src/controllers/gitlab.controller";

export const webhooksRouter = express.Router();
webhooksRouter.post('/email', handleIncomingEmail);
webhooksRouter.post('/email/sent', handleSentEmail);
webhooksRouter.post('/gitlab', handlePushEvent);
