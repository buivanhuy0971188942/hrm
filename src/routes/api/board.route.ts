import { addTaskValidation } from '@src/validations/task.validation';
import { updateBoardValidation, updateBoardIndexValidation } from '@src/validations/board.validation';
import { validateMiddleware } from '@src/middlewares/validate.middleware';
import { Router } from "express";
import { updateBoardIndex,updateBoard, deleteBoard } from "@src/controllers/board.controller";
import { addTask } from "@src/controllers/project-task.controller";

import 'express-async-errors';

export const boardRouter = Router();
boardRouter.post('/index', validateMiddleware(updateBoardIndexValidation), updateBoardIndex);
boardRouter.post('/:boardId/tasks', validateMiddleware(addTaskValidation), addTask);
boardRouter.post('/:boardId', validateMiddleware(updateBoardValidation), updateBoard);
boardRouter.delete('/:boardId', deleteBoard);
