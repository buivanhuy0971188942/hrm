import {Router} from "express";

import {validateMiddleware} from '@src/middlewares/validate.middleware';
import {updateAccountValidation, takeAttendanceValidation} from '@src/validations/account.validation';
import {addFeedbackValidation} from '@src/validations/feedback.validation';

import {
  getAccounts,
  searchAccounts,
  getAccount,
  updateAccount,
  deleteAccount, uploadAvatar, restoreAccount
} from "@src/controllers/account.controller";
import {takeAttendance, getAttendanceRecords} from "@src/controllers/attendance.controller";
import {getFeedbacks, addFeedback} from "@src/controllers/feedback.controller";
import {Authenticator} from "@src/services/AuthService";

import 'express-async-errors';
import {uploadMiddleware} from "@src/middlewares/general-file.middleware";
import {authMiddleware} from "@src/middlewares/auth.middleware";

export const accountsRouter = Router();

accountsRouter.use(authMiddleware('jwt'))
accountsRouter.get('/', Authenticator.need({roles: ['admin', 'hrm']}), getAccounts);
accountsRouter.get('/search', searchAccounts);
accountsRouter.get('/:accountId/attendance', Authenticator.need({roles: ['admin', 'hrm']}), getAttendanceRecords);
accountsRouter.get('/:accountId/feedbacks', Authenticator.need({roles: ['admin', 'hrm']}), getFeedbacks);
accountsRouter.get('/:accountId', Authenticator.need({roles: ['admin', 'hrm']}), getAccount);
accountsRouter.post('/:accountId', Authenticator.need({roles: ['admin', 'hrm']}), validateMiddleware(updateAccountValidation), updateAccount);
accountsRouter.post('/:accountId/avatar', Authenticator.need({roles: ['admin', 'hrm']}), uploadMiddleware(["png", "jpg", "jpeg"]), uploadAvatar);
accountsRouter.post('/:accountId/attendance', Authenticator.need({roles: ['admin', 'hrm']}), validateMiddleware(takeAttendanceValidation), takeAttendance);
accountsRouter.post('/:accountId/feedbacks', Authenticator.need({roles: ['admin', 'hrm']}), validateMiddleware(addFeedbackValidation), addFeedback);
accountsRouter.post('/:accountId/restore', Authenticator.need({roles: ['admin', 'hrm']}), restoreAccount);
accountsRouter.delete('/:accountId', Authenticator.need({roles: ['admin', 'hrm']}), deleteAccount);
