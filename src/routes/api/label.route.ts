import { addLabelValidation, updateLabelValidation } from '@src/validations/label.validation';
import { validateMiddleware } from '@src/middlewares/validate.middleware';
import { Router } from "express";

import {
  addLabel,
  getLabel,
  getLabels,
  updateLabel,
  deleteLabel
} from "@src/controllers/label.controller";
import 'express-async-errors';

export const labelRouter = Router();
labelRouter.get('/:labelId', getLabel);
labelRouter.post('/:labelId', validateMiddleware(updateLabelValidation), updateLabel);
labelRouter.delete('/:labelId', deleteLabel);
