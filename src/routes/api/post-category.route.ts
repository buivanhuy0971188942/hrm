import {updatePostCategoryValidation, addPostCategoryValidation} from '@src/validations/post-category.validation';
import {validateMiddleware} from '@src/middlewares/validate.middleware';
import {Router} from "express";
import {
  addPostCategory,
  getPostCategories,
  getPostCategory,
  updatePostCategory,
  deletePostCategory
} from "@src/controllers/post-category.controller";
import 'express-async-errors';
import {authMiddleware} from "@src/middlewares/auth.middleware";

export const postCategoryRouter = Router();
postCategoryRouter.get('/', getPostCategories);
postCategoryRouter.get('/:categoryId', getPostCategory);
postCategoryRouter.use(authMiddleware('jwt'));
postCategoryRouter.post('/', validateMiddleware(addPostCategoryValidation), addPostCategory);
postCategoryRouter.post('/:categoryId', validateMiddleware(updatePostCategoryValidation), updatePostCategory);
postCategoryRouter.delete('/:categoryId', deletePostCategory);