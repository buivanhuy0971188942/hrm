import {
  addProjectValidation,
  updateProjectValidation,
  addMemberValidation,
  updateMemberValidation,
  deleteMemberValidation,
  linkProjectToGitLabValidation, disconnectProjectFromGitLabValidation
} from '@src/validations/project.validation';
import {addBoardValidation} from '@src/validations/board.validation';
import {addIssueValidation} from "@src/validations/issue.validation";
import {addLabelValidation} from '@src/validations/label.validation';

import {validateMiddleware} from '@src/middlewares/validate.middleware';
import {Router} from "express";
import {
  getProjects, addProject,
  getProject, deleteProject,
  updateProject, addMembers,
  getProjectMembers, updateMembers,
  deleteMembers, getProjectResources,
  linkProjectToGitLab, disconnectFromGitLab
} from "@src/controllers/project.controller";
import {addIssue, getIssues} from "@src/controllers/issue.controller";
import {addBoard, getBoards,} from "@src/controllers/board.controller";
import {addLabel, getLabels,} from "@src/controllers/label.controller";
import {Authenticator} from "@src/services/AuthService";
import {uploadMiddleware} from "@src/middlewares/general-file.middleware";
import {fileTypes} from "@src/utils/google-storage";
import {getTasks} from "@src/controllers/project-task.controller";
import {upload} from "@google-cloud/storage/build/src/gcs-resumable-upload";

export const projectsRouter = Router();
projectsRouter.get('/',
  Authenticator.need({roles: ["admin"]}, {
    allowOwnResource: ["creator", "ref"],
    refDocFilter: {model: "ProjectMemberModel", returnField: "project", localField: "member"}
  }),
  getProjects
);

projectsRouter.get('/:projectId',
  Authenticator.need({roles: ["admin"]}, {
    allowOwnResource: ["creator", "ref"],
    refDocFilter: {model: "ProjectMemberModel", returnField: "project", localField: "member"}
  }),
  getProject
);

projectsRouter.get('/:projectId/boards', getBoards);
projectsRouter.get('/:projectId/members', getProjectMembers);
projectsRouter.get('/:projectId/issues', getIssues);
projectsRouter.get('/:projectId/labels', getLabels);
projectsRouter.get('/:projectId/tasks', getTasks);

projectsRouter.get('/:projectId/resources',
  Authenticator.need({roles: ["admin"]}, {
    allowOwnResource: ["creator", "ref"],
    refDocFilter: {model: "ProjectMemberModel", returnField: "project", localField: "member"}
  }),
  getProjectResources
);
projectsRouter.post('/',
  uploadMiddleware(fileTypes["image"]),
  validateMiddleware(addProjectValidation),
  addProject
);

projectsRouter.post('/:projectId',
  Authenticator.need({roles: ["admin"]}, {
    allowOwnResource: ["creator"],
  }),
  uploadMiddleware(["png", "jpeg", "jpg"]),
  validateMiddleware(updateProjectValidation),
  updateProject
);

projectsRouter.post('/:projectId/members',
  Authenticator.need({roles: ["admin"]}, {
    allowOwnResource: ["creator"],
  }),
  validateMiddleware(addMemberValidation),
  addMembers
);

projectsRouter.post('/:projectId/members/edit',
  Authenticator.need({roles: ["admin"]}, {
    allowOwnResource: ["creator"],
  }),
  validateMiddleware(updateMemberValidation),
  updateMembers
);

projectsRouter.post('/:projectId/boards', validateMiddleware(addBoardValidation), addBoard);
projectsRouter.post('/:projectId/issues', validateMiddleware(addIssueValidation), addIssue);
projectsRouter.post('/:projectId/labels', validateMiddleware(addLabelValidation), addLabel);

projectsRouter.post('/:projectId/gitlab',
  Authenticator.need({roles: ["admin"]}, {
    allowOwnResource: ["creator"],
  }),
  validateMiddleware(linkProjectToGitLabValidation),
  linkProjectToGitLab
);

projectsRouter.post('/:projectId/gitlab/disconnect',
  Authenticator.need({roles: ["admin"]}, {
    allowOwnResource: ["creator"],
  }),
  validateMiddleware(disconnectProjectFromGitLabValidation),
  disconnectFromGitLab
);

projectsRouter.delete('/:projectId',
  Authenticator.need({roles: ["admin"]}, {
    allowOwnResource: ["creator"],
  }),
  deleteProject
);

projectsRouter.delete('/:projectId/members', validateMiddleware(deleteMemberValidation), deleteMembers);
