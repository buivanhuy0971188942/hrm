import {updateIssueValidation} from '@src/validations/issue.validation';
import {validateMiddleware} from '@src/middlewares/validate.middleware';
import {Router} from "express";
import {
  getIssue,
  updateIssue,
} from "@src/controllers/issue.controller";
import 'express-async-errors';

export const issueRouter = Router();
issueRouter.get('/:issueId', getIssue);
issueRouter.post('/:issueId', validateMiddleware(updateIssueValidation), updateIssue);