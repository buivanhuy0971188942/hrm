import {updateProductValidation, addProductValidation} from '@src/validations/product.validation';
import {validateMiddleware} from '@src/middlewares/validate.middleware';
import {Router} from "express";
import {
  addProduct, getProducts,
  getProduct, updateProduct,
  deleteProduct
} from "@src/controllers/product.controller";
import 'express-async-errors';
import {uploadMiddleware} from "@src/middlewares/general-file.middleware";
import {authMiddleware} from "@src/middlewares/auth.middleware";
import {fileTypes} from "@src/utils/google-storage";

export const productRouter = Router();
productRouter.get('/', getProducts);
productRouter.get('/:productId', getProduct);
productRouter.use(authMiddleware('jwt'));
productRouter.post('/', uploadMiddleware(fileTypes.image), validateMiddleware(addProductValidation), addProduct);
productRouter.post('/:productId', uploadMiddleware(fileTypes.image), validateMiddleware(updateProductValidation), updateProduct);
productRouter.delete('/:productId', deleteProduct);