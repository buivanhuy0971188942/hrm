import {Router} from "express";

import {getEntranceRecords, getSelfEntranceRecords} from "@src/controllers/entrance.controller";
import {Authenticator} from "@src/services/AuthService";

import 'express-async-errors';

export const entranceRouter = Router();

entranceRouter.get('/history', getSelfEntranceRecords);

entranceRouter.get('/',
  Authenticator.need({roles: ["admin", "hrm"]}, {
    allowOwnResource: ["creator"],
    creatorDocFilter: {field: "cardId", accountField: "cardId"}
  }),
  getEntranceRecords
);
