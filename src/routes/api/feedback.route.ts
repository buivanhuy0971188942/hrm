import {updateFeedbackValidation} from '@src/validations/feedback.validation';
import {validateMiddleware} from '@src/middlewares/validate.middleware';
import {Router} from "express";
import {
  getFeedback, deleteFeedback,
  updateFeedback,
} from "@src/controllers/feedback.controller";
import {Authenticator} from "@src/services/AuthService";

import 'express-async-errors';

export const feedbackRouter = Router();
feedbackRouter.get('/:feedbackId', getFeedback);
feedbackRouter.post('/:feedbackId', Authenticator.need({roles: ['admin', 'hrm']}), validateMiddleware(updateFeedbackValidation), updateFeedback);
feedbackRouter.delete('/:feedbackId', Authenticator.need({roles: ['admin', 'hrm']}), deleteFeedback);