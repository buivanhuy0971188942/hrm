import {
  updateGeneralTaskValidation,
  addGeneralTaskValidation,
  deleteAttachmentValidation, deleteTaskMemberValidation
} from '@src/validations/general-task.validation';
import {validateMiddleware} from '@src/middlewares/validate.middleware';
import {Router} from "express";
import {
  addGeneralTask,
  getGeneralTasks,
  getGeneralTask,
  updateGeneralTask,
  deleteGeneralTask,
  deleteTaskMembers,
  deleteAttachment,
  addSubTask,
  updateSubTask,
  getSubTasks,
  deleteSubTask,
  getTrackingTask
} from "@src/controllers/general-task.controller";
import 'express-async-errors';
import {uploadMiddleware} from "@src/middlewares/general-file.middleware";
import {fileTypes} from "@src/utils/google-storage";
import {Authenticator} from "@src/services/AuthService";

export const generalTaskRouter = Router();
generalTaskRouter.get('/',
  // Authenticator.need({roles: ["admin", "hrm"]}, {
  //   allowOwnResource: ["creator", "ref"],
  //   refDocFilter: {model: "TaskMemberModel", returnField: "task", localField: "member"}
  // }),
  getGeneralTasks
);

generalTaskRouter.get('/:taskId',
  // Authenticator.need({roles: ["admin", "hrm"]}, {
  //   allowOwnResource: ["creator", "ref"],
  //   refDocFilter: {model: "TaskMemberModel", returnField: "task", localField: "member"}
  // }),
  getGeneralTask
);

generalTaskRouter.post('/',
  uploadMiddleware([...fileTypes["doc"], ...fileTypes["image"]]),
  // validateMiddleware(addGeneralTaskValidation),
  addGeneralTask
);

generalTaskRouter.post('/:taskId',
  // Authenticator.need({roles: ["admin", "hrm"]}, {
  //   allowOwnResource: ["creator", "ref"],
  //   refDocFilter: {model: "TaskMemberModel", returnField: "task", localField: "member"}
  // }),
  uploadMiddleware([...fileTypes["doc"], ...fileTypes["image"]]),
  // validateMiddleware(updateGeneralTaskValidation),
  updateGeneralTask
);

generalTaskRouter.delete('/:taskId',
  // Authenticator.need({roles: ["admin", "hrm"]}, {
  //   allowOwnResource: ["creator"],
  // }),
  deleteGeneralTask
);

generalTaskRouter.delete('/:taskId/attachment',
  // Authenticator.need({roles: ["admin", "hrm"]}, {
  //   allowOwnResource: ["creator"],
  // }),
  validateMiddleware(deleteAttachmentValidation),
  deleteAttachment
);

generalTaskRouter.delete('/:taskId/member',
  // Authenticator.need({roles: ["admin", "hrm"]}, {
  //   allowOwnResource: ["creator"],
  // }),
  validateMiddleware(deleteTaskMemberValidation),
  deleteTaskMembers
);

generalTaskRouter.get('/:taskId/sub-tasks', getSubTasks)
generalTaskRouter.post('/:taskId/sub-task', addSubTask)
generalTaskRouter.put('/:taskId/sub-task/:subTaskId', updateSubTask)
generalTaskRouter.delete('/:taskId/sub-task/:subTaskId', deleteSubTask)

generalTaskRouter.get('/:taskId/tracking-task', getTrackingTask)
