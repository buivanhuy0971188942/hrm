import {updateTaskValidation, updateTaskIndexValidation} from '@src/validations/task.validation';
import {validateMiddleware} from '@src/middlewares/validate.middleware';
import {Router} from "express";
import {
  getTask,
  updateTask,
  updateTaskIndex,
  deleteTask, getTasks, getTaskCalendar
} from "@src/controllers/project-task.controller";
import 'express-async-errors';

export const taskRouter = Router();
taskRouter.get('/', getTasks);
taskRouter.get('/calendar', getTaskCalendar);
taskRouter.get('/:taskId', getTask);
taskRouter.post('/index', validateMiddleware(updateTaskIndexValidation), updateTaskIndex);
taskRouter.post('/:taskId', validateMiddleware(updateTaskValidation), updateTask);
taskRouter.delete('/:taskId', deleteTask);