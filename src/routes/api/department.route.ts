import {addDepartmentValidation, updateDepartmentValidation} from '@src/validations/department.validation';
import {validateMiddleware} from '@src/middlewares/validate.middleware';
import {Router} from "express";
import {
  getDepartments, addDepartment,
  getDepartment, deleteDepartment,
  updateDepartment,
} from "@src/controllers/department.controller";
import {Authenticator} from "@src/services/AuthService";
import {uploadMiddleware} from "@src/middlewares/general-file.middleware";
import {fileTypes} from "@src/utils/google-storage";

export const departmentsRouter = Router();
departmentsRouter.get('/', getDepartments);
departmentsRouter.post('/',
  Authenticator.need({roles: ['admin', 'hrm']}),
  uploadMiddleware(fileTypes["image"]),
  validateMiddleware(addDepartmentValidation),
  addDepartment
);

departmentsRouter.get('/:departmentId', getDepartment);

departmentsRouter.post('/:departmentId',
  Authenticator.need({roles: ['admin', 'hrm']}),
  uploadMiddleware(fileTypes["image"]),
  validateMiddleware(updateDepartmentValidation),
  updateDepartment
);

departmentsRouter.delete('/:departmentId', Authenticator.need({roles: ['admin', 'hrm']}), deleteDepartment);