import {addEventValidation, updateEventValidation} from '@src/validations/calendar-event.validation';
import {validateMiddleware} from '@src/middlewares/validate.middleware';
import {Router} from "express";
import {
  getCalendarEvents, addCalendarEvent,
  getCalendarEvent, deleteCalendarEvent,
  updateCalendarEvent, getCalendarEventPrivate,
} from "@src/controllers/calendar-event.controller";
import {Authenticator} from "@src/services/AuthService";

export const calendarEventsRouter = Router();

calendarEventsRouter.get('/',
  getCalendarEvents
);

calendarEventsRouter.get('/private/:eventId',
  getCalendarEventPrivate
);

calendarEventsRouter.get('/:eventId',
  getCalendarEvent
);

calendarEventsRouter.post('/',
  // Authenticator.need({roles: ['admin', 'hrm']}),
  validateMiddleware(addEventValidation),
  addCalendarEvent
);

calendarEventsRouter.post('/:eventId',
  // Authenticator.need({roles: ['admin', 'hrm']}),
  validateMiddleware(updateEventValidation),
  updateCalendarEvent
);

calendarEventsRouter.delete('/:eventId',
  // Authenticator.need({roles: ['admin', 'hrm']}),
  deleteCalendarEvent
);