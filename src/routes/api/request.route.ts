import {validateMiddleware} from '@src/middlewares/validate.middleware';
import {Router} from "express";
import {
  getRequests, addRequest,
  getRequest, updateRequest,
  deleteRequest, responseToRequest, getSentRequests,
} from "@src/controllers/request.controller";
import {
  addRequestValidation, getRequestByPlaceHolder,
  responseToRequestValidation,
  updateRequestValidation
} from '@src/validations/request.validation';
import {Authenticator} from "@src/services/AuthService";
import {uploadMiddleware} from "@src/middlewares/general-file.middleware";
import {fileTypes} from "@src/utils/google-storage";

export const requestsRouter = Router();

requestsRouter.get('/', Authenticator.need({roles: ['admin', 'hrm', 'manager']}), getRequests);
requestsRouter.get('/sent', getSentRequests);
requestsRouter.get('/:requestId', getRequest);

requestsRouter.post('/',
  uploadMiddleware(fileTypes["image"]),
  validateMiddleware(addRequestValidation),
  addRequest
);

requestsRouter.post('/:requestId',
  uploadMiddleware(fileTypes["image"]),
  validateMiddleware(updateRequestValidation),
  updateRequest
);

requestsRouter.post('/:requestId/response', validateMiddleware(responseToRequestValidation), responseToRequest);

requestsRouter.delete('/:requestId',
  Authenticator.need({roles: ['admin']}, {
    allowOwnResource: ["creator"],
    creatorDocFilter: {
      field: "sender",
      accountField: "_id"
    }
  }),
  deleteRequest
);
