import {validateMiddleware} from '@src/middlewares/validate.middleware';
import {Router} from "express";
import {
  getNotification,
  getNotifications,
  sendNotification,
  deleteNotification,
} from "@src/controllers/notification.controller";
import {sendNotificationValidation, addDeviceTokenValidation} from '@src/validations/notification.validation';
import {Authenticator} from "@src/services/AuthService";
import {addDeviceToken} from "@src/controllers/device-token.controller";
import {authMiddleware} from "@src/middlewares/auth.middleware";

export const notificationsRouter = Router();

notificationsRouter.post('/',
  Authenticator.need({roles: ['admin', 'manager', 'hrm']}),
  validateMiddleware(sendNotificationValidation),
  sendNotification
);

notificationsRouter.use(
  Authenticator.need({roles: ['admin', 'manager', 'hrm']}, {
    allowOwnResource: ["creator"],
    creatorDocFilter: {field: "receiver", accountField: "_id"}
  })
);

notificationsRouter.get('/', getNotifications);
notificationsRouter.get('/:notificationId', getNotification);
notificationsRouter.post('/device_token', validateMiddleware(addDeviceTokenValidation), addDeviceToken);
notificationsRouter.delete('/:notificationId', deleteNotification);
