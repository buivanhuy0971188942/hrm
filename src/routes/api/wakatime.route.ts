import {Router} from "express";
import {verifyCode} from "@src/controllers/telegram.controller";
import {wakaConnect, wakaDisconnect, wakaGetStat, wakaRefresh} from "@src/controllers/waka.controller";

export const wakaRouter = Router();
wakaRouter.post('/connect', wakaConnect);
wakaRouter.post('/disconnect', wakaDisconnect);
wakaRouter.get('/ranking', wakaGetStat);
wakaRouter.post('/refresh', wakaRefresh);
