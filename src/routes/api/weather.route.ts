import express from "express";
import axios from "axios";

export const weatherRouter = express.Router();

weatherRouter.get('/forecast', (req, res) => {
  axios('http://api.weatherapi.com/v1/forecast.json', {
    method: 'GET',
    params: {
      key: '851d223c6d1b4287b5035440211112',
      q: req.body.city || req.query.city,
      days: req.body.days || req.query.days,
      aqi: 'yes',
      alerts: 'yes',
    }}
  ).then(response => {
    res.json(response.data);
  }).catch(err => {
    res.json(err);
  });

});
