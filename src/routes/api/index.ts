import express from "express";
import {authMiddleware} from "@src/middlewares/auth.middleware";
import {randomQuote} from "@src/controllers/quote.controller";
import {Authenticator} from "@src/services/AuthService";

import {emailRouter} from './email.route';
import {feedbackRouter} from './feedback.route';
import {criteriaRouter} from './criteria.route';
import {taskRouter} from './task.route';
import {labelRouter} from './label.route';
import {boardRouter} from './board.route';
import {salaryRouter} from './salary.route';
import {reportsRouter} from './report.route';
import {jobInfoRouter} from './job-info.route';
import {departmentsRouter} from './department.route';
import {accountsRouter} from './accounts.route';
import {projectsRouter} from './projects.route';
import {groupsRouter} from './groups.route';
import {infoRouter} from './info.route';
import {authRouter} from './auth.route';
import {requestsRouter} from './request.route';
import {regulationRouter} from './regulation.route';
import {telegramRouter} from './telegram.route';
import {weatherRouter} from "@src/routes/api/weather.route";
import {entranceRouter} from "@src/routes/api/entrance.route";
import {gitlabRouter} from "@src/routes/api/gitlab.route";
import {rewardsRouter} from "@src/routes/api/rewards.route";
import {issueRouter} from "@src/routes/api/issue.route";
import {generalTaskRouter} from "@src/routes/api/general-task.route";
import {documentRouter} from "@src/routes/api/document.route";
import {wakaRouter} from "@src/routes/api/wakatime.route";
import {postRouter} from "@src/routes/api/post.route";
import {postCategoryRouter} from "@src/routes/api/post-category.route";
import {timelineEventsRouter} from "@src/routes/api/timeline-event.route";
import {productRouter} from "@src/routes/api/product.route";
import {notificationsRouter} from "@src/routes/api/notification.route";
import {calendarEventsRouter} from "@src/routes/api/calendar-event.route";

export const apiRouter = express.Router();

apiRouter.use(Authenticator.middleware());

apiRouter.use('/info', infoRouter);
apiRouter.use('/auth', authRouter);
apiRouter.use('/gitlab', gitlabRouter);
apiRouter.use('/emails', emailRouter);
apiRouter.use('/accounts', accountsRouter);
apiRouter.use('/general-tasks', authMiddleware('jwt'), generalTaskRouter);
apiRouter.use('/posts', postRouter);
apiRouter.use('/post-categories', postCategoryRouter);
apiRouter.use('/weather', authMiddleware('jwt'), weatherRouter);
apiRouter.use('/requests', authMiddleware('jwt'), requestsRouter);
apiRouter.use('/reports', authMiddleware('jwt'), reportsRouter);
apiRouter.use('/groups', authMiddleware('jwt'), groupsRouter);
apiRouter.use('/projects', authMiddleware('jwt'), projectsRouter);
apiRouter.use('/departments', authMiddleware('jwt'), departmentsRouter);
apiRouter.use('/job-info', authMiddleware('jwt'), jobInfoRouter);
apiRouter.use('/salaries', authMiddleware('jwt'), salaryRouter);
apiRouter.use('/regulations', authMiddleware('jwt'), regulationRouter);
apiRouter.use('/boards', authMiddleware('jwt'), boardRouter);
apiRouter.use('/issues', authMiddleware('jwt'), issueRouter);
apiRouter.use('/tasks', authMiddleware('jwt'), taskRouter);
apiRouter.use('/labels', authMiddleware('jwt'), labelRouter);
apiRouter.use('/feedbacks', authMiddleware('jwt'), feedbackRouter);
apiRouter.use('/rewards', /*authMiddleware('jwt'),*/ rewardsRouter);
apiRouter.use('/criteria', authMiddleware('jwt'), criteriaRouter);
apiRouter.use('/telegram', authMiddleware('jwt'), telegramRouter);
apiRouter.use('/weather', authMiddleware('jwt'), weatherRouter);
apiRouter.use('/timeline-events', authMiddleware('jwt'), timelineEventsRouter);
apiRouter.use('/calendar-events', authMiddleware('jwt'), calendarEventsRouter);
apiRouter.use('/entrance', authMiddleware('jwt'), entranceRouter);
apiRouter.use('/waka', authMiddleware('jwt'), wakaRouter);
apiRouter.use('/document', authMiddleware('jwt'), documentRouter);
apiRouter.use('/notification', authMiddleware('jwt'), notificationsRouter);
apiRouter.use('/products', productRouter);

apiRouter.get('/quote', authMiddleware('jwt'), randomQuote);
apiRouter.get('/test', Authenticator.need({
  roles: ['admin']
}))
