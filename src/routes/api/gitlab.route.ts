import {Router} from "express";
import {
  connectToGitLab, disconnectToGitLab,
  getGroups, getProjectsByGroup,
  getProjectIssues, getProjectHooks,
} from "@src/controllers/gitlab.controller";
import {authMiddleware} from "@src/middlewares/auth.middleware";
import 'express-async-errors';

export const gitlabRouter = Router();
gitlabRouter.get("/connect", connectToGitLab);
gitlabRouter.get("/groups", authMiddleware('jwt'), getGroups);
gitlabRouter.get("/groups/:groupId/projects", authMiddleware('jwt'), getProjectsByGroup);
gitlabRouter.get("/projects/:projectId/hooks", authMiddleware('jwt'), getProjectHooks);
gitlabRouter.get("/projects/:projectId/issues", authMiddleware('jwt'), getProjectIssues);
gitlabRouter.post("/connect", authMiddleware('jwt'), connectToGitLab);
gitlabRouter.post("/disconnect", authMiddleware('jwt'), disconnectToGitLab);
