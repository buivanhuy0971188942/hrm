import express from "express";
import {getAccountConfigInfos} from "@src/controllers/account.controller";
import {authMiddleware} from "@src/middlewares/auth.middleware";
import {Authenticator} from "@src/services/AuthService";

export const infoRouter = express.Router();

infoRouter.get('/', (request, response) => {
  response.send('NorthStudio Stable API v1.0');
});

infoRouter.get('/account-config', authMiddleware("jwt"), Authenticator.need({roles: ['admin', 'hrm']}), getAccountConfigInfos);