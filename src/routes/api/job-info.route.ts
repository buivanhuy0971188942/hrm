import { Router } from "express";
import { deleteJobInfo, updateJobInfo, addJobInfo, getJobInfos } from "@src/controllers/account.controller";
export const jobInfoRouter = Router();

jobInfoRouter.get('/', getJobInfos);
jobInfoRouter.post('/', addJobInfo);
jobInfoRouter.post('/:jobId', updateJobInfo);
jobInfoRouter.delete('/:jobId', deleteJobInfo);
