import {validateMiddleware} from '@src/middlewares/validate.middleware';
import {Router} from "express";
import {
  getSalaryRecords, getPaymentAdjustments,
  getSalaryRecord, addSalaryPayment,
  addPaymentAdjustment, getPaymentAdjustment,
  updatePaymentAdjustment, deletePaymentAdjustment,
  updateSalaryPayment
} from "@src/controllers/salary.controller";
import {
  addSalaryPaymentValidation,
  addPaymentAdjustmentValidation,
  updateSalaryPaymentValidation
} from '@src/validations/salary.validation';
import {Authenticator} from "@src/services/AuthService";

import 'express-async-errors';

export const salaryRouter = Router();

salaryRouter.get('/', getSalaryRecords);
salaryRouter.get('/:salaryId', getSalaryRecord);
salaryRouter.get('/:salaryId/adjustments', getPaymentAdjustments);
salaryRouter.post('/', Authenticator.need({roles: ['admin', 'hrm']}), validateMiddleware(addSalaryPaymentValidation), addSalaryPayment);
salaryRouter.post('/:salaryId', Authenticator.need({roles: ['admin', 'hrm']}), validateMiddleware(updateSalaryPaymentValidation), updateSalaryPayment);

salaryRouter.post('/:salaryId/adjustments', Authenticator.need({roles: ['admin', 'hrm']}), validateMiddleware(addPaymentAdjustmentValidation), addPaymentAdjustment);
salaryRouter.get('/adjustments/:adjustmentId', Authenticator.need({roles: ['admin', 'hrm']}), getPaymentAdjustment);
salaryRouter.post('/adjustments/:adjustmentId', Authenticator.need({roles: ['admin', 'hrm']}), updatePaymentAdjustment);
salaryRouter.delete('/adjustments/:adjustmentId', Authenticator.need({roles: ['admin', 'hrm']}), deletePaymentAdjustment);