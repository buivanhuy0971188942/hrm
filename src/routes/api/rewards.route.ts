import {Router} from "express";
import {giveRewards} from "@src/controllers/rewards.controller";

import 'express-async-errors';

export const rewardsRouter = Router();
rewardsRouter.post('/', giveRewards);
