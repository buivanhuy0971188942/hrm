import {validateMiddleware} from '@src/middlewares/validate.middleware';
import {Router} from "express";
import {
  addPaymentRegulation,
  getPaymentRegulations,
  updatePaymentRegulation,
  addAttendanceRegulation,
  deletePaymentRegulation
} from "@src/controllers/regulation.controller";
import {
  addPaymentRegulationValidation,
  addAttendanceRegulationValidation,
  updatePaymentRegulationValidation
} from '@src/validations/regulation.validation';
import {Authenticator} from "@src/services/AuthService";

import 'express-async-errors';

export const regulationRouter = Router();

regulationRouter.get('/', getPaymentRegulations);
regulationRouter.post('/payments', Authenticator.need({roles: ['admin', 'hrm']}), validateMiddleware(addPaymentRegulationValidation), addPaymentRegulation);
regulationRouter.post('/payments/:regulationId', Authenticator.need({roles: ['admin', 'hrm']}), validateMiddleware(updatePaymentRegulationValidation), updatePaymentRegulation);
regulationRouter.post('/attendance', Authenticator.need({roles: ['admin', 'hrm']}), validateMiddleware(addAttendanceRegulationValidation), addAttendanceRegulation);
regulationRouter.post('/attendance/:regulationId', Authenticator.need({roles: ['admin', 'hrm']}), validateMiddleware(updatePaymentRegulationValidation), updatePaymentRegulation);
regulationRouter.delete('/:regulationId', Authenticator.need({roles: ['admin', 'hrm']}), deletePaymentRegulation);