import { addCriteriaValidation, updateCriteriaValidation } from '@src/validations/criteria.validation';
import { validateMiddleware } from '@src/middlewares/validate.middleware';
import { Router } from "express";
import {
  getAllCriteria, addCriteria,
  getCriteria, deleteCriteria,
  updateCriteria,
} from "@src/controllers/criteria.controller";
import 'express-async-errors';

export const criteriaRouter = Router();
criteriaRouter.get('/', getAllCriteria);
criteriaRouter.post('/', validateMiddleware(addCriteriaValidation), addCriteria);
criteriaRouter.get('/:criteriaId', getCriteria);
criteriaRouter.post('/:criteriaId', validateMiddleware(updateCriteriaValidation), updateCriteria);
criteriaRouter.delete('/:criteriaId', deleteCriteria);