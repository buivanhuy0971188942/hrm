import {
  addGroupValidation,
  updateGroupValidation,
  addGroupPostValidation,
  groupMemberValidation, addGroupMemberValidation, updateGroupPostValidation,
} from '@src/validations/group.validation';
import {validateMiddleware} from '@src/middlewares/validate.middleware';
import {Router} from "express";
import {
  addGroup, addGroupPost,
  deleteGroup, getGroupPosts,
  getGroups, getGroupPost,
  getGroup, updateGroup,
  updateGroupPost, deleteGroupPost,
  addGroupMember, deleteGroupMember,
  getGroupMembers, updateGroupMember
} from "@src/controllers/group.controller";
import {Authenticator} from "@src/services/AuthService";
import {uploadMiddleware} from "@src/middlewares/general-file.middleware";
import {fileTypes} from "@src/utils/google-storage";
import {readGroupFile} from "@src/controllers/group-file.controller";

export const groupsRouter = Router();

groupsRouter.get('/',
  Authenticator.need({roles: ["admin"]}, {
    allowOwnResource: ["creator", "ref"],
    refDocFilter: {model: "GroupMemberModel", returnField: "group", localField: "member"}
  }),
  getGroups
);

groupsRouter.get('/files', readGroupFile);

groupsRouter.get('/:groupId',
  Authenticator.need({roles: ["admin"]}, {
    allowOwnResource: ["creator", "ref"],
    refDocFilter: {model: "GroupMemberModel", returnField: "group", localField: "member"}
  }),
  getGroup
);

groupsRouter.get('/:groupId/posts', getGroupPosts);
groupsRouter.get('/:groupId/posts/:postId', getGroupPost);
groupsRouter.get('/:groupId/members', getGroupMembers);

groupsRouter.post('/',
  uploadMiddleware(fileTypes["image"]),
  validateMiddleware(addGroupValidation),
  addGroup
);

groupsRouter.post('/:groupId',
  Authenticator.need({roles: ["admin"]}, {
    allowOwnResource: ["creator"]
  }),
  uploadMiddleware(fileTypes["image"]),
  validateMiddleware(updateGroupValidation),
  updateGroup
);

groupsRouter.post('/:groupId/posts',
  uploadMiddleware([...fileTypes["doc"], ...fileTypes["image"]]),
  validateMiddleware(addGroupPostValidation),
  addGroupPost
);

groupsRouter.post('/:groupId/posts/:postId',
  uploadMiddleware([...fileTypes["doc"], ...fileTypes["image"]]),
  validateMiddleware(updateGroupPostValidation),
  updateGroupPost
);

groupsRouter.post('/:groupId/members',
  Authenticator.need({roles: ["admin"]}, {
    allowOwnResource: ["creator"]
  }),
  validateMiddleware(addGroupMemberValidation),
  addGroupMember
);

groupsRouter.post('/:groupId/members/edit',
  Authenticator.need({roles: ["admin"]}, {
    allowOwnResource: ["creator"]
  }),
  validateMiddleware(groupMemberValidation),
  updateGroupMember
);

groupsRouter.delete('/:groupId', Authenticator.need({roles: ["admin"]}, {allowOwnResource: ["creator"]}), deleteGroup);
groupsRouter.delete('/:groupId/posts/:postId', deleteGroupPost);
groupsRouter.delete('/:groupId/members/delete', validateMiddleware(groupMemberValidation), deleteGroupMember);
