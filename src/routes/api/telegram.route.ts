import {Router} from "express";
import {verifyCode} from "@src/controllers/telegram.controller";

export const telegramRouter = Router();
telegramRouter.post('/verify-code', verifyCode);
