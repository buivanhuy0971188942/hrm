import { Router } from "express";
import { uploadMiddleware } from "@src/middlewares/general-file.middleware";
import {
  addNewDocFile,
  handleStatusFile,
  updateFolder,
  getDocFileIsPending,
  deleteDoc,
  addNewDocFolder,
  getDoc,
  getDocs,
  restoreDoc
} from "@src/controllers/document.controller";
import { authMiddleware } from "@src/middlewares/auth.middleware";
import { Authenticator } from "@src/services/AuthService";
import { fileTypes } from "@src/utils/google-storage";

export const documentRouter = Router();

documentRouter.post(
  "/add-file",
  Authenticator.need({ roles: ["admin", "hrm"] }),
  uploadMiddleware([...fileTypes["doc"], ...fileTypes["image"]]),
  addNewDocFile
);
documentRouter.post(
  "/add-folder",
  Authenticator.need({ roles: ["admin", "hrm"] }),
  addNewDocFolder
);
documentRouter.get(
  "/is-pending",
  Authenticator.need({ roles: ["admin"] }),
  getDocFileIsPending
);
documentRouter.get("/:placeholder", authMiddleware("jwt"), getDocs);
documentRouter.get("/all/:folderId", authMiddleware("jwt"), getDoc);
documentRouter.put(
  "/handle-status",
  Authenticator.need({ roles: ["admin"] }),
  handleStatusFile
);
documentRouter.put(
  "/:folderId",
  Authenticator.need({ roles: ["admin"] }),
  updateFolder
);
documentRouter.put("/restore/:docId", Authenticator.need({ roles: ["admin"] }), restoreDoc);
documentRouter.delete("/:docId", Authenticator.need({ roles: ["admin"] }), deleteDoc);