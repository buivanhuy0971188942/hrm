import {Router} from "express";
import {
  getAuthCode,
  getTokens,
  logout
} from "@src/controllers/auth.controller";
import {authMiddleware} from "@src/middlewares/auth.middleware";

export const authRouter = Router();
authRouter.get("/", getAuthCode);
authRouter.post("/token", getTokens);
authRouter.post('/logout', authMiddleware("jwt"), logout);

if (process.env.NODE_ENV !== "test") {
  authRouter.get("/oauth", authMiddleware("oauth2"), (req, res) => {
    // res.json(req.data)
    res.json({success: true});
  });
}
