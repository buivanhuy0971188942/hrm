import {Router} from "express";
import {sendEmailValidation, updateEmailValidation} from '@src/validations/email.validation';
import {validateMiddleware} from '@src/middlewares/validate.middleware';
import {
  getEmail,
  getEmails,
  downloadEmailAttachment,
  sendEmail, updateEmail
} from "@src/controllers/email.controller";
import {authMiddleware} from "@src/middlewares/auth.middleware";
import 'express-async-errors';
import {uploadMiddleware} from "@src/middlewares/general-file.middleware";

export const emailRouter = Router();

// emailRouter.get('/', authMiddleware('jwt'), getEmails);
emailRouter.get('/:placeholder', authMiddleware('jwt'), getEmails);
emailRouter.get('/:placeholder/:emailId', authMiddleware('jwt'), getEmail);

// No auth!
emailRouter.get('/download-attachment', downloadEmailAttachment);

emailRouter.post('/send',
  authMiddleware('jwt'),
  uploadMiddleware([], 5 * 1000 * 1000),
  validateMiddleware(sendEmailValidation),
  sendEmail
);

emailRouter.post('/:emailId',
  authMiddleware('jwt'),
  validateMiddleware(updateEmailValidation),
  updateEmail
);
