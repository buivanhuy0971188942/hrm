import {updatePostValidation, addPostValidation} from '@src/validations/post.validation';
import {validateMiddleware} from '@src/middlewares/validate.middleware';
import {Router} from "express";
import {
  addPost, getPosts,
  getPost, updatePost,
  deletePost
} from "@src/controllers/post.controller";
import 'express-async-errors';
import {uploadMiddleware} from "@src/middlewares/general-file.middleware";
import {authMiddleware} from "@src/middlewares/auth.middleware";

export const postRouter = Router();
postRouter.get('/public', getPosts);
postRouter.get('/public/:postId', getPost);
postRouter.use(authMiddleware('jwt'));
postRouter.get('/', getPosts);
postRouter.get('/:postId', getPost);
postRouter.post('/', uploadMiddleware(), validateMiddleware(addPostValidation), addPost);
postRouter.post('/:postId', uploadMiddleware(), validateMiddleware(updatePostValidation), updatePost);
postRouter.delete('/:postId', deletePost);