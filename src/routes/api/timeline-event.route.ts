import {addEventValidation} from '@src/validations/timeline-event.validation';
import {validateMiddleware} from '@src/middlewares/validate.middleware';
import {Router} from "express";
import {
  getTimelineEvents, addTimelineEvent,
  getTimelineEvent, deleteTimelineEvent,
  updateTimelineEvent, getTimelineEventCalendar,
} from "@src/controllers/timeline-event.controller";
import {Authenticator} from "@src/services/AuthService";

export const timelineEventsRouter = Router();

timelineEventsRouter.get('/',
  getTimelineEvents
);

timelineEventsRouter.get('/calendar',
  getTimelineEventCalendar
);

timelineEventsRouter.get('/:eventId',
  getTimelineEvent
);

timelineEventsRouter.post('/',
  // Authenticator.need({roles: ['admin', 'hrm']}),
  validateMiddleware(addEventValidation),
  addTimelineEvent
);

timelineEventsRouter.post('/:eventId',
  // Authenticator.need({roles: ['admin', 'hrm']}),
  updateTimelineEvent
);

timelineEventsRouter.delete('/:eventId',
  // Authenticator.need({roles: ['admin', 'hrm']}),
  deleteTimelineEvent
);