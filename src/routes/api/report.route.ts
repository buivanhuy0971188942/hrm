import {validateMiddleware} from '@src/middlewares/validate.middleware';
import {Router} from "express";
import {
  getReports, addReport,
  getReport, deleteReport,
  getSentReports,
} from "@src/controllers/report.controller";
import {addReportValidation} from '@src/validations/report.validation';
import {Authenticator} from "@src/services/AuthService";

export const reportsRouter = Router();

reportsRouter.get('/', [Authenticator.need({roles: ['admin', 'hrm']})], getReports);
reportsRouter.get('/sent', getSentReports);
reportsRouter.post('/', validateMiddleware(addReportValidation), addReport);
reportsRouter.get('/:reportId', getReport);
reportsRouter.delete('/:reportId', deleteReport);
