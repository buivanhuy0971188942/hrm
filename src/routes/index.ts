import express from "express";
import {apiRouter} from '@src/routes/api';
import {webhooksRouter} from "@src/routes/webhooks";

export const mainRouter = express.Router();

mainRouter.get("/", (req, res) => {
  res.json({message: "Welcome to NorthStudio Stable API"});
});
mainRouter.use('/api/v1', apiRouter);
mainRouter.use('/webhook', webhooksRouter);
