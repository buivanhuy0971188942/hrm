type WorkType = "FULL_TIME" | "PART_TIME" | "REMOTE";

type WorkModel = "OFFICIAL" | "INTERNSHIP";

type RequestStatus = "APPROVED" | "REJECTED" | "PENDING";

type JobTitle =
  "BACKEND_DEVELOPER"
  | "FRONTEND_DEVELOPER"
  | "MOBILE_DEVELOPER"
  | "DESIGNER"
  | "CONTENT_WRITER"
  | "HUMAN_RESOURCE"
  | "CEO"
  | "CTO";

// type RequestStatus = "LATE" | "ON_TIME";

type PaymentRegulationType = "BONUS" | "PENALTY" | 'ALLOWANCE' | "ATTENDANCE";

type WorkingHourShift = "MORNING" | "AFTERNOON" | "ALL";

type WorkingHourStatus = "ON_TIME" | "LATE" | 'ABSENT';

type TaskType = "PROJECT" | "GENERAL";

type Privacy = "PUBLIC" | "PRIVATE";

type GroupMemberRole = "ADMIN" | "MEMBER";

type PostState = "CLOSED" | "OPENED";

type PostType = "GROUP" | "FEED";

type TimelineEventTypes = "STUDY" | "OVERTIME" | "CHECK_IN" | "CHECK_OUT" | "IDLE_TIME";

type TimelineEventStates = "APPROVED" | "REJECTED" | "PENDING";

type CalendarEventTypes = "PERSONAL-EVENT" | "COMPANY-EVENT" | "HOLIDAY";

type RecurrenceTypes = "DAILY" | "WEEKLY" | "MONTHLY" | "YEARLY";

type RequestTypes = "STUDY" | "OVERTIME" | "ABSENCE" | "OTHER";

type EmailLabels = "INBOX" | "STARRED" | "TRASH" | "SENT";

interface Account {
  _id?: any,
  id?: any,
  avatar?: string,
  fullName: string,
  slug: string,
  email: string,
  address?: string,
  phone?: string,
  password?: string,
  role?: string,
  accessToken?: string,
  rePassword?: string,
  status?: string,
  joinedDate?: number,
  leavedDate?: number,
  workType?: WorkType,
  workModel?: WorkModel,
  department?: Department,
  jobInfo: JobInfo,
  gender?: string,
  dob?: number,
  cardId?: string,
  isVerified: boolean,
  isTelegramConnected: boolean,
  activeEmail: string,
  emails: string[],
  identityNumber: string,
  gitLabId: string,
  wakaId: string,
  gitLabAccessToken: string,
  gitLabRefreshToken: string,
  telegramId: string,
  exp?: number,
  stars?: number,
  balance?: number,
  isDeleted?: boolean
}

type RewardType = 'stars' | 'exp' | 'money';

interface RewardRecord {
  // source: any,
  target: any,
  type: RewardType,
  amount: any,
  timestamp: number
}

interface GiveRewardsOptions {
  type: RewardType,
  target: any,
  // source: any,
  amount: number
}

interface EntranceRecord {
  entranceId: number,
  time: number,
  cardId: string,
  type: 0 | 1,
}

interface JobInfo {
  id?: any,
  creator: Account,
  title: string,
  slug: string,
  minSalary?: number,
  maxSalary?: number,

}

interface APIResponse {
  status: number,
  data?: any,
  message?: string
}

interface APIQueryOptions {
  endpoint: string,
  method?: string,
  data?: any,
  headers?: object,
  params?: object,
  options?: object
}

interface AuthConfig {
  grantType: string,
  redirectUri?: string,
  clientID: string,
  clientSecret: string
}

interface AuthData {
  code?: string,
  refreshToken?: string
}

interface TokenModel {
  accessToken: string,
  accessTokenExpiresIn: number,
  refreshToken?: string,
  refreshTokenExpiresIn?: number,
  account: Account,
  id?: any,
  createdAt: Date,
  updatedAt: Date
}

interface Role {
  id?: any,
  name: string,
  slug: string,
  level: number,
  permissions: GlobalPermissions[],
  default: boolean,
  createdAt: Date,
  updatedAt: Date
}

interface Notification {
  id?: any,
  sender: Account,
  receiver: Account,
  isSeen: boolean,
  body: string,
  title: string,
  isDeleted: boolean
  action: string
}

interface DeviceToken {
  id?: any,
  account: Account,
  tokens: string[],
}

interface BankAccountModel {
  id?: any,
  owner: Account,
  bankName: string,
  accountNumber: string,
  cardNumber: string,
  isActive: boolean,
  createdAt: Date,
  updatedAt: Date
}

interface TimelineEvent {
  id?: any,
  owner: Account,
  type: TimelineEventTypes,
  note: string,
  startTime: number,
  endTime: number,
  state: TimelineEventStates,
  request: Request
}

interface CalendarEvent {
  id?: any,
  owner: Account,
  title: string,
  type: CalendarEventTypes,
  privacy: Privacy,
  description?: string,
  startTime: Date,
  endTime: Date,
  alarm: number,
  isRecurrent: boolean,
  recurrenceType: RecurrenceTypes
}

interface Request {
  id?: any,
  sender: Account,
  assessor: Account,
  content: string,
  title: string,
  slug: string,
  type: RequestTypes,
  status: RequestStatus,
  response: string,
  department: Department,
  isDeleted: boolean,
  tasks: Task[],
}

interface RequestReceiver {
  id?: any,
  request: Request,
  receiver: Account
}

interface Report {
  id?: any,
  sender: Account,
  content: string,
  title: string,
  feedback: string,
  type: string,
  status: ReportStatus,
  isDeleted: boolean,
  createdAt: Date,
  updatedAt: Date
}

interface Receiver {
  receiver: Account,
  request: Request,
  createdAt: Date,
  updatedAt: Date
}

interface Group {
  id?: any,
  creator,
  name: string,
  slug: string,
  coverPicture?: string,
  avatar?: string,
  description?: string,
  privacy: Privacy
}

interface GroupFile extends Attachment {
  isFolder: string,
  level: number,
}

interface Department {
  id?: any,
  name: string,
  slug: string,
  avatar: string
}

interface DepartmentParticipant {
  department: Department,
  account: Account
}

interface GroupMember {
  group: Group,
  member: Account,
  role: GroupMemberRole
}

interface Attachment {
  _id?: any,
  id?: any,
  creator: Account,
  target: any,
  fileName: string,
  fileType: 'doc' | 'photo' | 'video',
  link: string,
  path: string,
  size: string,
  metadata?: any
}

interface Project {
  id?: any,
  creator: Account,
  name: string,
  slug: string,
  type: string,
  dueDate: Date,
  description: string,
  image: string,
  gitLabData: {
    projectId: string,
    namespace: string,
    webUrl: string,
    webhookId: string,
    projectName: string,
    sshUrl: string,
    httpUrl: string,
    createdAt: Date
  },
  isDeleted: boolean
}

interface ProjectMember {
  id?: any,
  member: Account,
  project: Project,
  role: string
}

interface TaskMember {
  id?: any,
  member: Account,
  task: Project,
}

interface Board {
  id?: any,
  creator: Account,
  name: string,
  slug: string,
  index: number,
  project: Project
}

interface Label {
  id?: any,
  creator: Account,
  name: string,
  color: string,
  project: Project,
  isDeleted: boolean
}

interface TaskLabel {
  id?: any,
  task: Task,
  label: Label
}

interface Issue {
  id?: any,
  iid: string,
  project: Project,
  gitLabProjectId: string,
  title: string,
  description: string,
  state: string,
  createdTimeOnGitLab: Date,
  closedAt: Date,
  labels: string[],
}

interface Task {
  id?: any,
  creator: Account,
  name: string,
  startDate: Date,
  dueDate: Date,
  assignedTo: Account,
  board: Board,
  project: Project,
  labels: Label[],
  cover?: string,
  index?: number,
  description?: string,
  state?: string,
  comments?: Comment[],
  attachments?: Attachment[],
  issue: Issue,
  type: TaskType,
  members: Account[]
  isDeleted: boolean
}

interface TrackingTask {
  _id?: any,
  id?: any,
  creator: Account
  task: Task,
  changes: Object,
  action: 'ADD' | 'UPDATE' | 'DELETE'
}

interface SubTask {
  _id?: any,
  id?: any,
  task: Task,
  creator: Account,
  name: string,
  status: 'IN_PROGRESS' | 'COMPLETED'
}

interface PostCategory {
  id?: any,
  name: string,
  slug: string,
  hashtag: string,
  description?: string
}

interface Post {
  id?: any,
  type: PostType,
  source: any,
  categories: PostCategory[],
  creator: Account,
  title: string,
  slug: string,
  content: string,
  banner: string,
  description: string,
  state: PostState,
  group: Group,
  isDeleted: boolean
}

interface Product {
  id?: any,
  author: Account,
  category: ProductCategory,
  description: string,
  url: string,
  banner: string,
  name: string,
  slug: string,
  downLoadCount: number,
  CHPlayUrl: string,
  AppStoreUrl: string,
}

interface ProductCategory {
  id?: any,
  author: Account,
  category: Category,
  description: string,
  url: string,
  banner: string,
}

interface PostComment {
  _id?: any,
  creationTime: Date,
  content: string,
  attachments: Attachment[],
  creator: Account
}

interface EmailAttachment {
  name: string,
  contentType: string,
  size: number,
  url: string
}

interface Email {
  id?: any,
  sender: string,
  from: string,
  emailType: string,
  creationTime: number,
  recipient: string,
  subject: string,
  data: any,
  strippedText: string,
  bodyHTML: string,
  owner: Account,
  createdAt: Date,
  updatedAt: Date,
  hasAttachment: boolean,
  attachments: EmailAttachment[],
  label: EmailLabels,
  starredAt: string,
  isStarred: boolean,
  deletedAt: string,
  isDeleted: boolean,
  isSeen: boolean,
}

interface SalaryPayment {
  id?: any,
  employee: Account,
  grossSalary: number,
  period: Date
}

interface PaymentRegulation {
  creator: Account,
  title: string,
  price: number,
  description: string,
  type: PaymentRegulationType,
  id?: any
}

interface PaymentAdjustment {
  id?: any,
  creator: Account,
  salaryPayment: SalaryPayment,
  regulation: PaymentRegulation,
  amount: number,
  description: string,
  workingHour: WorkingHour
}

interface WorkingHour {
  employee: Account,
  startTime: number,
  endTime: number,
  shift: WorkingHourShift,
  status: WorkingHourStatus,
  note: string,
  id?: any,
}


interface Feedback {
  employee: Account,
  assessor: Account,
  note: string,
  overallRating: string,
  id?: any,
}

interface FeedbackRecord {
  feedback: Feedback,
  criteria: Criteria,
  rating: number,
  note: string,
  id?: any,
}

interface Criteria {
  creator: Account,
  name: string,
  slug: string,
  id?: any,
}

interface DocFilter {
  start?: string,
  end?: string,
  range?: string,

  [key: string]: any
}

interface DocumentFile extends Attachment {
  status: "pending" | "approved" | "rejected";
  isDeleted: boolean;
  folderParentIds: GeneralFolder[];
}

interface DocumentFolder {
  _id?: any;
  id?: any;
  folderName: string;
  folders: GeneralFolder[];
  isDeleted: boolean;
  files: DocumentGeneral[];
  folderParentIds: GeneralFolder[];
  root?: boolean;
}
