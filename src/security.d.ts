type GlobalPermissions = "account.read" | "account.update" | "account.delete" |
  "project.create" | "project.update" | "project.read" | "project.delete" |
  "group.create" | "group.update";


interface AuthOptions {
  allowOwnResource: string[],
  creatorDocFilter?: {
    field: string,
    accountField: string
  }
  refDocFilter?: {
    model: string,
    localField: string
    returnField: string
  }
}