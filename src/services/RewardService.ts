import mongoose from "mongoose";
import {AccountModel} from "@src/models";
import {ApiError} from "@src/utils/api-error";
import {RewardRecordModel} from "@src/models/accounts/reward-record.model";

function randInt(min: number, max: number) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

export class RewardService {
  
  static async give(cardId: string) {

    const account = await AccountModel.findOne({
      cardId: cardId
    });
    if (!account) throw new ApiError(513, "It seems like this is not a valid account or the account does not exist.");

    const latestRewards = await RewardRecordModel.find({
      target: account._id
    }).limit(1).sort({$natural:-1});

    const giveOptions: GiveRewardsOptions = {
      target: account._id,
      amount: 0,
      type: 'money'
    };

    if (latestRewards.length && latestRewards[0].timestamp > (Date.now() - 1000 * 60 * 60)) {
      // -5k.
      giveOptions.amount = -5000;
    }
    else {
      giveOptions.amount = this.randomRewardAmount('money');
    }

    await this.createRewardRecord(giveOptions.type, giveOptions.target, /*giveOptions.source,*/ giveOptions.amount);
    account[giveOptions.type] = (account[giveOptions.type] || 0) + giveOptions.amount;
    await account.save();
  }

  static async createRewardRecord(type: RewardType, target: mongoose.Schema.Types.ObjectId, /*source: mongoose.Schema.Types.ObjectId,*/ amount: number) {
    await RewardRecordModel.create({
      // source: source,
      target: target,
      type: type,
      amount: amount,
      timestamp: Date.now()
    });
  }

  static randomRewardAmount(type: RewardType): number {
    if (type === 'money') {
      // 40% to receive 5k - 50k.
      const probabilities = [0, 1, 0, 1, 1, 0, 0, 1, 0, 0];
      const random = probabilities[randInt(0, probabilities.length - 1)];
      if (random === 1) {
        return randInt(5, 50) * 1000;
      }
      return 0;
    }
    else {
      throw new ApiError(513, "This reward type is not supported yet.");
    }
  }
}
