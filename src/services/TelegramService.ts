import {config} from "@src/config";
import {Markup, Telegraf} from "telegraf";
import {TelegramLinkRequestModel} from "@src/models/attendance/telegram-link-request.model";
import {randomString} from "@src/utils/randomString";
import axios from "axios";

export class TelegramService {
  static bot = new Telegraf(config.telegram.botToken);

  static async register() {
    const bot = this.bot;
    await bot.launch();

    this.startService();

    process.once('SIGINT', () => bot.stop('SIGINT'))
    process.once('SIGTERM', () => bot.stop('SIGTERM'))
  }

  static startService() {
    this.bot.command('/menu', ctx => {
      ctx.reply(`Bạn muốn làm gì?`, Markup.inlineKeyboard([
        Markup.button.callback('Lấy ID', 'action:get_id'),
        Markup.button.callback('Liên kết tài khoản', 'action:link_account'),
      ]))
    });
    this.bot.command('/talk', async ctx => {
      try {
        await ctx.tg.deleteMessage(ctx.chat.id, ctx.message.message_id)
        await ctx.reply(ctx.message.text.slice(6));
      } catch (e) {
        console.log("Something bad happened");
      }
    });
    this.bot.command('/tarot', async ctx => {
      try {
        const {data} = await axios.get('https://api-tarots.herokuapp.com/random');
        if (data.status === 200) {
          await ctx.replyWithPhoto({
            url: data['forward_image']
          });
          data['cards_Info'].forEach(card => {
            ctx.reply(`Lá ${card['name'].trim()}.\n` +
              `Mô tả:\n` +
              `${card['title_forward']}\n\n` +
              `Khái quát:\n` +
              `${card['title_main']}\n\n` +
              `Sức khỏe:\n` +
              `${card['title_heath']}\n\n` +
              `Công việc:\n` +
              `${card['title_work']}\n\n` +
              `Tình yêu:\n` +
              `${card['title_love']}\n\n` +
              `Tiền bạc:\n` +
              `${card['title_money']}\n\n` +
              `Lá bài ngược:\n` +
              `${card['title_reverse']}\n\n`
            );
          });
        }
      } catch (e) {

      }
    });
    this.bot.on('callback_query', async (ctx) => {
      ctx.telegram.answerCbQuery(ctx.callbackQuery.id);
      ctx.answerCbQuery();
      if (ctx.callbackQuery.data === 'action:get_id') {
        await ctx.replyWithMarkdownV2("ID của bạn là: `" + ctx.callbackQuery.from.id + "`");
      }

      if (ctx.callbackQuery.data === 'action:link_account') {
        let request = await TelegramLinkRequestModel.findOne({
          telegramId: ctx.callbackQuery.from.id,
        });
        if (!request) request = await TelegramLinkRequestModel.create({
          telegramId: ctx.callbackQuery.from.id,
          randomCode: randomString(80)
        });

        await ctx.reply(
          `Nhấn vào link sau để liên kết tài khoản:`,
          Markup.inlineKeyboard([
            Markup.button.url("Liên kết", `https://stable.northstudio.dev/auth/telegram-link?code=${request.randomCode}`)
          ])
        );
      }
    })
  }
  static async sendMessage(chatId: string | number, content: string) {
    return this.bot.telegram.sendMessage(chatId, content);
  }
}
