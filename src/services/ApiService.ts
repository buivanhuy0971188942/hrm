import axios, { type AxiosRequestConfig, type Method } from "axios";
import { config } from "@src/config";

interface ApiOptions extends AxiosRequestConfig {
}
export class ApiService {
  static AUTH_BASE_URL = config.oauth2.baseAuthUrl || "http://192.168.1.8:3000/api/v1";

  static async signUp(accountInfo: any): Promise<APIResponse> {
    return await this.request({
      method: "POST",
      url: `${this.AUTH_BASE_URL}/auth/sign-up`,
      data: accountInfo
    });
  }

  static async signIn(signInData: any): Promise<APIResponse> {
    return await this.request({
      method: "POST",
      url: `${this.AUTH_BASE_URL}/auth/sign-in`,
      data: signInData
    });
  }

  static async api(apiOptions: APIQueryOptions | string, reqData: object = null): Promise<APIResponse> {
    let method: Method = "GET", params = {}, data = {}, endpoint: string = '', exOptions = {};
    if (typeof apiOptions === 'object') {
      endpoint = apiOptions.endpoint;
      method = (apiOptions.method || "GET").toUpperCase() as Method;
      params = {
        ...apiOptions.params,
        ...(method === "GET" ? apiOptions.data : {})
      };
      data = method === "GET" ? null : apiOptions.data;
      exOptions = apiOptions.options || {};
    } else {
      endpoint = apiOptions;
      data = reqData;
    }
    return this.request({
      method: method,
      url: `${this.AUTH_BASE_URL}/${endpoint}`,
      data: data,
      params,
      ...exOptions,
    });
  }

  static async request(options: AxiosRequestConfig) {
    try {
      const { data, status } = await axios(options);
      return {
        status: status,
        ...data
      };
    } catch (e) {
      if (e.response && e.response.data) {
        return {
          status: e.response.status,
          ...e.response.data
        };
      } else if (e.response) {
        return {
          status: e.response.status
        }
      }
      return null
    }
  }
}
