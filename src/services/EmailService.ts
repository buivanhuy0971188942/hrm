import Mailgun from "mailgun.js";
import {config} from "@src/config";
import formData from "form-data";
import {logger} from "@src/config/logger.config";
import {promises as fs} from "fs";
import {MailgunMessageData} from "mailgun.js/interfaces/Messages";

interface EmailBody {
  // from: string,
  to?: string,
  subject?: string,
  text?: string,
  html?: string,
  attachment?: string[]
}

const mailgun = new Mailgun(formData);

export class EmailService {
  private apiKey = config.mailgun.apiKey;
  private mg = mailgun.client({username: "api", key: this.apiKey});
  private readonly domain: string;

  constructor(domain: string) {
    this.domain = domain || config.mailgun.domain;
  }

  public async sendEmail(emailBody: MailgunMessageData) {
    try {
      // const formData = new FormData();
      // Object.keys(emailBody).forEach(data => {
      //   if (Array.isArray(emailBody[data])) {
      //     emailBody[data].forEach(subData => formData.append(data, subData))
      //   } else
      //     formData.append(data, emailBody[data])
      // });

      if (emailBody.attachment) {
        const file = []
        for (const attachment of emailBody.attachment) {
          const data = await fs.readFile(attachment);
          file.push({data});
        }
        Object.assign(emailBody, {attachment: file});
      }
      console.log("Email body", emailBody);
      const data = await this.mg.messages.create(this.domain, emailBody);
      console.log("Email service response", data);
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  public async getEmail() {
    try {
      // const formData = new FormData();
      // Object.keys(emailBody).forEach(data => {
      //   if (Array.isArray(emailBody[data])) {
      //     emailBody[data].forEach(subData => formData.append(data, subData))
      //   } else
      //     formData.append(data, emailBody[data])
      // });
      // console.log(emailBody)

      const data = await this.mg.lists.list();
      console.log(data)
      return data;
    } catch (e) {
      logger.error(e);
      throw e;
    }
  }
}