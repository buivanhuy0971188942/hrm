import 'module-alias/register';
import express, {Express} from "express";
import path from 'path';
import passport from "passport";
import cors from 'cors';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import {config} from '@src/config';
import {errorHandler} from "@src/middlewares/error.middleware";
import {mainRouter} from '@src/routes';
import {connectDB} from "@src/middlewares/mongodb.middleware";
import swaggerUI from "swagger-ui-express";
import {logger} from "@src/config/logger.config";
import multer from 'multer';
import {SocketService} from "@src/services/SocketService";

const upload = multer();

export class WebService {
  protected static app: Express;
  static port: string | number = config.port || 3000;

  static async start() {
    this.app = express();
    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded({extended: true}));
    this.app.use(express.json());
    this.app.use(express.urlencoded({extended: true}));
    // this.app.use(upload.array("file"));
    this.app.use(express.static(path.join(__dirname, './public')));
    this.app.use(cookieParser(config.jwt.secret));
    this.app.use(cors());
    this.app.options('*', cors());
    this.app.use(connectDB);
    this.app.use(passport.initialize());
    this.app.use(mainRouter);
    this.app.use(errorHandler);
    this.app.use('/docs', swaggerUI.serve, swaggerUI.setup(require("../../swagger.json")));
    this.app.use(function (req, res, next) {
      return res.status(404).json({error: 'Page not found'});
    });

    const httpServer = this.app.listen(this.port, () => {
      logger.info(`Server is listening at http://localhost:${this.port}`);
      SocketService.bindSocketServer(httpServer);
      logger.info(`Socket server is running at ws://localhost:${this.port}`);
    });

  }
}