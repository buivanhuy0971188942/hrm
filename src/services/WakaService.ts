import axios from "axios";
import {config} from "@src/config";
import qs from "qs";
import {WakaConnectionModel} from "@src/models/accounts/waka-connection.model";

export class WakaService {
  static getToken(code: string, redirectUri: string) {
    return axios({
      method: "POST",
      url: `${config.waka.baseUrl}/oauth/token`,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      data: qs.stringify({
        client_id: config.waka.clientId,
        client_secret: config.waka.clientSecret,
        redirect_uri: redirectUri,
        grant_type: "authorization_code",
        code: code,
      }),
    });
  }

  static async refreshToken(refreshToken: string) {
    let authResponse = {};
    try {
      const authRequestData = {
        client_id: process.env.WAKA_APP_ID,
        client_secret: process.env.WAKA_APP_SECRET,
        redirect_uri: process.env.WAKA_REDIRECT_URI,
        grant_type: 'refresh_token',
        refresh_token: refreshToken,
      };
      const response: any = await axios({
        method: "POST",
        url: "https://wakatime.com/oauth/token",
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: qs.stringify(authRequestData)
      });
      authResponse = response.data;
    } catch (e) {
      console.log(e);
      throw new Error("Failed to authorize due to unknown reason.");
    }
    return authResponse;
  }

  static async getStat(uid: string, accessToken: string, refreshToken?: string) {
    try {
      const response: any = await axios({
        method: "get",
        url: `https://wakatime.com/api/v1/users/${uid}/stats/last_7_days`,
        headers: {
          'Authorization': `Bearer ${accessToken}`
        },
      });
      return response.data.data;
    } catch (e) {
      // console.log(e);
      const wakaRefresh = await WakaService.refreshToken(refreshToken);

      const wakaConnection = await WakaConnectionModel.findOneAndUpdate({
        wakaId: uid
      }, {
        token: wakaRefresh["access_token"],
        refreshToken: wakaRefresh["refresh_token"],
        expirationTime: wakaRefresh["expires_at"]
      }, {new: true});
      // console.log({wakaRefresh, wakaConnection})
      return await this.getStat(wakaConnection["wakaId"], wakaConnection["token"], wakaConnection["refreshToken"]);
    }
  }
}


