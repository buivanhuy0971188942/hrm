import {DbService} from '@src/services/DbService';
import jwt, {SignOptions} from "jsonwebtoken";
import {config} from "@src/config";
import {ExtendedNextFunction, ExtendedRequest, ExtendedResponse} from "@src/types/type";
import {ApiError} from "@src/utils/api-error";
import httpStatus from "http-status";
import axios from "axios";
import {RoleModel} from "@src/models";

export class AuthService {
  static generateToken(payload, options: SignOptions = {}) {
    return jwt.sign(JSON.stringify({
      ...payload,
    }), config.jwt.secret, options).toString();
  }

  static async saveToken(tokenData: TokenModel) {
    await DbService.addDoc("TokenModel", tokenData);
  }

  static validateToken(token: string) {
    try {
      return jwt.verify(token, config.jwt.secret);
    } catch (e) {
      return false;
    }
  }

  static tokenExtractorFromRequest = (req: ExtendedRequest) => {
    let jwt;
    if (req.cookies && req.cookies.access_token) {
    } else if (req.headers && req.headers.authorization) {
      jwt = req.headers.authorization.slice("Bearer ".length);
    } else if (req.body && req.body.access_token) {
      jwt = req.body.access_token;
    } else if (req.query && req.query.access_token) {
      jwt = req.query.access_token;
    }
    return jwt;
  }

  static getTokensFromOAuth = async (tokenUrl: string, authConfig: AuthConfig, authData: AuthData) => {
    let body = {}
    switch (authConfig.grantType) {
      case "authorization_code":
        body = {
          grant_type: authConfig.grantType,
          redirect_uri: authConfig.redirectUri,
          client_id: authConfig.clientID,
          client_secret: authConfig.clientSecret,
          code: authData.code,
        }
        break;
      case "refresh_token":
        body = {
          grant_type: authConfig.grantType,
          client_id: authConfig.clientID,
          client_secret: authConfig.clientSecret,
          refresh_token: authData.refreshToken
        }
        break;
      default:
        throw new ApiError(httpStatus.BAD_REQUEST, "Invalid grant type");
    }

    try {
      const data = await axios({
        url: tokenUrl,
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          'Access-Control-Allow-Origin': '*'
        },
        data: body
      });
      return data["data"];
    } catch (e) {
      console.log(e)
      throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, "Failed to retrieve token")
    }
  }
}

export interface NeedConfig {
  roles?: string[],
  permissions?: GlobalPermissions[],
}

export class Authenticator {
  static roles: Role[] = [];
  userPermissions: string[] = [];
  filterByOwnResource: any = {};

  constructor() {
  }

  static async register() {
    this.roles = (await RoleModel.find({})).map(role => role.toJSON());
  }

  static middleware() {
    return (req: ExtendedRequest, res: ExtendedResponse, next: ExtendedNextFunction) => {
      req.authenticator = new Authenticator();
      next();
    }
  }

  static getRolePermissions(roleSlug: string) {
    return this.roles.find(x => x.slug === roleSlug).permissions;
  }

  static need(config: NeedConfig, options?: AuthOptions) {
    const roles = config.roles || [];
    const permissions = config.permissions || [];

    return async (req: ExtendedRequest, res: ExtendedResponse, next: ExtendedNextFunction) => {
      if (roles && roles.length && roles.length > 0) {
        if (!roles.includes(req.account.role)) {
          if (req.params?.accountId === req.account.id || req.params?.accountId === "me") return next();
          if (options?.allowOwnResource && options?.allowOwnResource.length > 0) {
            req.authenticator.filterByOwnResource["$or"] = [];
            for (const field of options.allowOwnResource) {
              switch (field) {
                case "creator":
                  req.authenticator.filterByOwnResource["$or"].push((options.creatorDocFilter)
                    ? {[options.creatorDocFilter.field]: req.account[options.creatorDocFilter.accountField]}
                    : {creator: req.account._id});
                  break;

                case"ref":
                  if (options.refDocFilter) {
                    req.authenticator.filterByOwnResource["$or"].push({
                      _id: {
                        $in: (await DbService.getDocByFilter(options.refDocFilter.model, {[options.refDocFilter.localField]: req.account._id}, {
                          many: true,
                          nullable: true
                        })).map(data => data[options.refDocFilter.returnField])
                      }
                    });
                  }
                  break;
              }
            }
            return next();
          }
          throw new ApiError(httpStatus.FORBIDDEN, "Unauthorized");
        }
      }
      if (permissions && permissions.length && permissions.length > 0) {
        const accountPermissions: GlobalPermissions[] = this.getRolePermissions(req.account.role) || [];
        for (let perm in permissions) {
          if (!accountPermissions.includes(perm as GlobalPermissions)) throw new ApiError(httpStatus.FORBIDDEN, "Unauthorized");
        }
      }
      next();
    }
  }
}
