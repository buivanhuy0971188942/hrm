import httpStatus from 'http-status';
import {ApiError} from '@src/utils/api-error';
import mongoose, {isValidObjectId} from "mongoose";
import {config, mongooseConfig} from '@src/config';
import {DbQueryOptions, ExtendedRequest} from '@src/types/type';
import {processPaginationData, requestPagination} from '@src/utils/request-pagination';
import * as models from "@src/models";
import {slugify} from "@src/utils/slugify";
import {logger} from "@src/config/logger.config";

const DATE_FIELDS = ["createdAt", "dueDate", "deadline"];
const EXCLUDE_FIELDS = ["role", "_id", "$or", "$and", "member", "dueDate"];

export class DbService {
  static async start() {
    if (!mongoose.connections[0].readyState) {
      await mongoose.connect(mongooseConfig.url);
      logger.info("Connect to MongoDB successfully");
      await this.initiateData();
    }
  }

  private static async initiateData() {
    const existsRole = config.system.roles.split("|");
    const roles = (await DbService.getDocByFilter("RoleModel", {name: {$in: existsRole}}, {
      many: true,
      lean: true,
      nullable: true
    })).map(data => data.name);
    let newRoles = existsRole.filter(data => !roles.includes(data)).map(data => {
      return {
        insertOne: {
          document: {
            name: data,
            slug: slugify(data)
          }
        }
      }
    });
    if (newRoles.length > 0) await DbService.bulkWriteDoc("RoleModel", newRoles);
  }

  static async disconnect() {
    if (!mongoose.connections[0].readyState) {
      return await mongoose.disconnect();
    }
  }

  static async paginateDocsByFilter(model: string, filter: DocFilter = {}, req: ExtendedRequest, options: DbQueryOptions = {}) {
    let excludedFields = [...EXCLUDE_FIELDS, filter.range]

    if (options.excludedFilterField) excludedFields = [...excludedFields, ...options.excludedFilterField];

    const paginateSettings = requestPagination(req);
    if (filter.range) {
      if (DATE_FIELDS.includes(filter.range)) {
        Object.assign(filter, {
          start: new Date(parseInt(req.query["start"].toString())),
          end: new Date(parseInt(req.query["end"].toString())),
        });
      }
      filter[filter.range] = (filter.start && filter.end) ? {
        $gte: filter.start,
        $lte: filter.end
      } : (filter.start) ? {$gte: filter.start} : (filter.end) && {$lte: filter.end}
      delete filter.start;
      delete filter.end;
      delete filter.range;
    }

    for (const field in filter) {
      if (!isValidObjectId(filter[field]) && !excludedFields.includes(field) && typeof filter[field] === 'string') { filter[field] = new RegExp(filter[field], 'i');
     }
    }
    console.log(filter)
    const docs = await models[model].paginate(filter, {...paginateSettings, ...options});
    return processPaginationData(docs);
  }


  static async getDocByFilter(model: string, filter: any = {}, options: DbQueryOptions = {}) {
    let doc;
    if (filter.range) {
      if (DATE_FIELDS.includes(filter.range)) {
        Object.assign(filter, {
          start: new Date(parseInt(filter.start)),
          end: new Date(parseInt(filter.end)),
        });
      }
      filter[filter.range] = (filter.start && filter.end) ? {
        $gte: filter.start,
        $lte: filter.end
      } : (filter.start) ? {$gte: filter.start} : (filter.end) && {$lte: filter.end}
      delete filter.start;
      delete filter.end;
      delete filter.range;
    }
    doc = await models[model].find(filter, null, options).populate(options.populate);
    if (!options.nullable && doc.length === 0) throw (options.customError) ? new ApiError(options.customError.code, options.customError.message) : new Error(`${model.split("Model")[0]} not found`);
    return (options.many) ? doc : doc[0];
  }

  static async addDoc(model: string, docBody: any = {}, options: DbQueryOptions = {uniqueFields: [], populate: ""}) {
    if (options.many) {
      const doc = await models[model].insertMany(docBody, options);
      return doc;
    } else {
      if (options?.uniqueFields?.length > 0) {
        for (const field of options.uniqueFields) {
          if (await models[model].exists({[field]: docBody[field]})) {
            throw new ApiError(httpStatus.BAD_REQUEST, `This ${field} has already been taken`);
          }
        }
      }
      const doc = await models[model].create(docBody);
      (options.populate) && await doc.populate(options.populate);
      return doc;
    }
  }

  static async updateDocByFilter(model: string, filter: any = {}, updateBody: any, options: DbQueryOptions = {}) {
    if (options.many) {
      await models[model].updateMany(filter, updateBody);
    } else {
      let doc = (filter.filterAsDoc) ? filter.filterAsDoc : await this.getDocByFilter(model, filter, options);
      if (options?.uniqueFields?.length > 0) {
        for (const field of options.uniqueFields) {
          if (await models[model].exists({[field]: updateBody[field], _id: {$ne: doc._id}})) {
            throw new ApiError(httpStatus.BAD_REQUEST, `This ${field} has already been taken`);
          }
        }
      }

      Object.assign(doc, updateBody);
      doc = await doc.save();
      if (options.populate) await doc.populate(options.populate);
      return doc;
    }
  }

  static async deleteDocByFilter(model: string, filter: any = {}, options: DbQueryOptions = {}) {
    if (options.many) {
      await models[model].deleteMany(filter);

    } else {
      const doc = await this.getDocByFilter(model, filter, options);
      await doc.deleteOne({});
      return doc;
    }
  }

  static async bulkWriteDoc(model: string, operations: any = []) {
    await models[model].bulkWrite(operations);
    return;
  }

  static async countDocumentByFilter(model: string, filter: any = {}, options: DbQueryOptions = {}) {
    return await models[model].countDocuments(filter);
  }

  static async multipleQueries(models: string[], query: string, filter: any = {}, updateBody: any = {}, options: DbQueryOptions = {}) {
    for (const model of models) {
      if (query === "updateDocByFilter") {
        await this[query](model, filter, updateBody, options);
      } else {
        const data = await this[query](model, filter, options);
        console.log(model + ": " +data)
      }
    }
  }
}
