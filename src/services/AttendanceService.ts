import axios from "axios";
import {EntranceRecordModel} from "@src/models/attendance/entrance-record.model";
import {AccountModel} from "@src/models";
import {TelegramService} from "@src/services/TelegramService";
import {TimelineEventModel} from "@src/models/events/timeline-event.model";
import moment from "moment";
import {logger} from "@src/config/logger.config";
import {NotificationService} from "@src/services/NotificationService";

const RESTART_SERVER_DURATION = 15;

export interface EntranceRecordOptions {
  allowSendNotification: boolean
}

export class AttendanceService {
  static lastId: number = 0;

  static register() {
    this.heartBeat();
  }

  static async heartBeat() {
    let date = new Date().toISOString().split('T')[0];
    const accounts = (await AccountModel.find({cardId: {$ne: null}})).map(account => account.cardId);
    (accounts.length > 0) && AttendanceService.queryEntranceRecords(date, accounts).then(() => {
      setTimeout(AttendanceService.heartBeat, 1000);
    }).catch(async _ => {
      logger.error("Failed to connect to attendance server, restarting...");
      // logger.error("Failed to connect to attendance server");
      setTimeout(AttendanceService.heartBeat, RESTART_SERVER_DURATION * 1000);
      // let i = RESTART_SERVER_DURATION;
      // process.stdout.clearLine(0);
      // process.stdout.cursorTo(0);
      // process.stdout.write(`Restart attendance server after ${i} seconds...`);
      // const a = setInterval(function () {
      //   --i;
      //   process.stdout.clearLine(0);
      //   process.stdout.cursorTo(0);
      //   if (i > 0) {
      //     process.stdout.write(`Restart attendance server in ${i} ${(i > 1) ? "seconds" : "second"}...`);
      //   } else {
      //     clearInterval(a);
      //     logger.info("Restarting attendance server...");
      //   }
      // }, 1000);
    });
  }

  static async queryEntranceRecords(date: string, cardIds: string[], options?: EntranceRecordOptions) {
    const {data} = await axios({
      method: "POST",
      url: 'https://api-timechecker.timebird.org/card/data',
      headers: {
        "Content-Type": "application/json"
      },
      data: {
        "date": date,
        "cardIds": cardIds
      }
    });
    const entranceRecords = data.data;
    for (let record of entranceRecords) {
      if ((await EntranceRecordModel.countDocuments({entranceId: record['idAttendance']})) <= 0) {
        const newRecord = {
          entranceId: record['idAttendance'],
          time: record['checktime'],
          cardId: record['userId'],
          type: record['checktype'],
        };

        await EntranceRecordModel.create(newRecord);
        const account = await AccountModel.findOne({
          cardId: newRecord.cardId,
        });
        await this.reportNewEntrance(account);
        await this.recordDailyTimelineEvent(account, newRecord);
        await NotificationService.send({_id: account._id}, {
          title: "Cập nhật lịch sử ra vào.",
          body: `Bạn vừa ${((newRecord.type === 0) ? "checkin" : "checkout")} vào lúc ${moment(newRecord.time).format("LT")}.`
        }, account._id);
      }
    }
  }

  static async recordDailyTimelineEvent(account: Account, newRecord?: EntranceRecord) {
    const state: string = (newRecord.type === 0) ? "in" : "out";
    const startOfDay = moment().startOf("day").format("x");
    if (account) {
      const dailyEvents = await TimelineEventModel.find({
        owner: account._id,
        startTime: {$gte: startOfDay}
      }).sort("time");
      switch (state) {
        case "in":
          if (dailyEvents.length <= 0) {
            await TimelineEventModel.create({
              startTime: newRecord.time, type: "CHECK_IN",
              owner: account._id
            });
          } else {
            const lastRecord = dailyEvents.pop();
            if (lastRecord.type === "CHECK_OUT" && lastRecord.startTime) {
              Object.assign(lastRecord, {endTime: newRecord.time, type: "IDLE_TIME"});
              await lastRecord.save();
            } else {
              await TimelineEventModel.create({
                endTime: newRecord.time, type: "IDLE_TIME",
                owner: account._id
              });
            }
          }
          break;
        case "out":
          await TimelineEventModel.create({
            startTime: newRecord.time, type: "CHECK_OUT",
            owner: account._id
          });
          break;
      }
    }
  }

  static async reportNewEntrance(account: Account) {
    if (account && account.isTelegramConnected) {
      const telegramId = account.telegramId;
      await TelegramService.sendMessage(telegramId, "Vừa quẹt thẻ.");
    }
    return account;
  }
}
