import admin from "firebase-admin";
import {firebaseConfig} from "@src/config/firebase.config";
import {DbService} from "@src/services/DbService";

export class NotificationService {
  static start() {
    return admin.initializeApp({
      credential: admin.credential.cert(firebaseConfig),
      databaseURL: "https://igclone-2b707-default-rtdb.asia-southeast1.firebasedatabase.app"
    });
  }

  static async extractTokens(filter: any) {
    let tokens = [];
    const accounts = (await DbService.getDocByFilter("AccountModel", filter, {
      many: true,
      nullable: true
    })).map(data => data._id);

    const deviceTokenDocs = (await DbService.getDocByFilter("DeviceTokenModel", {account: {$in: accounts}}, {
      many: true,
      nullable: true
    }));

    deviceTokenDocs.forEach(data => tokens = tokens.concat(data.tokens));
    return {deviceTokenDocs, tokens};
  }
  // TODO: Delete device token if failed to send notification
  // TODO: Add handler for sending to only 100 device each
  static async send(receiverFilter: any, notification: { title: string, body: string, action?: string }, sender?: Account) {
    const {deviceTokenDocs, tokens} = await NotificationService.extractTokens(receiverFilter);

    if (tokens.length > 0) {
      this.sendNotificationFromFirebase(tokens, notification);
      const notificationBody = deviceTokenDocs.map(data => {
        return {
          title: notification.title,
          body: notification.body,
          action: notification.action,
          receiver: data.account,
          sender: sender,
        }
      });
      await DbService.addDoc("NotificationModel", notificationBody);
    }
  }

  private static sendNotificationFromFirebase (tokens: string[], notification) {
    admin
      .messaging()
      .sendMulticast({
        notification: {
          title: notification.title,
          body: notification.body,
        },
        android: {
          notification: {
            title: notification.title,
            body: notification.body,
          },
          data: {action: notification.action}
          //priority: "high"
        },
        apns: {
          headers: {
            "apns-priority": "10",
            "apns-expiration": "360000",
          },
          payload: {
            aps: {
              alert: {
                title: notification.title,
                body: notification.body,
              },
              sound: "default",
            },
            data: {action: notification.action},
          },
        },
        tokens,
      })
      .then(async (batchResponse) => {
        console.log({res: batchResponse.responses});
      })
      .catch((error) => {
        console.log(error);
      });
  }
}