import axios, {AxiosRequestConfig, Method} from 'axios';
import {config} from '@src/config';

interface GitLabQueryOptions {
  pagination?: boolean;
  maxPages?: number;
  page?: number;
  limit?: number;
}

interface HookBody {
  url: string;
  push_events?: boolean;
  issues_events?: boolean;

  [key: string]: any
}

export class GitLabService {

  static async getSelfAccount(accessToken: string) {
    const {data} = await axios(`${config.gitLab.apiUrl}/user`, {
      method: "get",
      headers: {
        "Authorization": `Bearer ${accessToken}`
      }
    });
    return data;
  }

  static async getTokenInfo(accessToken: string) {
    const {data} = await axios(`${config.gitLab.baseUrl}/oauth/token/info`, {
      method: "get",
      headers: {
        "Authorization": `Bearer ${accessToken}`
      }
    });
    return data;
  }

  static async getGroups(accessToken: string, options?: GitLabQueryOptions) {
    const ownNamespace = await this.request("/namespaces?owned_only=true", "get", accessToken);
    Object.assign(ownNamespace[0], {id: "me", full_name: ownNamespace[0].name});
    const groups = [ownNamespace[0], ...(await this.request(`/groups`, 'get', accessToken))];

    return (options.pagination) ? this.paginate(groups, options) : {data: groups};
  }

  static async getProjectsByGroups(accessToken: string, groupId: string, options?: GitLabQueryOptions) {
    const projects = (groupId === "me") ? await this.request(`/projects?owned=true`, 'get', accessToken) : await this.request(`/groups/${groupId}/projects`, 'get', accessToken);
    return (options.pagination) ? this.paginate(projects, options) : {data: projects};
  }

  static async getProjectIssues(accessToken: string, projectId: string, options?: GitLabQueryOptions) {
    const issues = await this.request(`/projects/${projectId}/issues`, 'get', accessToken);
    return (options.pagination) ? this.paginate(issues, options) : {data: issues};
  }

  static async getProjectIssue(accessToken: string, projectId: string, issueId: string, options?: GitLabQueryOptions) {
    const issue = await this.request(`/projects/${projectId}/issues/${issueId}`, 'get', accessToken);
    return issue;
  }

  static async addProjectIssue(accessToken: string, projectId: string, issueBody, options?: GitLabQueryOptions) {
    const issue = await this.request(`/projects/${projectId}/issues`, 'post', accessToken, {
      data: issueBody
    });
    return issue;
  }

  static async updateProjectIssue(accessToken: string, projectId: string, issueId: string, issueBody, options?: GitLabQueryOptions) {
    const issue = await this.request(`/projects/${projectId}/issues/${issueId}`, 'put', accessToken, {
      data: issueBody
    });
    return issue;
  }

  static async getProjectCommits(accessToken: string, projectId: string, options?: GitLabQueryOptions) {
    const issues = await this.request(`/projects/${projectId}/issues`, 'get', accessToken);
    return (options.pagination) ? this.paginate(issues, options) : {data: issues};
  }


  static async getProjectHooks(accessToken: string, projectId: string, options: GitLabQueryOptions) {
    const hooks = await this.request(`/projects/${projectId}/hooks`, 'get', accessToken);
    return this.paginate(hooks, options);
  }

  static async addProjectHook(accessToken: string, projectId: string, hookBody: HookBody) {
    const hooks = await this.request(`/projects/${projectId}/hooks`, 'get', accessToken);
    let hook = hooks.find(data => data.url === hookBody.url);
    hook = (hook) ?
      await this.request(`/projects/${projectId}/hooks/${hook.id}`, 'put', accessToken, {data: hookBody}) :
      await this.request(`/projects/${projectId}/hooks`, 'post', accessToken, {
        data: {
          ...hookBody,
          enable_ssl_verification: false
        }
      });
    return hook;
  }

  static async deleteProjectHook(accessToken: string, projectId: string, hookId: string) {
    let hook = await this.request(`/projects/${projectId}/hooks/${hookId}`, 'get', accessToken);
    if (hook) hook = await this.request(`/projects/${projectId}/hooks/${hook.id}`, 'delete', accessToken);
    return hook;
  }

  static async request(endpoints: string, method: Method, accessToken: string, axiosConfig?: AxiosRequestConfig) {
    const {data} = await axios(`${config.gitLab.apiUrl}${endpoints}`, {
      method,
      headers: {
        "Authorization": `Bearer ${accessToken}`
      },
      ...axiosConfig
    });
    return data;
  }

  private static paginate(arr: any[], options: GitLabQueryOptions) {
    if (!options.page) options.page = 1;
    if (!options.limit) options.limit = 5;
    options.page = parseInt(options.page.toString())
    options.limit = parseInt(options.limit.toString())
    let skip = (options.page === 1) ? 0 : options.limit * (options.page - 1);
    let data = arr.slice(skip, (skip + options.limit));
    return {
      data: data,
      pagination: {
        total: arr.length,
        page: parseInt(options.page.toString()),
        hasPrev: options.page > 1,
        hasNext: arr.length > options.page * options.limit,
      }
    }
  }
}