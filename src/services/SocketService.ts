import {Server} from "http";
import {Server as SocketServer} from "socket.io";
import {logger} from "@src/config/logger.config";

export class SocketService {
  static io: SocketServer;

  static bindSocketServer(server: Server) {
    this.io = new SocketServer(server, {
      path: "/socket.io",
      cors: {
        origin: "*"
      }
    });

    this.io.on("connection", (socket) => {
      logger.info(`A new connection has been established with id ${socket.id}` );
    });
  }
}
