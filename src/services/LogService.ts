
export enum LOG_LEVELS {
  DEBUG = 0,
  ERROR = 1,
  WARNING = 2,
}


export let LOG_LEVEL = 0;

export const setLogLevel = (level: number): void => {
  LOG_LEVEL = level;
}

export const logData = (data: any, loggingLevel: LOG_LEVELS = 0) : void => {
  if (LOG_LEVEL >= loggingLevel) {
    return console.log(`[${loggingLevel}]`, data);
  }
}
