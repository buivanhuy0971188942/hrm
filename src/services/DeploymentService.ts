import { DbService } from '@src/services/DbService';
import httpStatus from 'http-status';
import { ApiError } from '@src/utils/api-error';
import { config } from "@src/config";
import axios from "axios";

export class DeploymentService {
  static async checkGitlabId(projectId: string, excludedProject = null) {
    try {
      const { data } = await axios.get(`${config.gitLab.apiUrl}/projects/${projectId}?private_token=${config.gitLab.privateToken}`)
      const branches = (await axios.get(`${config.gitLab.apiUrl}/projects/${projectId}/repository/branches?private_token=${config.gitLab.privateToken}`)).data.map(branch => branch.name);
      return {
        name: data['name'],
        gitLabUrl: data['web_url'],
        branches
      }
    } catch (e) {
      console.log(e);
      throw new ApiError(httpStatus.NOT_FOUND, `Cannot find project with ID ${projectId}`);
    }
  }

  static async deployProject(projectId: string) {
    const project = await DbService.getDocByFilter("Project", { id: projectId });
    try {
      await axios.post(config.deployment.url, {
        project_id: project.autoDeploy.gitlabId
      });
      return project;
    } catch (e) {
      console.log(e);
      throw new ApiError(httpStatus.NOT_FOUND, `Cannot find project with ID ${projectId}`);
    }
  }
}