import 'module-alias/register';
import {DbService} from "../services/DbService";

test('database connection', async () => {
  const connection = await DbService.connect();
  expect(!!connection);
});

afterAll(async () => {
  return await DbService.disconnect();
});