import {DbService} from '@src/services/DbService';
import {config} from '@src/config';
import axios from 'axios';
import {oAuth2Strategy} from "@src/config/passport.config";
import {ExtendedNextFunction, ExtendedRequest, ExtendedResponse} from "@src/types/type";
import {ApiError} from "@src/utils/api-error";
import httpStatus from "http-status";
import passport from "passport";

const verifyJWTCallback = async (req: ExtendedRequest) => {
  try {
    const {data} = await axios(`${config.oauth2.baseAuthUrl}/auth/verify-token`, {
      method: "post",
      headers: {
        "authorization": req.headers.authorization
      }
    });

    if (data.role !== "admin" && data.role !== "northstudio") {
      throw new Error('Not authenticated');
    }
    req.account = await DbService.getDocByFilter("AccountModel", {id: data.id});
  } catch (e) {
    if (config.env !== "deployment" && config.env !== "production") console.log(e);
    throw new ApiError(httpStatus.UNAUTHORIZED, 'Not authenticated');
  }
}

const verifyOAuth2Callback = (req: ExtendedRequest) => (err, user, info) => {
  if (err || !user) {
    throw new ApiError(httpStatus.UNAUTHORIZED, 'Not authenticated');
  }
  req.data = user;
}

export const authorizeMiddleware = (roles: string[]) => (req: ExtendedRequest, res: ExtendedResponse, next: ExtendedNextFunction) => {
  if (!roles.includes(req.account.role)) {
    return next(new ApiError(httpStatus.FORBIDDEN, 'Access denied'));
  }
  next();
}

export const authMiddleware = (method: string) => async (req: ExtendedRequest, res: ExtendedResponse, next: ExtendedNextFunction) => {
  switch (method) {
    case "jwt":
      await verifyJWTCallback(req);
      break;
    case "oauth2":
      passport.use('oauth2', oAuth2Strategy);
      passport.authenticate(method, {failureRedirect: '/auth/sign-in'}, verifyOAuth2Callback(req))(req, res, next);
      break;
    default: {
      throw new Error("Invalid authentication strategy");
    }
  }
  next();
}

