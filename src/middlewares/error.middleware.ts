import httpStatus from 'http-status';
import { config } from "@src/config";
import { logger } from '@src/config/logger.config';
import { ApiError } from '@src/utils/api-error';
import { ExtendedNextFunction, ExtendedRequest, ExtendedResponse } from "@src/types/type";

export const errorHandler = (err, req: ExtendedRequest, res: ExtendedResponse, next: ExtendedNextFunction) => {
  console.log(err)
  let error = err;
  if (error && !(error instanceof ApiError)) {
    const statusCode = error.statusCode || httpStatus.INTERNAL_SERVER_ERROR;
    const message = error.message || httpStatus[statusCode];
    error = new ApiError(statusCode, message, false, err.stack);
  }

  let { statusCode, message } = error;
  res.errorMessage = error.message;
  const response = {
    code: statusCode,
    message,
  };

  if (config.env === 'development') {
    logger.error(err);
  }
  
  res.status(statusCode).send(response);
}
