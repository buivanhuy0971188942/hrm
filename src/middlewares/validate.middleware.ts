import Joi from 'joi';
import httpStatus from 'http-status';
import { queryFilter } from '@src/utils/filter';
import { ApiError } from '@src/utils/api-error';
import { ExtendedNextFunction, ExtendedRequest, ExtendedResponse } from "@src/types/type";
import { config } from '@src/config';

export const validateMiddleware = (schema) => (req: ExtendedRequest, res: ExtendedResponse, next: ExtendedNextFunction) => {
  const validSchema = queryFilter(schema, ['params', 'query', 'body', 'file']);
  const object = queryFilter(req, Object.keys(validSchema));
  const { value, error } = Joi.compile(validSchema)
    .prefs({ errors: { label: 'key' }, abortEarly: false })
    .validate(object);
  if (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, (config.env !== "deployment") ? error.toString() : "Validation failed");
  }

  Object.assign(req, value);
  next();
};