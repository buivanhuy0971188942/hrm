import mongoose from 'mongoose';
import { ExtendedNextFunction, ExtendedRequest, ExtendedResponse } from "@src/types/type";
import { logger } from '@src/config/logger.config';

export const connectDB = (req: ExtendedRequest, res: ExtendedResponse, next: ExtendedNextFunction) => {
  if (!mongoose.connections[0].readyState) {
    // Use new db connection
    mongoose.connect(process.env.MONGO_URL, () => {
      // connected
      logger.info("Connect to MongoDB successfully");
    });
  }
  next();
};

export default connectDB;
