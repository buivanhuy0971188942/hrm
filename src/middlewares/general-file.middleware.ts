import formidable from 'formidable'
import httpStatus from "http-status";
import {ApiError} from "@src/utils/api-error";
import {uploadFileFromBase64} from "@src/utils/google-storage";
import {ExtendedNextFunction, ExtendedRequest, ExtendedResponse} from "@src/types/type";

export const uploadMiddleware = (extFilter = [], limitedSize = 2 * 1000 * 1000) => (req: ExtendedRequest, res: ExtendedResponse, next: ExtendedNextFunction) => {
  const form = formidable({multiples: true});

  form.parse(req, async (err, fields, files) => {
    if (err) {
      console.log(err)
      return next(new ApiError(httpStatus.BAD_REQUEST, "Failed to upload file"));
    }
    req.body = fields;
    if (Object.keys(files).length <= 0) return next();

    if (!Array.isArray(files.file)) {
      files.file = [files.file];
    }

    for (const file of files.file) {
      if (!file["originalFilename"] || !file["size"]) return next(new ApiError(httpStatus.BAD_REQUEST, "Invalid file"));
      const ext = file["originalFilename"].split(".").pop();
      if (extFilter.length > 0 && !extFilter.includes(ext)) return next(new ApiError(httpStatus.BAD_REQUEST, "Unsupported file type"));
      if (file["size"] > limitedSize) return next(new ApiError(httpStatus.BAD_REQUEST, "This file is too large"));
    }
    req.files = files.file;
    next()
  });
}

export const handleBase64 = async (value) => {
  try {
    const files = [];
    let index: any = -1;
    const startIndexes = [];
    do {
      const startIndex = value.indexOf("<img", index);
      startIndexes.push({
        start: startIndex,
        end: value.indexOf(">", startIndex),
      });
      value.indexOf("<img", startIndex + 1) !== -1
        ? (index = startIndex + 1)
        : (index = false);
    } while (index);
    const newImages = [];
    for (const x of startIndexes) {
      const image = value.slice(x.start, x.end + 1);
      // console.log("===>",  value.slice(x.start, x.end + 1))
      if (image.includes('data:image')) {
        const base64 = image.slice(
          image.indexOf('src="') + 5,
          image.indexOf('"', image.indexOf('src="') + 5)
        );

        const filename = `image_${Date.now() + '_' + Math.round(Math.random() * 1E9)}.png`
        const fileData = await uploadFileFromBase64(filename, base64, "image/png");
        newImages.push({
          new: `<img style="max-width: 100%; object-fit: contain" src="${fileData["mediaLink"]}">`,
          old: image
        });
        files.push(fileData["mediaLink"]);

      } else {
        newImages.push({old: image, new: image.replace('<img', '<img style="max-width: 100%; object-fit: contain"')});
      }
    }
    for (let i = 0; i < newImages.length; i++) {
      value = value.replace(newImages[i]['old'], newImages[i]['new']);
    }
    return {data: value, files};
  } catch (e) {
    throw e;
  }
}
