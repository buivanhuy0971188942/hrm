import {Storage} from "@google-cloud/storage";
import {config} from "@src/config";
import {get} from 'lodash'
import {base64MimeType} from "@src/utils/base64";

export const fileTypes = {
  doc: ["doc", "pdf", "docx", "ppt", "pptx", "xlsx", "xls", "csv", "txt", "js", "ts"],
  video: ["mp4"],
  sound: ["mp3"],
  image: ["jpeg", "jpg", "png", "svg", "webp"]
}

export const connectToGCloud = async (bucketId: string = "northstudio-internal") => {
  try {
    const gStorage = new Storage({
      projectId: config.gCloud.projectId,
      credentials: {
        client_email: config.gCloud.clientEmail,
        private_key: config.gCloud.privateKey
      }
    });
    return gStorage.bucket(bucketId);
  } catch (e) {
    console.log(e);
  }
}

export const readFile = async (filepath: string) => {
  return new Promise(async (resolve) => {
    try {
      const bucket = await connectToGCloud();

    } catch (e) {
      console.log(e);
    }
  });
}

export const deleteFile = (filepath: string) => {
  return new Promise(async (resolve) => {
    try {
      const bucket = await connectToGCloud();
      if (filepath && filepath.includes("https://storage.googleapis.com")) {
        await bucket.file(filepath.split("/").pop().split("?")[0]).delete();
      }
      resolve("Delete previous file successfully");
    } catch (e) {
      console.log(e);
    }
  });
}

export const uploadFile = (filepath: string, options) => {
  return new Promise(async (resolve) => {
    const bucket = await connectToGCloud();
    const gFile = await bucket.upload(filepath, options);
    resolve(gFile);
  });
}

export const processFile = async (file) => {
  const uniqueSuffix = Date.now() + '_' + Math.round(Math.random() * 1E9);
  const ext = file["originalFilename"].split(".").pop();

  const gFile = await uploadFile(file["filepath"], {
    destination: `${file["newFilename"]}_${uniqueSuffix}.${ext}`,
  });
  // await deleteFile(gFile[1]["mediaLink"]);
  return {
    path: gFile[1]["mediaLink"],
    fileName: file["originalFilename"],
    fileType: Object.keys(fileTypes).filter(data => fileTypes[data].includes(ext))[0],
    size: file["size"]
  };
}

export const uploadFileFromBase64 = async (fileName: string, data: Blob | string, defaultMimeType?: string) => {
  const bucket = await connectToGCloud();

  const file = bucket.file(fileName);

  const fileOptions = {
    public: true,
    resumable: false,
    metadata: {contentType: base64MimeType(data) || defaultMimeType},
    validation: false
  }
  if (typeof data === 'string') {
    const base64EncodedString = data.replace(/^data:\w+\/\w+;base64,/, '')
    const fileBuffer = Buffer.from(base64EncodedString, 'base64')
    await file.save(fileBuffer, fileOptions);
  } else {
    await file.save(get(data, 'buffer', data), fileOptions);
  }
  // const publicUrl = `https://storage.googleapis.com/${BUCKET_NAME}/${fileName}`

  const [metadata] = await file.getMetadata()
  return {
    ...metadata,
    // publicUrl
  }
}
