import {randomNum, randomString} from './randomString';
import * as models from "@src/models";
import {v4 as uuid} from "uuid";
import crypto from "crypto";

export const slugify = (str: string) => {
  return str.normalize('NFD').replace(/[\u0300-\u036f]/g, '').replace(/đ/g, 'd').replace(/Đ/g, 'D').replace(/ /g, '_').toLowerCase().replace(/[^\w-]+/g, '');
}

export const uniqueStringGenerator = async (type: string, model: string, value: string = "", field: string = "id"): Promise<string> => {
  switch (type) {
    case "slug":
      let count = 0, newSlug = slugify(value);
      while (await models[model].exists({[field]: newSlug})) {
        newSlug = `${slugify(value)}_${++count}`;
      }
      return newSlug;
    case "code":
      let newCode = randomString(6);
      while (await models[model].exists({[field]: newCode})) {
        newCode = randomString(6);
      }
      return newCode.toString();
    case "iId":
      const doc = await models[model].findOne({}).sort({id: -1});
      let newIId = (doc && doc.id) ? parseInt(doc.id) : (value) ? parseInt(value) : 1000000000;
      while (await models[model].exists({[field]: newIId.toString()})) {
        newIId = newIId + randomNum(1);
      }
      return newIId.toString();
    case "uuid":
      return uuid();
    case "crypto":
      const seed = crypto.randomBytes(256);
      return crypto
        .createHash('sha1')
        .update(seed)
        .digest('hex');
  }
};