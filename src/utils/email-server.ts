import httpStatus from "http-status";
import mailgun from "mailgun-js";
import { logger } from "@src/config/logger.config";
import { ApiError } from "@src/utils/api-error";
import { config } from "@src/config";

export const sendEmailFromMailGun = async (mailData) => {
  const mg = mailgun({
    apiKey: config.mailgun.apiKey, domain: config.mailgun.domain
  });

  try {
    mg.messages().send({
      from: `NS <postmaster@idde.me>`,
      to: mailData.sendTo,
      subject: mailData.subject,
      html: mailData.contentHTML,
    });
    if (config.env !== "production") logger.info(`Email sent to ${mailData.sendTo}`);
    return {
      message: 'A request was sent to your email'
    };
  } catch (err) {
    if (config.env !== "production") logger.error(err);
    throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, "Cannot send email");
  }
}