export const randomString = (length) => {
  const result = [];
  const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
  const charactersLength = characters.length;
  for (let i = 0; i < length; i++) {
    result.push(characters.charAt(Math.floor(Math.random() *
      charactersLength)));
  }
  return result.join('');
}

export const randomNum = (length) => {
  const result = [];
  const characters = '0123456789';
  const charactersLength = characters.length;
  for (let i = 0; i < length; i++) {
    result.push(characters.charAt(Math.floor(Math.random() *
      charactersLength)));
  }
  return parseInt(result.join(''));
}

export const genRandomUser = () => {
  const name = `User${randomString(8)}`
  return {
    username: name,
    avatar: 'https://www.gravatar.com/avatar/3b3be63a4c2a439b013787725dfce802?d=mp&size=200',
    fullName: name,
  }
}

