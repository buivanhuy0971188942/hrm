import { Document } from "mongoose";
import { ExtendedRequest } from "@src/types/type";
import { PaginateResult } from "mongoose";

export const requestPagination = (req: ExtendedRequest, defaultPaginationSetup = {
  limit: 5,
  page: 1,
  sort: "-id"
}) => {
  let paginationSetup = defaultPaginationSetup;
  let limit = parseInt(req.query.limit as string);
  let page = parseInt(req.query.page as string);
  let sort = req.query.sortBy as string;

  if (typeof req.query.limit !== "undefined") {
    if (typeof limit === "number" && !isNaN(limit)) paginationSetup.limit = limit;
    else throw Error("Wrong pagination info.");
  }
  if (typeof req.query.page !== "undefined") {
    if (typeof page === "number" && !isNaN(page)) paginationSetup.page = page;
    else throw Error("Wrong pagination info.");
  }
  if (typeof req.query.sortBy !== "undefined") {
     paginationSetup.sort = sort;
  }

  return paginationSetup;
}

export const processPaginationData = (data: PaginateResult<Document<any>>) => {
  return {
    data: data.docs,
    pagination: {
      total: data.totalDocs,
      page: data.page,
      hasPrev: data.hasPrevPage,
      hasNext: data.hasNextPage,
    }
  }
}
