import { ExtendedNextFunction, ExtendedRequest, ExtendedResponse } from "@src/types/type";

export const catchAsync = (fn) => (req: ExtendedRequest, res: ExtendedResponse, next: ExtendedNextFunction) => {
  Promise.resolve(fn(req, res, next)).catch(err => next(err));
}
