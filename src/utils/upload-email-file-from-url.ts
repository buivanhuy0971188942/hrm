import { File } from "@google-cloud/storage";
import { join, parse } from "path";
import { uuid } from 'uuidv4';
import { URL } from "url";
import request from "request";
import {connectToGCloud} from "@src/utils/google-storage";

/**
 * This function takes a http url and uploads it to a destination in the bucket
 * under the same filename or a generated unique name.
 */
export async function uploadEmailFileFromUrl(
  url: string,
  destinationDir: string,
  options: { generateUniqueFilename: boolean, fileName: string } = {
    fileName: '',
    generateUniqueFilename: true
  }
) {
  let destination = join(destinationDir, uuid(), options.fileName);

  const bucket = await connectToGCloud("northstudio-internal");

  return new Promise<File>((resolve, reject) => {
    const file = bucket.file(destination);
    const req = request({
      url: url,
      headers: {
        Authorization: `Basic ${Buffer.from('api:' + process.env.MAILGUN_KEY).toString('base64')}`
      }
    });
    req.pause();
    req.on("response", res => {
      if (res.statusCode !== 200) {
        return reject(
          new Error(
            `Failed to request file from url: ${url}, status code: ${res.statusCode}`
          )
        );
      }

      req
        .pipe(
          file.createWriteStream({
            resumable: false,
            public: true,
            metadata: {
              contentType: res.headers["content-type"]
            }
          })
        )
        .on("error", err => {
          reject(
            new Error(
              `Failed to upload ${url} to ${destinationDir}: ${err.message}`
            )
          );
        })
        .on("finish", () => {
          console.log(`Successfully uploaded ${url} to ${destination}`);
          resolve(file);
        });
      req.resume();
    });
  });
}
