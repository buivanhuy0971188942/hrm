import { ExtendedNextFunction , ExtendedResponse, ExtendedRequest} from '@src/types/type';
import formidable from "formidable";

export const form = (req: ExtendedRequest, res: ExtendedResponse, next: ExtendedNextFunction) => {
  const form = formidable({ multiples: true });
  form.parse(req, (err, fields, files) => {
    if (err) {
      next(err);
      return;
    }
    req.body = fields;
    // if (files) req.filess = files;
    next();
  });
}