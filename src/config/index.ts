import dotenv from 'dotenv';
import path from 'path';

dotenv.config();

export const mongooseConfig = {
  url: process.env.MONGO_URL,
  options: {
    useCreateIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true,
  },
};

export const config = {
  env: process.env.NODE_ENV || "development",
  port: process.env.PORT  || 3000,
  jwt: {
    secret: process.env.JWT_SECRET,
    accessExpirationMinutes: process.env.JWT_ACCESS_EXPIRATION_MINUTES,
    refreshExpirationDays: process.env.JWT_REFRESH_EXPIRATION_DAYS,
    resetPasswordExpirationMinutes: process.env.JWT_RESET_PASSWORD_EXPIRATION_MINUTES,
    verifyEmailExpirationMinutes: process.env.JWT_VERIFY_EMAIL_EXPIRATION_MINUTES,
  },
  system: {
    baseUrl: process.env.BASE_URL || "https://stable-api.northstudio.dev",
    startWorkingTime: process.env.START_WORKING_TIME || "8:00",
    endWorkingTime: process.env.END_WORKING_TIME || "18:00",
    adminEmail: process.env.ADMIN_EMAIL || "admin@northstudio.vn",
    maxTimeLimiter: process.env.MAX_TIME_LIMITER || 1000000000000,
    roles: process.env.ROLES || ""
  },
  email: {
    smtp: {
      host: process.env.SMTP_HOST,
      port: process.env.SMTP_PORT,
      auth: {
        user: process.env.SMTP_USERNAME,
        pass: process.env.SMTP_PASSWORD,
      },
    },
    url: process.env.EMAIL_SERVER_URL,
    secret: process.env.EMAIL_SERVER_SECRET,
    from: `\"EOF\'s mailer\" ${process.env.EMAIL_SERVER_SENDER}`,
    verifyUrl: (process.env.NODE_ENV === "production") ? "https:eof.vn/api/email/verify/" : "http:localhost:3000/api/email/verify/"
  },
  mailgun: {
    apiKey: process.env.MAILGUN_KEY || "",
    domain: process.env.MAILGUN_DOMAIN || "northstudio.vn"
  },
  gCloud: {
    projectId: process.env.GCLOUD_PROJECT_ID,
    clientEmail: process.env.GCLOUD_CLIENT_EMAIL,
    privateKey: process.env.GCLOUD_PRIVATE_KEY && process.env.GCLOUD_PRIVATE_KEY.replace(/(\|\|)/g, '\n'),
    baseUrl: "https://storage.googleapis.com/"
  },
  oauth2: {
    baseAuthUrl: process.env.BASE_AUTH_URL,
    authorizationURL: process.env.AUTHORIZATION_URL,
    tokenURL: process.env.TOKEN_URL,
    clientID: process.env.CLIENT_ID,
    clientSecret: process.env.CLIENT_SECRET,
    callbackURL: process.env.CALLBACK_URL,
    scope: process.env.SCOPE || "",
    testUrl: process.env.NODE_ENV === "test" && (process.env.OAUTH_TEST_URL || "http://localhost:3001/api/v1/auth/oauth"),
  },
  gitLab: {
    privateToken: process.env.GITLAB_PRIVATE_TOKEN,
    apiUrl: process.env.GITLAB_API_URL,
    baseUrl: process.env.GITLAB_BASE_URL,
    clientID: process.env.GITLAB_CLIENT_ID,
    clientSecret: process.env.GITLAB_CLIENT_SECRET,
    callbackURL: process.env.GITLAB_CALLBACK_URL,
    scope: process.env.GITLAB_SCOPE || ""
  },
  waka: {
    baseUrl: `https://wakatime.com`,
    clientId: process.env.WAKA_APP_ID,
    clientSecret: process.env.WAKA_APP_SECRET
  },
  deployment: {
    url: process.env.DEPLOY_URL || ""
  },
  telegram: {
    botToken: process.env.BOT_TOKEN
  }
};
