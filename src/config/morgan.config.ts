import morgan from 'morgan';
import {config} from '@src/config/index';
import { logger } from '@src/config/logger.config';
import { ExtendedNextFunction, ExtendedRequest, ExtendedResponse } from "@src/types/type";

morgan.token('message', (req: ExtendedRequest, res: ExtendedResponse) => res.errorMessage);

const getIpFormat = () => (config.env === 'production' ? ':remote-addr - ' : '');
const successResponseFormat = `${getIpFormat()}:method :url :status - :response-time ms`;
const errorResponseFormat = `${getIpFormat()}:method :url :status - :response-time ms - message: :message`;

const successHandler = morgan(successResponseFormat, {
  skip: (req: ExtendedRequest, res: ExtendedResponse) => res.statusCode >= 400,
  stream: {write: (message) => logger.info(message.trim())},
});

const errorHandler = morgan(errorResponseFormat, {
  skip: (req: ExtendedRequest, res: ExtendedResponse) => res.statusCode < 400,
  stream: {write: (message) => logger.error(message.trim())},
});

export default {
  successHandler,
  errorHandler,
};
