import { config } from "@src/config";

export const urls = {
  redirectUrl: (config.env === "production") ? "https://idde.me/idea/" : "http://localhost:3000/idea/",
  verifyAccountUrl: "",
  resetPasswordUrl: ""
}

export const notificationEmailContent = (redirectUrl, content) => `
<html>
<head>
<style>
.controls-section {
    text-align: center;
}
</style>
</head>
<body>
  <div
    style="
      padding: 10px;
      border-radius: 10px;
      border: 1px solid rgba(0, 0, 0, 0.1);
      width: 500px;
      box-shadow: 3px 4px 4px rgba(0, 0, 0, 0.05);
    "
  >
    <div
      style=""
    >
      <div style="margin-left: 210px">
          <img
            src="https://idde.me/img/logo.png"
            height="50"
          />
      </div>
      <h2  style="text-align: center; margin-top: 16px">
        ${content}
      </h2>
    </div>
    <h3 style="text-align: center; margin-top: 16px">
     
    </h3>
    <div class="controls-section">
      <a href="${redirectUrl}">
        <button
          id="button-link"
          style="
            background-color: #3e79f7;
            color: white;
            padding: 10px;
            border-radius: 10px;
            cursor: pointer;
            border: none !important;
          "
        >
          View content
        </button>
      </a>
    </div>
  </div>
</body>
</html>
`;
export const verifyEmailContent = (verifyAccountUrl) => `
<html>
<head>
<style>
.controls-section {
    text-align: center;
}
</style>
</head>
<body>
  <div
    style="
      padding: 10px;
      border-radius: 10px;
      border: 1px solid rgba(0, 0, 0, 0.1);
      width: 500px;
      box-shadow: 3px 4px 4px rgba(0, 0, 0, 0.05);
    "
  >
    <div
      style=""
    >
      <div style="margin-left: 210px">
          <img
            src="https://idde.me/img/logo.png"
            height="50"
          />
      </div>
      <h2
        style="
          text-align: center;
          flex: 1;
          margin-bottom: 0;
          color: #1a3353;
          margin-left: 50%;
          transform: translateX(-50%);
        "
      >
        IDDE
      </h2>
    </div>
    <h3 style="text-align: center; margin-top: 16px">
      Click on the link below to verify your account!
    </h3>
    <div class="controls-section">
      <a href="${verifyAccountUrl}{verifyToken}">
        <button
          id="button-link"
          style="
            background-color: #3e79f7;
            color: white;
            padding: 10px;
            border-radius: 10px;
            cursor: pointer;
            border: none !important;
          "
        >
          Click to verify
        </button>
      </a>
    </div>
  </div>
</body>
</html>
`;

export const changePasswordContent = (resetPasswordUrl) => `
<html>
<head>
<style>
.controls-section {
    text-align: center;
}
</style>
</head>
<body>
  <div
    style="
      padding: 10px;
      border-radius: 10px;
      border: 1px solid rgba(0, 0, 0, 0.1);
      width: 500px;
      box-shadow: 3px 4px 4px rgba(0, 0, 0, 0.05);
    "
  >
      <div
        style="display: flex; justify-content: center"
      >
        <span style="margin-left: 210px">
          <img
            
            src="https://idde.me/img/logo.png"
            height="50"
          />
        </span>

      </div>
      <h2
        style="
          text-align: center;
          flex: 1;
          margin-bottom: 0;
          color: #1a3353;
        "
      >
        IDDE
      </h2>
  
    <h3 style="
        text-align: center; 
        margin-top: 16px;
       
        "
       >
      Welcome to IDDE mailer system, You can sign in using this username: <b>{username}</b>.
      Click on the link below to change your password!
    </h3>
    <div class="controls-section">
     <a href="${resetPasswordUrl}{verifyToken}">
        <button
          style="
            background-color: #3e79f7;
            color: white;
            padding: 10px;
            border-radius: 10px;
            cursor: pointer;
            border: none !important;
          "
        >
          Click to verify
        </button>
      </a>
    </div>
  </div>
</body>
</html>
`;

