import {config} from '@src/config';
import {Strategy as OAuth2Strategy} from 'passport-oauth2';
import passport from "passport";

passport.serializeUser(function (user, done) {
  done(null, user);
});
passport.deserializeUser(function (user, done) {
  done(null, user);
});

const passportOAuth2Verify = async (accessToken, refreshToken, profile, done) => {
  try {
    done(null, {accessToken, refreshToken});
  } catch (error) {
    done(error, false);
  }
};

const oauth2Options = {
  authorizationURL: config.oauth2.authorizationURL,
  tokenURL: config.oauth2.tokenURL,
  clientID: config.oauth2.clientID,
  clientSecret: config.oauth2.clientSecret,
  callbackURL: (config.env === "test") ? config.oauth2.testUrl : config.oauth2.callbackURL,
}

export const oAuth2Strategy = new OAuth2Strategy(oauth2Options, passportOAuth2Verify);
