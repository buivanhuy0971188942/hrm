const isProd = process.env.NODE_ENV === "production";
export const BASE_URL = isProd ? "http://localhost:3000/api" : "http://<production_base>/api";


export const API_ENDPOINTS = {
  // AUTH
  LOGIN: BASE_URL + "/auth/login",
  SIGNUP: BASE_URL + "/auth/signup",
  RESET_PASSWORD: BASE_URL + "/auth/reset-password",
  // GROUPS
  GROUPS: BASE_URL + "/groups",
  GROUP: (groupId: string) : string => BASE_URL + "/group/" + groupId,
  // ACCOUNTS
  ACCOUNTS: BASE_URL + "/accounts",
  ACCOUNT: (groupId: string) : string => BASE_URL + "/accounts/" + groupId,
}