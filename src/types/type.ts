import { Request, Response, NextFunction } from "express";
import { PaginateOptions } from "mongoose";
import {Authenticator} from "@src/services/AuthService";
import {AccountDocument} from "@src/models/accounts/account.model";

export interface ExtendedRequest extends Request {
  tokens?: any
  account?: AccountDocument,
  files?: any,
  data?: any,
  authenticator: Authenticator
}

export interface ExtendedResponse extends Response {
  errorMessage?: string
}

export interface ExtendedNextFunction extends NextFunction {

}

export interface DbQueryOptions extends PaginateOptions {
  nullable?: boolean,
  uniqueFields?: string[],
  many?: boolean,
  ability?: any,
  customError?: {
    message: string
    code: number
  },
  filterAsDoc?: any,
  excludedFilterField?: string[]
}

export interface ApiResponse {
  message?: string,
  data?: any
}

