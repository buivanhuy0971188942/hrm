import httpStatus from 'http-status';
import {ApiError} from '@src/utils/api-error';
import {ExtendedRequest, ExtendedResponse} from '@src/types/type';
import {DbService} from '@src/services/DbService';
import {queryFilter} from '@src/utils/filter';
import {GitLabService} from "@src/services/GitLabService";
import {config} from "@src/config";
import {handleRefreshToken} from "@src/controllers/gitlab.controller";
import {deleteFile, processFile} from "@src/utils/google-storage";

export const getProjects = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const filter = queryFilter(req.query, ["slug"]);
  Object.assign(filter, req.authenticator.filterByOwnResource);
  const projects = await DbService.paginateDocsByFilter("ProjectModel", filter, req, {
    populate: [{
      path: "members",
      populate: {
        path: "member",
        select: "fullName avatar id"
      }
    }, {
      path: "creator",
      select: "id avatar fullName"
    }]
  });
  for (const doc of projects["data"]) {
    let exc = [];
    exc.push(
      DbService.countDocumentByFilter("TaskModel", {project: doc._id}),
      DbService.countDocumentByFilter("TaskModel", {project: doc._id, state: "COMPLETED"}),
      DbService.countDocumentByFilter("IssueModel", {project: doc._id}),
      DbService.countDocumentByFilter("ProjectMemberModel", {project: doc._id})
    )
    const [taskCount, completedTaskCount, issueCount, memberCount] = await Promise.all(exc);
    Object.assign(doc["_doc"], {
      taskCount,
      completedTaskCount,
      issueCount,
      memberCount,
      ownRole: doc["$$populatedVirtuals"]["members"].find(data => data.id === req.account.id)
    });
  }

  res.json(projects);
};

export const getProject = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const filter = {id: req.params.projectId}
  Object.assign(filter, req.authenticator.filterByOwnResource);

  const project = await DbService.getDocByFilter("ProjectModel", filter);
  res.json({data: project});
};

export const getProjectResources = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const filter = {id: req.params.projectId}
  Object.assign(filter, req.authenticator.filterByOwnResource);

  const project = await DbService.getDocByFilter("ProjectModel", filter, {
    lean: true,
    customError: {
      code: httpStatus.FORBIDDEN,
      message: "Forbidden"
    },
    populate: [{
      path: "members",
      select: "member role",
      populate: {
        path: "member",
        select: "fullName avatar id"
      },
      options: {limit: 5}
    }, {
      path: "labels"
    }, {
      path: "issues"
    }]
  });
  const boards = await DbService.getDocByFilter("BoardModel", {...req.body, project: project._id}, {
    many: true, sort: "index", nullable: true, populate: [{
      path: "tasks", populate: [
        {path: "labels"},
        {path: "assignedTo", select: "id fullName avatar"}
      ],
      options: {
        sort: "index"
      }
    }]
  });

  const [taskCount, completedTaskCount, issueCount, memberCount] = await Promise.all([
    DbService.countDocumentByFilter("TaskModel", {project: project._id}),
    DbService.countDocumentByFilter("TaskModel", {project: project._id, state: "COMPLETED"}),
    DbService.countDocumentByFilter("IssueModel", {project: project._id}),
    DbService.countDocumentByFilter("ProjectMemberModel", {project: project._id})
  ]);
  Object.assign(project, {
    taskCount,
    completedTaskCount,
    issueCount,
    memberCount,
    ownRole: project["members"].find(data => data.id === req.account.id)
  });
  res.json({data: {...project, boards}});
}

export const addProject = async (req: ExtendedRequest, res: ExtendedResponse) => {
  try {
    req.body.creator = req.account._id;
    if (req.files) req.body.image = (await processFile(req.files[0])).path;
    const project = await DbService.addDoc("ProjectModel", req.body, {uniqueFields: ["name"]});
    await DbService.addDoc("ProjectMemberModel", {
      project: project._id,
      member: req.account._id,
      role: "MAINTAINER"
    }, {populate: {path: "creator", select: "fullName avatar id"}});

    res.json({data: project});
  } catch (e) {
    if (req.body.image) await deleteFile(req.body.image);
    throw e;
  }
};

export const updateProject = async (req: ExtendedRequest, res: ExtendedResponse) => {
  try {
    const filter = {id: req.params.projectId}
    Object.assign(filter, req.authenticator.filterByOwnResource);

    let project = await DbService.getDocByFilter("ProjectModel", filter, {
      customError: {
        code: httpStatus.FORBIDDEN,
        message: "Forbidden"
      },
      populate: {path: "creator", select: "fullName avatar id"}
    }), fileToDelete;

    if (req.files) {
      req.body.image = (await processFile(req.files[0])).path;
      if (project.image && project.image.includes("https://storage.googleapis.com")) fileToDelete = project.image;
    }
    Object.assign(project, req.body);
    project = await project.save();
    if (fileToDelete) await deleteFile(fileToDelete);
    res.json({data: project});
  } catch (e) {
    if (req.body.image) await deleteFile(req.body.image);
    throw e;
  }
};

export const deleteProject = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const filter = {id: req.params.projectId}
  Object.assign(filter, req.authenticator.filterByOwnResource);
  const project = await DbService.deleteDocByFilter("ProjectModel", filter, {
    customError: {
      code: httpStatus.FORBIDDEN,
      message: "Forbidden"
    },
    populate: {path: "creator", select: "fullName avatar id"}
  });
  if (project.image && project?.image.includes("https://storage.googleapis.com")) await deleteFile(project.image);
  res.json({data: project});
};

export const addMembers = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const filter = {id: req.params.projectId}
  Object.assign(filter, req.authenticator.filterByOwnResource);
  const project = (await DbService.getDocByFilter("ProjectModel", filter, {
    customError: {
      code: httpStatus.FORBIDDEN,
      message: "Forbidden"
    },
  }))["_id"];
  if (await DbService.getDocByFilter("ProjectMemberModel", {member: req.body.member, project}, {nullable: true})) {
    throw new ApiError(httpStatus.BAD_REQUEST, "This accounts has already been added to this project");
  }
  const data = await DbService.addDoc("ProjectMemberModel", {...req.body, project}, {
    populate: {
      path: "member",
      select: "fullName avatar id"
    }
  })
  res.json({data});
};

export const updateMembers = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const filter = {id: req.params.projectId}
  Object.assign(filter, req.authenticator.filterByOwnResource);
  const project = (await DbService.getDocByFilter("ProjectModel", filter, {
    customError: {
      code: httpStatus.FORBIDDEN,
      message: "Forbidden"
    },
  }))["_id"];
  const data = await DbService.updateDocByFilter("ProjectMemberModel", {
    member: req.body.member,
    project
  }, req.body, {populate: {path: "member", select: "fullName avatar id"}})
  res.json({data});
};

export const deleteMembers = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const project = await DbService.getDocByFilter("ProjectModel", {id: req.params.projectId.toString()}, {populate: "members"});
  const currentMember = project.members.find(data => data.member.toString() === req.account._id.toString());
  let data;
  if (currentMember && project.creator.toString() !== currentMember.member.toString()) {
    if (project.creator.toString() === req.body.member.toString() || (req.body.member.toString() !== req.account._id.toString() && currentMember.role !== "MAINTAINER")) {
      throw new ApiError(httpStatus.FORBIDDEN, "Failed to delete member");
    }
  } else if (req.body.authorizedMember && project.members.length > 1) {
    await DbService.updateDocByFilter("ProjectMemberModel", {
      member: req.body.authorizedMember,
      project: project._id
    }, {role: "MAINTAINER"});
    Object.assign(project, {creator: req.body.authorizedMember});
    await project.save();
  } else {
    throw new ApiError(httpStatus.BAD_REQUEST, "Please delete the project instead");
  }

  data = await DbService.deleteDocByFilter("ProjectMemberModel", {
    member: req.body.member,
    project: project._id
  }, {populate: {path: "member", select: "fullName avatar id"}});
  res.json({data: data.member});
};

export const linkProjectToGitLab = handleRefreshToken(async (req: ExtendedRequest, res: ExtendedResponse) => {
  const filter = {id: req.params.projectId}
  Object.assign(filter, req.authenticator.filterByOwnResource);
  let project = await DbService.getDocByFilter("ProjectModel", filter, {
    customError: {
      code: httpStatus.FORBIDDEN,
      message: "Forbidden"
    },
  });

  let check = await DbService.countDocumentByFilter("ProjectModel", {
    id: {$ne: req.params.projectId.toString()},
    "gitLabData.projectId": req.body.projectId
  },);
  if (check > 0) throw new ApiError(httpStatus.BAD_REQUEST, "Cannot connect to this Gitlab project");

  const hook = await GitLabService.addProjectHook(req.account.gitLabAccessToken, req.body.projectId, {
    url: `${config.system.baseUrl}/webhook/gitlab`,
    push_events: true,
    issues_events: true
  });

  Object.assign(project, {
    gitLabData: {
      projectId: req.body.projectId,
      namespace: req.body.namespace,
      webUrl: req.body.web_url,
      projectName: req.body.name,
      sshUrl: req.body.ssh_url_to_repo,
      httpUrl: req.body.http_url_to_repo,
      createdAt: req.body.created_at,
      webhookId: hook.id
    }
  });
  project = await project.save();
  const {data: issuesFromGitLab} = await GitLabService.getProjectIssues(req.account.gitLabAccessToken, req.body.projectId, {pagination: false});
  let issues = (await DbService.getDocByFilter("IssueModel", {project: project._id}, {
    many: true,
    nullable: true,
    lean: true
  })).map(data => data.id);
  issues = issuesFromGitLab.filter(data => !issues.includes(data.id.toString())).map(data => {
    return {
      ...data,
      project: project._id,
      createdTimeOnGitLab: data.created_at,
      closedAt: data.closed_at,
      gitLabProjectId: data.project_id
    }
  });
  await DbService.addDoc("IssueModel", issues);
  res.json({data: project});
});

export const disconnectFromGitLab = handleRefreshToken(async (req: ExtendedRequest, res: ExtendedResponse) => {
  const filter = {id: req.params.projectId}
  Object.assign(filter, req.authenticator.filterByOwnResource);
  let project = await DbService.getDocByFilter("ProjectModel", filter, {
    customError: {
      code: httpStatus.FORBIDDEN,
      message: "Forbidden"
    },
  });

  if (!project.gitLabData) throw new Error("This project was not connected to any Gitlab project");

  if (project.gitLabData.webhookId) await GitLabService.deleteProjectHook(req.account.gitLabAccessToken, project.gitLabData.projectId, project.gitLabData.webhookId);

  Object.assign(project, {gitLabData: null});
  project = await project.save();
  const issues = (await DbService.getDocByFilter("IssueModel", {project: project._id}, {
    many: true,
    nullable: true
  })).map(data => data._id);

  if (!req.body.allowDeleteTasks) await DbService.updateDocByFilter("TaskModel", {issue: {$in: issues}}, {issue: null}, {many: true});
  else await DbService.deleteDocByFilter("TaskModel", {issue: {$in: issues}}, {many: true});
  await DbService.deleteDocByFilter("IssueModel", {project: project._id}, {many: true});

  res.json({data: project});
});

export const getProjectMembers = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const data = (await DbService.getDocByFilter("ProjectModel", {id: req.params.projectId.toString()}, {
    populate: {
      path: "members",
      populate: {path: "member", select: "fullName avatar id"}
    }
  }))["members"];
  res.json({data});
};