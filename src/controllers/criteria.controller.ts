import { ExtendedRequest, ExtendedResponse } from '@src/types/type';
import { DbService } from '@src/services/DbService';

export const getAllCriteria = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const criteria = await DbService.paginateDocsByFilter("CriteriaModel", {}, req);
  res.json(criteria);
};

export const getCriteria = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const criteria = await DbService.getDocByFilter("CriteriaModel", { id: req.params.criteriaId });
  res.json({data: criteria});
};

export const addCriteria = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const criteria = await DbService.addDoc("CriteriaModel", req.body, { uniqueFields: ["name"] });
  res.json({data: criteria});
};

export const updateCriteria = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const criteria = await DbService.updateDocByFilter("CriteriaModel", { id: req.params.criteriaId }, req.body, { uniqueFields: ["name"] });
  res.json({data: criteria});
};

export const deleteCriteria = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const criteria = await DbService.deleteDocByFilter("CriteriaModel", { id: req.params.criteriaId });
  res.json({data: criteria});
};