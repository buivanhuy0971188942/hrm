import { ExtendedRequest, ExtendedResponse } from '@src/types/type';
import { catchAsync } from '@src/utils/catch-async';

export const getRoles = catchAsync(async (req: ExtendedRequest, res: ExtendedResponse) => {
    
});

export const getRole = catchAsync(async (req: ExtendedRequest, res: ExtendedResponse) => {

});

export const addRole = catchAsync(async (req: ExtendedRequest, res: ExtendedResponse) => {

});

export const updateRole = catchAsync(async (req: ExtendedRequest, res: ExtendedResponse) => {

});

export const deteteRole = catchAsync(async (req: ExtendedRequest, res: ExtendedResponse) => {

});