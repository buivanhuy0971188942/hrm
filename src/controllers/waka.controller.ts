import {ExtendedRequest, ExtendedResponse} from "@src/types/type";
import {ApiError} from "@src/utils/api-error";
import {WakaService} from "@src/services/WakaService";
import {AccountModel} from "@src/models";
import {WakaConnection, WakaConnectionModel} from "@src/models/accounts/waka-connection.model";
import {credential} from "firebase-admin";
import refreshToken = credential.refreshToken;

export const wakaConnect = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const {code, redirect_uri, grant_type} = req.body;
  console.log(code, redirect_uri, grant_type);
  if (!code || code === "") throw new ApiError(400, "Invalid authentication code.");
  try {
    const {data} = await WakaService.getToken(code, redirect_uri);
    const {access_token, expires_at, expires_in, refresh_token, scope, token_type, uid} = data;
    const doc = await WakaConnectionModel.findOneAndUpdate({
      owner: req.account._id,
      wakaId: uid,
    }, {
      owner: req.account._id,
      wakaId: uid,
      expirationTime: new Date(expires_at),
      token: access_token,
      refreshToken: refresh_token,
      scope,
      tokenType: token_type,
    }, {
      upsert: true,
      new: true,
      setDefaultsOnInsert: true
    });
    req.account.wakaId = uid;
    await req.account.save();
    res.json(data);
  } catch (e) {
    console.log(e.message);
    throw new ApiError(503, `Failed to connect with WakaTime Service`);
  }
}

export const wakaDisconnect = async (req: ExtendedRequest, res: ExtendedResponse) => {
  try {
    // req.account.wakaId = null;
    const doc = await WakaConnectionModel.findOneAndDelete({
      owner: req.account._id,
      wakaId: req.account.wakaId,
    });
    res.json({
      message: `Disconnected successfully`,
    });

    Object.assign(req.account, {wakaId: null});
    await req.account.save();
  } catch (e) {
    throw new ApiError(500, `Cannot disconnect WakaTime due to unknown reason.`);
  }
}

export const wakaGetStat = async (req: ExtendedRequest, res: ExtendedResponse) => {
  // const doc = await WakaConnectionModel.create({
  //     wakaId: "cae86aba-5456-43be-8f97-465ff83cc41e",
  //     owner: "628350f0b59fa7c2ec54d255",
  //     token: "sec_jhIy0fbKpPH5OzqhqWTUKLWzYKHtnX7d6p4aHdKTlRHRVNXkHOHs3VSgNe3PUnk3VBHRXN4KoLrVdi3m",
  //      refreshToken: "ref_DYIqB7v5MeYJLpNldL8xNo0wXxpyLBKS6qrCwPKO5ZJozrgphvzFCmld9JZBfv47KUb4wO9UQGjlFg0i"
  //   })
  // ;
  const wakaConnections: WakaConnection[] = await WakaConnectionModel.find({}).populate('owner');
  const wakaStats = [];
  for (const wakaConnection of wakaConnections) {
    const data = await WakaService.getStat(wakaConnection["wakaId"], wakaConnection["token"], wakaConnection["refreshToken"]);
    wakaStats.push({
      totalSeconds: data.total_seconds,
      fullName: wakaConnection["owner"].fullName,
    })
  }
  res.json({
    data: wakaStats.sort((a, b) => {
      return b.totalSeconds - a.totalSeconds;
    }),
  })
}

export const wakaRefresh = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const wakaConnection = await WakaConnectionModel.findOne({
    owner: req.account._id
  });
  try {
    const wakaRefresh = await WakaService.refreshToken(wakaConnection['refreshToken']);
    res.json({
      data: wakaRefresh,
    })
  } catch (e) {
    throw new ApiError(500, `Cannot refresh Token WakaTime.`);
  }
}
