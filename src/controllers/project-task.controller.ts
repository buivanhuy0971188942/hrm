import {ExtendedRequest, ExtendedResponse} from '@src/types/type';
import {DbService} from '@src/services/DbService';
import {queryFilter} from "@src/utils/filter";
import {TaskDocument} from "@src/models/tasks/task.model";
import moment from "moment";

export const addTask = async (req: ExtendedRequest, res: ExtendedResponse) => {
  req.body.creator = req.account._id;
  const board = (await DbService.getDocByFilter("BoardModel", {id: req.params.boardId}))["_id"];
  const project = (await DbService.getDocByFilter("ProjectModel", {id: req.body.project}))["_id"];
  const task = await DbService.addDoc("TaskModel", {...req.body, board, project, type: 'PROJECT'}, {
    populate: [
      {path: "board"},
      {path: "labels"},
      {path: "assignedTo", select: "id fullName avatar"}
    ]
  });

  res.json({data: task});
}

export const getTasks = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const filter: any = queryFilter(req.query, ["start", "end", "range", "issue"]);
  if (filter.issue) filter.issue = (await DbService.getDocByFilter("IssueModel", {id: filter.issue}))["_id"];
  if (req.params.projectId) filter.project = (await DbService.getDocByFilter("ProjectModel", {id: req.params.projectId}))["_id"];
  Object.assign(filter, (req.account.role === "admin") ? {} : {assignedTo: req.account._id})

  const task = await DbService.paginateDocsByFilter("TaskModel", {...filter, type: 'PROJECT'}, req, {
    nullable: true,
    many: true,
    populate: [
      {path: "labels"},
      {path: "assignedTo", select: "id fullName avatar"},
      {path: "project"}
    ]
  });

  res.json(task);
}

export const getTaskCalendar = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const filter = queryFilter(req.query, ["start", "end", "range"]);
  let tasks = (await DbService.getDocByFilter("TaskModel", {
    ...filter,
    assignedTo: req.account._id,
    dueDate: {$ne: null},
    state: {$ne: "COMPLETED"},
    type: 'PROJECT'
  }, {
    populate: {
      path: "project"
    },
    nullable: true,
    many: true,
    lean: true,
    sort: "dueDate"
  })).reduce((prev, cur, i) => {
    let date = `${moment(cur.dueDate).format("DD-MM")}`
    return prev[date] && prev[date].length < 3 ? prev[date].push(cur) : prev[date] = [cur], prev
  }, {});
  res.json({data: tasks});
}

export const getTask = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const task = await DbService.getDocByFilter("TaskModel", {id: req.params.taskId, type: 'PROJECT'}, {
    populate: [
      {path: "labels"},
      {path: "assignedTo", select: "id fullName avatar"}
    ]
  });

  res.json({data: task});
}

export const updateTask = async (req: ExtendedRequest, res: ExtendedResponse) => {
  if (req.body.issue) {
    if (req.body.issue === "unlink") req.body.issue = null;
    else await DbService.getDocByFilter("IssueModel", {_id: req.body.issue});
  }

  const task = await DbService.updateDocByFilter("TaskModel", {id: req.params.taskId}, req.body, {
    populate: [
      {path: "labels"},
      {path: "assignedTo", select: "id fullName avatar"}
    ]
  });

  res.json({data: task});
}
export const deleteTask = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const task = await DbService.deleteDocByFilter("TaskModel", {id: req.params.taskId});
  res.json({data: task});
}

export const updateTaskIndex = async (req: ExtendedRequest, res: ExtendedResponse) => {
  let exc = [];
  if (req.body.afterBoard !== req.body.beforeBoard) {
    const beforeTasks = await DbService.getDocByFilter("TaskModel", {
      board: req.body.beforeBoard,
      _id: {$ne: req.body.task}
    }, {lean: true, sort: "index", many: true, nullable: true});
    exc = beforeTasks.map((data, index) => {
      const id = data._id;
      delete data._id;

      return {
        updateOne: {
          filter: {_id: id},
          update: {
            board: req.body.beforeBoard,
            index: index
          },
        }
      }
    });
  }
  const tasksAfter = await DbService.getDocByFilter("TaskModel", {
    board: req.body.afterBoard,
    _id: {$ne: req.body.task}
  }, {lean: true, sort: "index", many: true, nullable: true}) as TaskDocument[];
  let task = await DbService.getDocByFilter("TaskModel", {_id: req.body.task}, {lean: true});

  tasksAfter.splice(req.body.index, 0, task);

  exc = [...exc, ...(tasksAfter.map((data, index) => {
    const id = data._id;
    delete data._id;

    return {
      updateOne: {
        filter: {_id: id},
        update: {
          board: req.body.afterBoard,
          index: index
        },
      }
    }
  }))];
  await DbService.bulkWriteDoc("TaskModel", exc);
  task = await DbService.getDocByFilter("TaskModel", {_id: req.body.task}, {
    lean: true,
    nullable: true
  }) as TaskDocument;
  res.json({data: task});
};
