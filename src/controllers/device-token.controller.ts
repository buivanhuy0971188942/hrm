import {ExtendedRequest, ExtendedResponse} from '@src/types/type';
import {DbService} from '@src/services/DbService';

export const addDeviceToken = async (req: ExtendedRequest, res: ExtendedResponse) => {
  let data = await DbService.getDocByFilter("DeviceTokenModel", {account: req.account._id,}, {nullable: true});

  if (!data) {
    const deviceTokenBody = Object.assign(req.body, {account: req.account._id});
    data = await DbService.addDoc("DeviceTokenModel", deviceTokenBody);
  } else {
    data.tokens = data.tokens.concat(req.body.tokens.filter(token => !data.tokens.includes(token)));
    await data.save();
  }

  res.json({data});
};

export const getDeviceTokens = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const data = await DbService.getDocByFilter("DeviceTokenModel", {account: req.account._id}, {
    populate: [
      {path: "account", select: "id fullName avatar"},
    ],
    many: true, nullable: true
  });
  res.json({data});
};

export const deleteDeviceToken = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const data = await DbService.deleteDocByFilter("DeviceTokenModel", {id: req.params.tokenId}, {
    populate: {path: "sender", select: "id fullName avatar"}
  });
  res.json({data});
};
