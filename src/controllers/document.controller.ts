import { ExtendedRequest, ExtendedResponse } from "@src/types/type";
import { processFile } from "@src/utils/google-storage";
import { DbService } from "@src/services/DbService";
import { uniqueStringGenerator } from "@src/utils/slugify";
import { DocumentFolderModel, DocumentFileModel } from "@src/models";
import { ApiError } from "@src/utils/api-error";
import httpStatus from "http-status";

const getBreadCrumb = (doc: any) => {
  const result = [];
  for (const folder of doc.folderParentIds) {
    result.push({ folderName: folder.folderName, folderId: folder._id });
  }
  result.push({ folderName: doc.folderName, folderId: doc._id });

  return result;
};

const checkDocIsDeleted = (doc: any) => {
  if (doc.isDeleted) return true;

  for (const folder of doc.folderParentIds) {
    if (folder?.isDeleted) return true;
  }

  return false;
};

export const addNewDocFile = async (
  req: ExtendedRequest,
  res: ExtendedResponse
) => {
  let files = [];
  for (const file of req.files) {
    files.push(await processFile(file));
  }
  files[0].creator = req.account._id;
  const docBody = {
    ...files[0],
    folderParentIds: req.body.folderParentIds,
  };

  const data = await DbService.addDoc("DocumentFileModel", docBody);
  req.query.folderId && await DocumentFolderModel.updateOne(
    { _id: req.query.folderId },
    { $push: { files: data._id } }
  );

  res.json({ data: data });
};

export const addNewDocFolder = async (
  req: ExtendedRequest,
  res: ExtendedResponse
) => {
  const id = await uniqueStringGenerator(
    "code",
    "DocumentFolderModel",
    "",
    "id"
  );
  const docBody = {
    id: id,
    folderName: req.body.folderName,
    folderParentIds: req.body.folderParentIds,
  };
  const data = await DbService.addDoc("DocumentFolderModel", docBody);
  req.query.folderId && await DocumentFolderModel.updateOne(
    { _id: req.query.folderId },
    { $push: { folders: data._id } }
  );

  return res.json({ data: data });
};

export const getDocs = async (
  req: ExtendedRequest,
  res: ExtendedResponse
) => {
 
  const filter = {
    isDeleted: false
  };
  const options = {
    select: '_id folderName folderParentIds',
    many: true,
    nullable: true
  };

  switch(req.params.placeholder) {
    case 'all': 
      Object.assign(filter, {folderParentIds: {$exists:true, $size:0}})
      break;
    case 'trash': 
      Object.assign(filter, {isDeleted: true})
      break;
    default:
      throw new ApiError(httpStatus.BAD_REQUEST, "Invalid placeholder");
  }
  const folders = await DbService.getDocByFilter(
    "DocumentFolderModel",
    filter,
    options
  );
  const files = await DbService.getDocByFilter(
    "DocumentFileModel",
    filter,
    {select: '_id fileName path status', many: true,  nullable: true}
  );

  res.json({ data: {folders: folders, files: files} });
};

export const getDoc = async (
  req: ExtendedRequest,
  res: ExtendedResponse
) => {
  const filter = {
    _id: req.params.folderId,
  };
  const options = {
    select: '_id folderName folderParentIds',
    populate: [
      { path: "files", model: "DocumentFile", match: { isDeleted: false }, select: '_id fileName path status'},
      { path: "folders", model: "DocumentFolder", match: { isDeleted: false }, select: '_id folderName folderParentIds'},
      { path: "folderParentIds", model: "DocumentFolder", select: '_id folderName folderParentIds'},
    ],
  };

  const data = await DbService.getDocByFilter(
    "DocumentFolderModel",
    filter,
    options
  );

  //check if document has been deleted
  if (checkDocIsDeleted(data))
    throw new ApiError(httpStatus.BAD_REQUEST, "document does not exist");

  const breadCrumb = getBreadCrumb(data);

  res.json({ data: data, breadCrumb: breadCrumb });
};

export const getDocFileIsPending = async (
  req: ExtendedRequest,
  res: ExtendedResponse
) => {
  const files = await DbService.getDocByFilter(
    "DocumentFileModel",
    {
      status: "pending",
      deleted: false,
    },
    {
      populate: {
        path: "creator",
        model: "Account",
      },
      many: true,
    }
  );

  res.json({ data: files });
};

export const updateFolder = async (
  req: ExtendedRequest,
  res: ExtendedResponse
) => {
     const data = await DbService.updateDocByFilter('DocumentFolderModel', {
       _id: req.params.folderId,
     }, {
       folderName: req.body.folderName
     }, {select: '_id folderName folderParentIds'})

  res.json({ data: data });
};

export const handleStatusFile = async (
  req: ExtendedRequest,
  res: ExtendedResponse
) => {
  let data = await DbService.updateDocByFilter(
    "DocumentFileModel",
    {
      _id: req.body._id,
    },
    {
      status: req.body.status,
    },
    {}
  );

  res.json({ data: data });
};

export const deleteDoc = async (
  req: ExtendedRequest,
  res: ExtendedResponse
) => {
  if (req.body.type === "folder") {
    await DbService.updateDocByFilter(
      "DocumentFolderModel",
      {
        _id: req.params.docId,
      },
      {
        isDeleted: true,
      },
      {}
    );
  } else if (req.body.type === "file") {
    await DbService.updateDocByFilter(
      "DocumentFileModel",
      {
        _id: req.params.docId,
      },
      {
        isDeleted: true,
      },
      {}
    );
  } else {
    throw new ApiError(httpStatus.BAD_REQUEST, "request is not valid");
  }

  res.json("deleted successfully");
};

export const restoreDoc = async (
  req: ExtendedRequest,
  res: ExtendedResponse
) => {
  if (req.body.type === "folder") {
    await DbService.updateDocByFilter(
      "DocumentFolderModel",
      {
        _id: req.params.docId,
      },
      {
        isDeleted: false,
      },
      {}
    );
  } else if (req.body.type === "file") {
    await DbService.updateDocByFilter(
      "DocumentFileModel",
      {
        _id: req.params.docId,
      },
      {
        isDeleted: false,
      },
      {}
    );
  } else {
    throw new ApiError(httpStatus.BAD_REQUEST, "request is not valid");
  }

  res.json("restore successfully");
};
