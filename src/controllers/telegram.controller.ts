import {ExtendedRequest, ExtendedResponse} from "@src/types/type";
import {DbService} from "@src/services/DbService";
import {TelegramService} from "@src/services/TelegramService";

export const verifyCode = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const {telegramId} = await DbService.deleteDocByFilter("TelegramLinkRequestModel", {randomCode: req.body.code});
  await DbService.updateDocByFilter("AccountModel", {_id: req.account._id}, {
    isTelegramConnected: true,
    telegramId: telegramId
  });
  await TelegramService.sendMessage(telegramId, "Liên kết tài khoản thành công");
  res.json({message: "Linked accounts successfully"});
}
