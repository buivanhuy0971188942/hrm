import {ExtendedRequest, ExtendedResponse} from '@src/types/type';
import {DbService} from '@src/services/DbService';
import {queryFilter} from "@src/utils/filter";
import {ApiError} from "@src/utils/api-error";
import httpStatus from "http-status";
import {AttendanceService} from "@src/services/AttendanceService";

export const getSelfEntranceRecords = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const filter = queryFilter(req.query, ["start", "end", "range"]);
  const options: any = queryFilter(req.query, ["pagination"]);
  if (options.sortBy) options.sortBy = "-time";
  const data = await DbService.paginateDocsByFilter("EntranceRecordModel", {
    ...filter,
    cardId: req.account.cardId
  }, req, options);
  res.json(data);
};

export const getEntranceRecords = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const filter: any = Object.assign(queryFilter(req.query, ["start", "end", "range", "user_id"]), req.authenticator.filterByOwnResource);
  const options: any = queryFilter(req.query, ["pagination", "sortBy"]);
  if (!filter.user_id) throw new ApiError(httpStatus.BAD_REQUEST, "Please provide user ID");
  if (options.sortBy) options.sortBy = "-time";

  const account = await DbService.getDocByFilter("AccountModel", {id: (Object.keys(req.authenticator.filterByOwnResource).length <= 0) ? filter.user_id : req.account.id}, {
    select: "id avatar jobInfo fullName cardId",
    populate: "jobInfo"
  });
  filter.cardId = account["cardId"];
  // if (filter.range) await AttendanceService.queryEntranceRecords(new Date(parseInt(filter.end)).toISOString().split('T')[0], [filter.cardId]);
  if (options.pagination) options.pagination = (options.pagination === "true") && true
  const data = await DbService.paginateDocsByFilter("EntranceRecordModel", filter, req, options);
  res.json({...data, account});
};

export const addEntranceRecord = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const data = await DbService.addDoc("EntranceRecordModel", {...req.body, sender: req.account._id});
  res.json({data});
};

export const getEntranceRecord = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const data = await DbService.getDocByFilter("EntranceRecordModel", {id: req.params.reportId}, {
    populate: [{path: "sender", select: "id fullName avatar"}]
  });
  res.json({data});
};

export const deleteEntranceRecord = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const data = await DbService.deleteDocByFilter("EntranceRecordModel", {id: req.params.reportId,}, {populate: "sender"});
  res.json({data});
};
