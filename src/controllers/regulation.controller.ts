import httpStatus from 'http-status';
import {ApiError} from '@src/utils/api-error';
import {queryFilter} from '@src/utils/filter';
import {ExtendedRequest, ExtendedResponse} from '@src/types/type';
import {DbService} from '@src/services/DbService';
import {config} from "@src/config";

export const addPaymentRegulation = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const data = await DbService.addDoc("PaymentRegulationModel", req.body, {uniqueFields: ["title"]});
  res.json({data});
}

export const addAttendanceRegulation = async (req: ExtendedRequest, res: ExtendedResponse) => {
  if (!(await DbService.getDocByFilter("PaymentRegulationModel", {type: "ATTENDANCE"}, {nullable: true}))) {
    await DbService.addDoc("PaymentRegulationModel", {
      type: "ATTENDANCE",
      attendance: {
        timeLimit: config.system.maxTimeLimiter,
        amount: "FULL_DAY"
      }
    });
  }
  if ((await DbService.getDocByFilter("PaymentRegulationModel", {
    type: "ATTENDANCE",
    "attendance.timeLimit": req.body.attendance.timeLimit
  }, {nullable: true}))) throw new ApiError(httpStatus.BAD_REQUEST, "This time limiter is not available");
  const data = await DbService.addDoc("PaymentRegulationModel", req.body);
  res.json({data});
}

export const getPaymentRegulations = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const filter = queryFilter(req.query, ["type", "querySearch", "range", "start", "end"]);
  if (filter["querySearch"]) {
    filter["$or"] = [];
    for (const field of ["title", "description", "content",]) {
      filter["$or"].push({[field]: new RegExp(filter["querySearch"], "i")})
    }
    delete filter["querySearch"]
  }

  const data = await DbService.paginateDocsByFilter("PaymentRegulationModel", filter, req, {excludedFilterField: ["type"]});
  res.json(data);
}

export const updatePaymentRegulation = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const data = await DbService.updateDocByFilter("PaymentRegulationModel", {id: req.params.regulationId}, req.body, {uniqueFields: ["title"]});
  res.json({data});
}

export const deletePaymentRegulation = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const data = await DbService.deleteDocByFilter("PaymentRegulationModel", {id: req.params.regulationId});
  res.json({data});
}