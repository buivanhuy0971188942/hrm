import {AccountModel, EmailModel} from "@src/models";
import {uploadEmailFileFromUrl} from "@src/utils/upload-email-file-from-url";
import {TelegramService} from "@src/services/TelegramService";

export const handleIncomingEmail = async (req, res) => {
  const receivedEmail = req.body;
  // console.log("receivedEmail", receivedEmail)
  if (receivedEmail["X-Mailgun-Incoming"] === "Yes") {
    const user = await AccountModel.findOne({
      emails: receivedEmail['recipient'].toLowerCase(),
    });
    const attachments = receivedEmail.attachments ? JSON.parse(receivedEmail.attachments) : [];
    let movedAttachments: EmailAttachment[] = [];
    for (let attachment of attachments) {
      try {
        const file = await uploadEmailFileFromUrl(attachment.url, 'email-storage', {
          generateUniqueFilename: true,
          fileName: attachment.name,
        });
        movedAttachments.push({
          name: attachment.name,
          contentType: attachment['content-type'],
          size: attachment.size,
          url: file.publicUrl(),
        })
      } catch (e) {
        console.log(`Failed to upload email file...`);
        console.error(e);
      }
    }
    await EmailModel.create({
      owner: user ? user._id : undefined,
      creationTime: new Date(receivedEmail.Date).getTime(),
      sender: receivedEmail['sender'],
      from: receivedEmail['from'],
      recipient: receivedEmail['recipient'],
      subject: receivedEmail['subject'],
      strippedText: receivedEmail['stripped-text'],
      bodyHTML: receivedEmail['body-html'],
      hasAttachment: attachments && attachments.length > 0,
      attachments: movedAttachments,
      data: receivedEmail
    });
    res.json({
      msg: 'Success'
    });
  } else {
    res.status(500).json({
      msg: "Unauthorized"
    });
  }
}
export const handleSentEmail = async (req, res) => {
  const receivedEmail = req.body;
  await TelegramService.sendMessage(1362849660, receivedEmail);
  res.json(receivedEmail);
}
