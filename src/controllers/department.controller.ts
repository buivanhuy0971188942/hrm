import {ExtendedRequest, ExtendedResponse} from '@src/types/type';
import {DbService} from '@src/services/DbService';
import {deleteFile, processFile} from "@src/utils/google-storage";
import {ApiError} from "@src/utils/api-error";
import httpStatus from "http-status";
import {queryFilter} from "@src/utils/filter";
import {DepartmentModel} from "@src/models";
import {uniqueStringGenerator} from "@src/utils/slugify";

export const getDepartments = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const filter = queryFilter(req.query, ["slug"]);
  const departments = await DbService.paginateDocsByFilter("DepartmentModel", filter, req);
  for (const department of departments.data) {
    department["_doc"].memberCount = await DbService.countDocumentByFilter("AccountModel", {department: department._id});
  }
  res.json(departments);
};

export const getDepartment = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const department = await DbService.getDocByFilter("DepartmentModel", {$or: [{id: req.params.departmentId}, {slug: req.params.departmentId}]});
  res.json({data: department});
};

export const addDepartment = async (req: ExtendedRequest, res: ExtendedResponse) => {
  try {
    if (req.files) Object.assign(req.body, {avatar: (await processFile(req.files[0])).path});
    const deployment = await DbService.addDoc("DepartmentModel", req.body, {uniqueFields: ["name"]});
    res.json({data: deployment});
  } catch (e) {
    if (req.body.avatar) await deleteFile(req.body.avatar);
    throw e;
  }
};

export const updateDepartment = async (req: ExtendedRequest, res: ExtendedResponse) => {
  try {
    if (req.files) Object.assign(req.body, {avatar: (await processFile(req.files[0])).path});
    const deployment = await DbService.updateDocByFilter("DepartmentModel", {$or: [{id: req.params.departmentId}, {slug: req.params.departmentId}]}, req.body, {uniqueFields: ["name"]});
    res.json({data: deployment});
  } catch (e) {
    if (req.body.avatar) await deleteFile(req.body.avatar);
    throw e;
  }
};

export const deleteDepartment = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const department = await DbService.getDocByFilter("DepartmentModel", {$or: [{id: req.params.departmentId}, {slug: req.params.departmentId}]});
  if (await DbService.countDocumentByFilter("AccountModel", {department: department._id})) throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, "This department was assigned to accounts")
  const deployment = await DbService.deleteDocByFilter("DepartmentModel", {id: req.params.departmentId});
  res.json({data: deployment});
};