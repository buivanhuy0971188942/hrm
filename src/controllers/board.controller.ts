import {ExtendedRequest, ExtendedResponse} from '@src/types/type';
import {DbService} from '@src/services/DbService';

export const addBoard = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const {_id: project} = await DbService.getDocByFilter("ProjectModel", {id: req.params.projectId.toString()});
  const board = await DbService.addDoc("BoardModel", {...req.body, project: project});
  res.json({data: board});
};

export const getBoards = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const project = await DbService.getDocByFilter("ProjectModel", {id: req.params.projectId.toString()}, {lean: true});
  const boards = await DbService.getDocByFilter("BoardModel", {...req.body, project: project._id}, {
    many: true, sort: "index", nullable: true, populate: [{
      path: "tasks", populate: [
        {path: "labels"},
        {path: "assignedTo", select: "id fullName avatar"}
      ],
      options: {
        sort: "index"
      }
    }]
  });

  res.json({data: boards});
};

export const updateBoard = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const board = await DbService.updateDocByFilter("BoardModel", {id: req.params.boardId}, req.body, {
    populate: {
      path: "tasks", populate: [
        {path: "labels"},
        {path: "assignedTo", select: "id fullName avatar"}
      ]
    }
  });
  Object.assign(board["_doc"], {taskCount: await DbService.countDocumentByFilter("TaskModel",{board: board._id})})
  res.json({data: board});
};

export const updateBoardIndex = async (req: ExtendedRequest, res: ExtendedResponse) => {
  let exc = [], filter = req.body.index.map(data => data.board);
  for (const data of req.body.index) {
    exc.push({
      updateOne: {
        filter: {_id: data.board},
        update: {index: data.index},
      }
    });
  }
  await DbService.bulkWriteDoc("BoardModel", exc);
  const boards = await DbService.getDocByFilter("BoardModel", {_id: {$in: filter}}, {
    many: false, sort: "index", nullable: true, populate: [{
      path: "tasks", populate: [
        {path: "labels"},
        {path: "assignedTo", select: "id fullName avatar"}
      ],
      options: {
        sort: "index"
      }
    }],
  });
  res.json({data: boards});
};

export const deleteBoard = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const board = await DbService.deleteDocByFilter("BoardModel", {id: req.params.boardId});
  res.json({data: board});
};