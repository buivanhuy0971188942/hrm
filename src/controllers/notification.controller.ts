import {ExtendedRequest, ExtendedResponse} from '@src/types/type';
import {DbService} from '@src/services/DbService';
import {NotificationService} from "@src/services/NotificationService";

// TODO: When user login with new device (in client), update list of device tokens (in server)

export const sendNotification = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const filter = (req.body.receivers === "all") ? {} : {account: {$in: req.body.receivers}};
  await NotificationService.send(filter, {title: req.body.title, body: req.body.body, action: req.body.action}, req.account._id);
  res.json({data: "Sent notification successfully"});
};

export const getNotifications = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const data = await DbService.paginateDocsByFilter("NotificationModel", {receiver: req.account._id}, req, {
    populate: [
      {path: "sender", select: "id fullName avatar"},
      {path: "receiver", select: "id fullName avatar"}
    ]
  });
  res.json(data);
};

export const getSentNotifications = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const data = await DbService.paginateDocsByFilter("NotificationModel", {sender: req.account._id}, req, {
    populate: {path: "sender", select: "id fullName avatar"}
  });
  res.json(data);
};

export const getNotification = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const data = await DbService.getDocByFilter("NotificationModel", {id: req.params.reportId}, {
    populate: [{path: "sender", select: "id fullName avatar"}]
  });
  res.json({data});
};

export const deleteNotification = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const data = await DbService.deleteDocByFilter("NotificationModel", {id: req.params.reportId}, {
    populate: {path: "sender", select: "id fullName avatar"}
  });
  res.json({data});
};
