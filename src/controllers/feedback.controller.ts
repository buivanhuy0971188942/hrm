import {ExtendedRequest, ExtendedResponse} from '@src/types/type';
import {DbService} from '@src/services/DbService';

export const getFeedbacks = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const {_id: account} = await DbService.getDocByFilter("AccountModel", {id: req.params.accountId});
  const feedback = await DbService.paginateDocsByFilter("FeedbackModel", {employee: account}, req, {
    populate: [
      {path: "employee", select: "id avatar fullName"},
      {path: "assessor", select: "id avatar fullName"},
    ]
  });
  res.json(feedback);
};

export const getFeedback = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const feedback = await DbService.getDocByFilter("FeedbackModel", {id: req.params.feedbackId}, {
    populate: [
      {path: "employee", select: "id avatar fullName"},
      {path: "assessor", select: "id avatar fullName"},
      {
        path: "records", populate: "criteria", options: {sort: '-rating'}
      },
    ]
  });

  res.json({data: feedback});
};

export const addFeedback = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const {_id: employee} = await DbService.getDocByFilter("AccountModel", {id: req.params.accountId});
  const feedback = await DbService.addDoc("FeedbackModel", {...req.body, employee, assessor: req.account._id}, {
    populate: [
      {path: "employee", select: "id fullName avatar"},
      {path: "assessor", select: "id fullName avatar"},
    ]
  });
  const fbBody = [];
  for (const record of req.body.feedbacks) {
    fbBody.push({
      ...record,
      feedback: feedback._id,
    });
  }

  await DbService.addDoc("FeedbackRecordModel", fbBody, {many: true});
  res.json({data: feedback});
};

export const updateFeedback = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const feedback = await DbService.updateDocByFilter("FeedbackModel", {id: req.params.feedbackId}, req.body, {
    populate: [
      {path: "employee", select: "id fullName avatar"},
      {path: "assessor", select: "id fullName avatar"},
    ]
  });
  res.json({data: feedback});
};

export const deleteFeedback = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const feedback = await DbService.deleteDocByFilter("FeedbackModel", {id: req.params.feedbackId});
  res.json({data: feedback});
};