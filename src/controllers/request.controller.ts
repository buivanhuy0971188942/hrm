import {ExtendedRequest, ExtendedResponse} from '@src/types/type';
import {queryFilter} from '@src/utils/filter';
import {DbService} from '@src/services/DbService';
import {ApiError} from "@src/utils/api-error";
import httpStatus from "http-status";
import {deleteFile, processFile} from "@src/utils/google-storage";
import {RequestModel} from "@src/models";
import {NotificationService} from "@src/services/NotificationService";

export const getRequests = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const docFilter = queryFilter(req.query, ["start", "end", "range", "slug", "type"]);
  if (req.query.placeholder === "inbox") {
    const requests = (await DbService.getDocByFilter("RequestReceiverModel", {receiver: req.account._id}, {
      many: true,
      nullable: true,
    })).map(data => data.request);
    Object.assign(docFilter, {$or: [{request: {$in: requests}}, {department: req.account.department}]});
  }
  const data = await DbService.paginateDocsByFilter("RequestModel", docFilter, req, {
    populate: [
      {path: "sender", select: "id fullName avatar", populate: "jobInfo"},
      {path: "tasks", populate: "project"},
      {path: "receivers", populate: {path: "receiver", select: "id avatar fullName"}},
      {path: "attachments"},
      {path: "assessor", select: "id fullName avatar", populate: "jobInfo"},
      {path: "timelineEvents"}
    ]
  });
  res.json(data);
};

export const getSentRequests = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const docFilter = queryFilter(req.query, ["start", "end", "range", "slug", "type"]);
  Object.assign(docFilter, {sender: req.account._id});
  const data = await DbService.paginateDocsByFilter("RequestModel", docFilter, req, {
    populate: [
      {path: "sender", select: "id fullName avatar", populate: "jobInfo"},
      {path: "tasks", populate: "project"},
      {path: "receivers", populate: {path: "receiver", select: "id avatar fullName"}},
      {path: "attachments"},
      {path: "assessor", select: "id fullName avatar", populate: "jobInfo"},
      {path: "timelineEvents"}
    ]
  });
  res.json(data);
};

export const addRequest = async (req: ExtendedRequest, res: ExtendedResponse) => {
  let exc = [], populate = [], attachments = [];
  populate.push({path: "sender", select: "id fullName avatar", populate: "jobInfo"});
  const request = new RequestModel(req.body);
  try {
    Object.assign(request, {sender: req.account._id});
    let receivers;
    if (!req.body.department && req.body.receivers.length <= 0) throw new ApiError(httpStatus.BAD_REQUEST, "Please provide department or receivers");
    else if (req.body.receivers.length > 0) {
      receivers = (await DbService.getDocByFilter("AccountModel", {
        _id: {$in: req.body.receivers},
        role: {$in: ["admin", "hrm", "manager"]}
      }, {
        nullable: true,
        many: true
      })).map(data => {
        return (data) && {
          receiver: data._id,
          request: request._id
        }
      });
      await DbService.addDoc("RequestReceiverModel", receivers);
      populate.push({path: "receivers", populate: {path: "receiver", select: "id avatar fullName"}});
    }
    if (req.files) {
      for (const file of req.files) {
        attachments.push({
          ...(await processFile(file)),
          creator: req.account._id,
          target: request._id,
        })
      }
      await DbService.addDoc("AttachmentModel", attachments);
      populate.push({path: "attachments"});
    }
    if (["STUDY", "OVERTIME"].includes(req.body.type) && req.body.time) {
      // if (req.body.type === "STUDY" && (!req.files || req.files.length <= 0)) throw new ApiError(httpStatus.BAD_REQUEST, "Please provide evidence for this request");
      req.body.time = (Array.isArray(req.body.time)) ? req.body.time : [req.body.time]
      for (let event of req.body.time) {
        event = JSON.parse(event);
        if (event.startTime > event.endTime || (event.endTime - event.startTime) < (15 * 60 * 1000)) throw new ApiError(httpStatus.BAD_REQUEST, "Invalid time");
        exc.push({
          insertOne: {
            document: {
              ...event,
              type: req.body.type,
              owner: req.account._id,
              request: request._id
            }
          }
        });
      }
      populate.push({path: "timelineEvents"});
    }
    if (req.body.type === "OVERTIME" && req.body.tasks) {
      const tasks = (await DbService.getDocByFilter("TaskModel", {_id: {$in: req.body.tasks}}, {
        nullable: true,
        lean: true,
        many: true
      })).map(data => data._id);
      Object.assign(request, {tasks});
      populate.push({path: "tasks", populate: "project"});
    }
    await request.save();
    await DbService.bulkWriteDoc("TimelineEventModel", exc);
    await request.populate(populate);

    await NotificationService.send((!receivers) ? {role: {$ne: "user"}} : {_id: {$in: receivers.map(data => data.receiver)}}, {
      title: "Bạn có 1 request mới.",
      body: `${req.account.fullName} đã gửi 1 yêu cầu ` + generateNotificationMessage(req.body.type),
      action: `requests/${request.id}`
    }, req.account._id);

    res.json({data: request});
  } catch (e) {
    console.log(e);
    for (const atm of attachments) await deleteFile(atm.path);
    await DbService.deleteDocByFilter("AttachmentModel", {target: request._id}, {many: true});
    throw e;
  }
};

export const getRequest = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const data = await DbService.getDocByFilter("RequestModel", {id: req.params.requestId}, {
    populate: [
      {path: "sender", select: "id fullName avatar", populate: "jobInfo"},
      {path: "tasks", populate: "project"},
      {path: "receivers", populate: {path: "receiver", select: "id avatar fullName"}},
      {path: "attachments"},
      {path: "assessor", select: "id fullName avatar", populate: "jobInfo"},
      {path: "timelineEvents"}
    ]
  });
  res.json({data});
};

export const updateRequest = async (req: ExtendedRequest, res: ExtendedResponse) => {
  try {
    let deletedFile;
    const request = await DbService.getDocByFilter("RequestModel", {id: req.params.requestId, sender: req.account._id})
    if (req.body.type === "STUDY" && req.files) {
      Object.assign(req.body, {
        attachment: (await processFile(req.files[0])).path,
      });
      if (request.attachment) deletedFile = request.attachment;
    }

    if (request.state !== "PENDING") throw new ApiError(httpStatus.FORBIDDEN, "You cannot edit this request once being processed");

    const data = await DbService.updateDocByFilter("RequestModel", {}, req.body, {
      filterAsDoc: request,
      populate: [
        {path: "sender", select: "id fullName avatar", populate: "jobInfo"},
        {path: "tasks", populate: "project"},
        {path: "receivers", populate: {path: "receiver", select: "id avatar fullName"}},
        {path: "attachments"},
        {path: "assessor", select: "id fullName avatar", populate: "jobInfo"},
        {path: "timelineEvents"}
      ]
    });
    await deleteFile(deletedFile);
    res.json({data});
  } catch (e) {
    if (req.body.attachment) await deleteFile(req.body.attachment);
    throw e;
  }
};

export const responseToRequest = async (req: ExtendedRequest, res: ExtendedResponse) => {
  Object.assign(req.body, {assessor: req.account._id});
  const request = await DbService.updateDocByFilter("RequestModel", {id: req.params.requestId}, req.body);
  await request.populate([
    {path: "sender", select: "id fullName avatar", populate: "jobInfo"},
    {path: "tasks", populate: "project"},
    {path: "receivers", populate: {path: "receiver", select: "id avatar fullName"}},
    {path: "attachments"},
    {path: "assessor", select: "id fullName avatar", populate: "jobInfo"},
    {path: "timelineEvents"}
  ])
  await DbService.updateDocByFilter("TimelineEventModel", {request: request._id}, {state: req.body.status}, {many: true});

  await NotificationService.send({_id: request.sender}, {
    title: " Yêu cầu của bạn đã được phản hồi",
    body: `Yêu cầu với id ${request.id} đã ${generateNotificationMessage(req.body.status)} bới ${req.account.fullName}.`,
    action: `requests/${request.id}`
  }, req.account._id);

  res.json({data: request});
};

export const deleteRequest = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const filter = {id: req.params.requestId}
  Object.assign(filter, req.authenticator.filterByOwnResource)
  const data = await DbService.deleteDocByFilter("RequestModel", filter, {
    populate: "sender"
  });
  res.json({data});
};

function generateNotificationMessage(value: string) {
  switch (value) {
    case "OVERTIME": {
      return "đăng kí làm thêm giờ.";
    }
    case "STUDY": {
      return "đăng kí lịch học.";
    }
    case "ABSENCE": {
      return "xin nghỉ.";
    }
    case "APPROVED": {
      return "được chấp thuận";
    }
    case "REJECTED": {
      return "bị từ chối";
    }
    default:
      return ".";
  }
}
