import {ExtendedRequest, ExtendedResponse} from '@src/types/type';
import {DbService} from '@src/services/DbService';
import {NotificationService} from "@src/services/NotificationService";

export const getReports = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const data = await DbService.paginateDocsByFilter("ReportModel", {}, req, {
    populate: {path: "sender", select: "id fullName avatar"}
  });
  res.json(data);
};

export const getSentReports = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const data = await DbService.paginateDocsByFilter("ReportModel", {sender: req.account._id}, req, {
    populate: {path: "sender", select: "id fullName avatar"}
  });
  res.json(data);
};

export const addReport = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const data = await DbService.addDoc("ReportModel", {...req.body, sender: req.account._id}, {
    populate: {path: "sender", select: "id fullName avatar"}
  });

  await NotificationService.send({role: {$ne: "user"}}, {
    title: "Bạn có 1 report mới.",
    body: `${req.account.fullName} đã gửi report.`,
    action: `reports/${data.id}`
  }, req.account._id);

  res.json({data});
};

export const getReport = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const data = await DbService.getDocByFilter("ReportModel", {id: req.params.reportId}, {
    populate: [{path: "sender", select: "id fullName avatar"}]
  });
  res.json({data});
};

export const deleteReport = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const data = await DbService.deleteDocByFilter("ReportModel", {id: req.params.reportId}, {
    populate: {path: "sender", select: "id fullName avatar"}
  });
  res.json({data});
};
