import {queryFilter} from '@src/utils/filter';
import httpStatus from 'http-status';
import {ApiError} from '@src/utils/api-error';
import {DbService} from '@src/services/DbService';
import {ExtendedRequest, ExtendedResponse} from '@src/types/type';
import request from "request";
import {EmailModel} from "@src/models";
import {EmailService} from "@src/services/EmailService";

export const addEmailAddresses = async (req: ExtendedRequest, res: ExtendedResponse) => {
  let check;
  for (const email of req.body.emails) {
    check = await DbService.getDocByFilter("AccountModel", {emails: email}, {nullable: true});
    if (check) {
      throw new ApiError(httpStatus.BAD_GATEWAY, `Email with name ${email} has already been taken`);
    }
  }
  const data = await DbService.updateDocByFilter("AccountModel", {_id: req.body.account}, {emails: req.body.emails});
  res.json({data});
};

export const sendEmail = async (req: ExtendedRequest, res: ExtendedResponse) => {
  if (!req.account.activeEmail) throw new ApiError(httpStatus.BAD_REQUEST, "Please provide an active email to use this feature.");
  req.body.from = req.account.activeEmail;
  if (req.files) {
    Object.assign(req.body, {attachment: req.files.map(data => data.filepath)});
    // console.log("Attachments", req.body.attachment);
  }
  const movedAttachments = []
  // movedAttachments.push({
  //   name: attachment.name,
  //   contentType: attachment['content-type'],
  //   size: attachment.size,
  //   url: file.publicUrl(),
  // })

  const domain = req.body.from.split("@")[1];
  const emailService = new EmailService(domain);
  await emailService.sendEmail(req.body);

  const emails = [];
  if (typeof req.body.to === "string") {
    req.body.to = [req.body.to];
  }
  for (const recipient of req.body.to) {
    emails.push({
      creationTime: new Date().getTime(),
      sender: req.body.from,
      from: req.body.from,
      recipient: recipient,
      subject: req.body.subject,
      strippedText: req.body.html.replace(/(<([^>]+)>)/gi, ""),
      data: {
        from: req.body.from,
        sender: req.body.from,
        timestamp: new Date().getTime(),
        subject: req.body.subject,
        recipient: recipient,
        domain: domain,
      },
      bodyHTML: req.body.html,
      owner: req.account._id,
      hasAttachment: req.body.attachment && req.body.attachment.length > 0,
      attachments: movedAttachments
    });
  }

  await DbService.addDoc("EmailModel", emails);

  res.json({message: "Sent email successfully"});
};

export const getEmails = async (req: ExtendedRequest, res: ExtendedResponse) => {
  if (!req.account.activeEmail) {
    throw new ApiError(httpStatus.BAD_REQUEST, "Please select an active email to use this feature.");
  }
  const filter = queryFilter(req.query, ["recipient, sender, "]);
  Object.assign(filter, {
    isDeleted: {$ne: true},
    recipient: req.account.activeEmail
  });

  switch (req.params.placeholder) {
    case "inbox":
      break;
    case "sent":
      delete filter["recipient"];
      Object.assign(filter, {sender: req.account.activeEmail});
      break;
    case "starred":
      Object.assign(filter, {isStarred: true});
      break;
    case "trash":
      Object.assign(filter, {isDeleted: true});
      break;
    default:
      throw new ApiError(httpStatus.BAD_REQUEST, "Invalid placeholder");
  }

  // console.log(filter)

  const data = await DbService.paginateDocsByFilter("EmailModel", filter, req, {
    populate: {
      path: "owner",
      select: "id avatar fullName"
    }
  });
  res.json(data);
};

export const getEmail = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const filter = {
    _id: req.params.emailId,
    isDeleted: {$ne: true}
  }
  switch (req.params.placeholder) {
    case "inbox":
      break;
    case "sent":
      delete filter["recipient"];
      Object.assign(filter, {sender: req.body.activeEmail});
      break;
    case "starred":
      Object.assign(filter, {isStarred: true});
      break;
    case "trash":
      Object.assign(filter, {isDeleted: true});
      break;
    default:
      throw new ApiError(httpStatus.BAD_REQUEST, "Invalid placeholder");
  }

  const data = await DbService.getDocByFilter("EmailModel", filter, {
    populate: {
      path: "owner",
      select: "id avatar fullName"
    }
  });

  if (!data.isSeen) await data.updateOne({isSeen: true});

  res.json({data});
};


export const updateEmail = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const data = await DbService.updateDocByFilter("EmailModel", {_id: req.params.emailId}, req.body);
  res.json({data});
};

export const downloadEmailAttachment = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const attachmentUrl = req.query.url;
  request({
    url: attachmentUrl,
    headers: {
      Authorization: `Basic ${Buffer.from(process.env.MAILGUN_KEY).toString('base64')}`
    }
  }).pipe(res);
}
