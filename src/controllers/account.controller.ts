import {ExtendedRequest, ExtendedResponse} from '@src/types/type';
import {queryFilter} from '@src/utils/filter';
import {DbService} from '@src/services/DbService';
import {deleteFile, processFile} from "@src/utils/google-storage";
import axios from "axios";
import {config} from "@src/config";
import {ApiError} from "@src/utils/api-error";
import httpStatus from "http-status";
import {AccountModel} from "@src/models";

export const getAccounts = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const filter: DocFilter = queryFilter(req.query, ["fullName", "role", "department"]);
  if (filter.role) filter.role = {
    $in: (await DbService.getDocByFilter("RoleModel", {slug: {$in: filter.role.split("$")}}, {
      many: true,
      nullable: true
    })).map(data => data.slug)
  };
  if (filter.department) filter.department = (await DbService.getDocByFilter("DepartmentModel", {slug: filter.department}))["_id"];
  const data = await DbService.paginateDocsByFilter("AccountModel", filter, req, {
    populate: ["department", "jobInfo"],
  });
  res.json(data);
};

export const searchAccounts = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const filter: DocFilter = queryFilter(req.query, ["slug", "role", "department"]);
  if (filter.role) filter.role = {
    $in: (await DbService.getDocByFilter("RoleModel", {slug: {$in: filter.role.split("$")}}, {
      many: true,
      nullable: true
    })).map(data => data.slug)
  };

  if (filter.department) filter.department = (await DbService.getDocByFilter("DepartmentModel", {slug: filter.department}))["_id"];

  const data = await DbService.paginateDocsByFilter("AccountModel", filter, req, {
    select: "id fullName avatar jobInfo",
    populate: "jobInfo",
    sort: "-id"
  },);
  res.json(data);
};

export const getAccount = async (req: ExtendedRequest, res: ExtendedResponse) => {
  let queryId: string | number = req.params.accountId;
  if (req.params.accountId === "me") queryId = req.account.id;
  const account = await DbService.getDocByFilter("AccountModel", {id: queryId}, {
    populate: ["department", "jobInfo"],
  });
  res.json({data: account});
};

export const updateAccount = async (req: ExtendedRequest, res: ExtendedResponse) => {
  if (req.body.role) {
    await DbService.getDocByFilter("RoleModel", {slug: req.body.role});
  }
  const account = await DbService.getDocByFilter("AccountModel", {id: req.params.accountId});

  if (req.body.department) req.body.department = (await DbService.getDocByFilter("DepartmentModel", {_id: req.body.department}))["_id"];
  if (req.body.activeEmail) {
    if (req.body.activeEmail === "disable") req.body.activeEmail = undefined;
    else if (!account.emails.includes(req.body.activeEmail)) throw new ApiError(httpStatus.BAD_REQUEST, "This email is not valid");
  }

  if (req.body.emails) {
    let check;
    const emails = req.body.emails.filter(data => !account.emails.includes(data));
    for (const email of emails) {
      check = await DbService.getDocByFilter("AccountModel", {emails: email}, {nullable: true});
      if (check) {
        throw new ApiError(httpStatus.BAD_REQUEST, `Email with name ${email} has already been taken`);
      }
    }
  }
  Object.assign(account, req.body);
  await account.save();
  res.json({data: account});
};

export const uploadAvatar = async (req: ExtendedRequest, res: ExtendedResponse) => {
  try {
    if (!req.files || req.files.length === 0) throw new Error("Avatar must not be empty");
    let account = await DbService.getDocByFilter("AccountModel", {id: req.params.accountId}), avatarForDelete;
    if (account.avatar && account.avatar.includes("https://storage.googleapis.com")) avatarForDelete = account.avatar;
    req.body.avatar = (await processFile(req.files[0])).path;

    account = await DbService.updateDocByFilter("AccountModel", {id: req.params.accountId}, {avatar: req.body.avatar}, {uniqueFields: ["email"]});
    if (avatarForDelete) await deleteFile(avatarForDelete);
    res.json({data: account});
  } catch (e) {
    if (req.body.avatar) await deleteFile(req.body.avatar);
    throw e;
  }
};

export const deleteAccount = async (req: ExtendedRequest, res: ExtendedResponse) => {
  try {
    const account = await DbService.updateDocByFilter("AccountModel", {$and: [{id: req.params.accountId}, {id: {$ne: req.account.id}}]}, {isDeleted: true});

    await DbService.multipleQueries(["ProjectModel", "PostModel", "TaskModel"], "updateDocByFilter", {creator: account._id}, {isDeleted: true}, {many: true});
    await DbService.multipleQueries(["TimelineEventModel", "EmailModel"], "updateDocByFilter", {owner: account._id}, {isDeleted: true}, {many: true});
    await DbService.multipleQueries(["NotificationModel", "ReportModel", "RequestModel"], "updateDocByFilter", {sender: account._id}, {isDeleted: true}, {many: true});
    await DbService.multipleQueries(["NotificationModel"], "updateDocByFilter", {receiver: account._id}, {isDeleted: true}, {many: true});
    const {data} = await axios(`${config.oauth2.baseAuthUrl}/api/v1/accounts/${account.id}/disconnect`, {method: "post"});
    res.json({data: account});
  } catch (e) {
    // console.log(e)
    throw new Error("Failed to delete accounts");
  }
};

export const restoreAccount = async (req: ExtendedRequest, res: ExtendedResponse) => {
  try {
    const account = await AccountModel.findOneAndUpdate({id: req.params.accountId}, {isDeleted: false});

    await DbService.multipleQueries(["ProjectModel", "PostModel", "TaskModel"], "updateDocByFilter", {creator: account._id}, {isDeleted: false}, {many: true});
    await DbService.multipleQueries(["TimelineEventModel", "EmailModel"], "updateDocByFilter", {owner: account._id}, {isDeleted: false}, {many: true});
    await DbService.multipleQueries(["NotificationModel", "ReportModel", "RequestModel"], "updateDocByFilter", {sender: account._id}, {isDeleted: false}, {many: true});
    await DbService.multipleQueries(["NotificationModel"], "updateDocByFilter", {receiver: account._id}, {isDeleted: false}, {many: true});
    const {data} = await axios(`${config.oauth2.baseAuthUrl}/api/v1/accounts/${account.id}/restore`, {method: "post"});
    res.json({data: account});
  } catch (e) {
    console.log(e)
    throw new Error("Failed to restore accounts");
  }
};

export const getJobInfos = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const data = await DbService.paginateDocsByFilter("JobInfoModel", {}, req);
  res.json(data);
}

export const addJobInfo = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const data = await DbService.addDoc("JobInfoModel", req.body, {uniqueFields: ["title"]});
  res.json({data});
}

export const updateJobInfo = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const data = await DbService.updateDocByFilter("JobInfoModel", {id: req.params.jobId}, req.body, {uniqueFields: ["title"]});
  res.json({data});
}

export const deleteJobInfo = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const data = await DbService.deleteDocByFilter("JobInfoModel", {id: req.params.jobId}, req.body);
  res.json({data});
}

export const getAccountConfigInfos = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const roles = await DbService.getDocByFilter("RoleModel", {}, {many: true, nullable: true});
  const jobInfo = await DbService.getDocByFilter("JobInfoModel", {}, {many: true, nullable: true});
  const departments = await DbService.getDocByFilter("DepartmentModel", {}, {many: true, nullable: true});
  res.json({
    data: {
      roles, jobInfo, departments
    }
  });
}
