import {ExtendedRequest, ExtendedResponse} from '@src/types/type';
import {DbService} from '@src/services/DbService';
import {queryFilter} from "@src/utils/filter";
import moment from "moment";
import {ApiError} from "@src/utils/api-error";
import httpStatus from "http-status";

export const getTimelineEvents = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const filter: any = queryFilter(req.query, ["start", "end", "range", "type", "user_id", "pagination"]);
  if (filter.user_id) filter.owner = (await DbService.getDocByFilter("AccountModel", {id: filter.user_id}))["_id"];
  if (filter.type) filter.type = {$in: filter.type.split("$").map(data => data.toUpperCase())};
  if (filter.range) {
    filter.start = parseInt(filter.start);
    filter.end = parseInt(filter.end);
  }
  Object.assign(filter, {state: "APPROVED"})
  const events = await DbService.paginateDocsByFilter("TimelineEventModel", filter, req, {
    populate: {path: "owner", select: "avatar fullName id"},
    sort: "startTime",
    pagination: filter.pagination
  });
  res.json(events);
};

export const getTimelineEventCalendar = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const filter: any = queryFilter(req.query, ["start", "end", "range", "type"]);
  if (filter.type) filter.type = {$in: filter.type.split("$").map(data => data.toUpperCase())};
  Object.assign(filter, {
    owner: req.account._id,
    state: "APPROVED"
  });
  let tasks = (await DbService.getDocByFilter("TimelineEventModel", filter, {
    populate: {
      path: "owner",
      select: "id avatar fullName"
    },
    nullable: true,
    many: true,
    lean: true,
    sort: "startTime"
  })).reduce((prev, cur, i) => {
    let date = `${moment(cur.startTime).format("DD-MM")}`
    return prev[date] ? prev[date].push(cur) : prev[date] = [cur], prev
  }, {});
  res.json({data: tasks});
};

export const getTimelineEvent = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const events = await DbService.getDocByFilter("TimelineEventModel", {id: req.params.eventId}, {
    populate: {
      path: "owner",
      select: "avatar fullName id"
    },
  });
  res.json({data: events});
};

export const addTimelineEvent = async (req: ExtendedRequest, res: ExtendedResponse) => {
  req.body.owner = req.account._id;

  if (req.body.startTime > req.body.endTime || (["OVERTIME", "STUDY"].includes(req.body.type) && (req.body.endTime - req.body.startTime) < (15 * 60 * 1000))) throw new ApiError(httpStatus.BAD_REQUEST, "Invalid time");

  const events = await DbService.addDoc("TimelineEventModel", req.body, {
    populate: {
      path: "owner",
      select: "avatar fullName id"
    },
  });
  res.json({data: events});
};

export const updateTimelineEvent = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const event = await DbService.getDocByFilter("TimelineEventModel", {id: req.params.eventId}, {
    populate: {
      path: "owner",
      select: "avatar fullName id"
    },
  });
  const startTime = (req.body.startTime) ? req.body.startTime : (event.startTime) ? event.startTime : null
  const endTime = (req.body.endTime) ? req.body.endTime : (event.endTime) ? event.endTime : null
  if (startTime && endTime && (startTime > event.endTime || (["OVERTIME", "STUDY"].includes(event.type) && (endTime - startTime) < (15 * 60 * 1000)))) throw new ApiError(httpStatus.BAD_REQUEST, "Invalid time");

  Object.assign(event, req.body);
  await event.save();
  res.json({data: event});
};

export const deleteTimelineEvent = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const events = await DbService.deleteDocByFilter("TimelineEventModel", {id: req.params.eventId}, {
    populate: {
      path: "owner",
      select: "avatar fullName id"
    },
  });
  res.json({data: events});
};