import {AuthService} from '@src/services/AuthService';
import {ApiError} from '@src/utils/api-error';
import {DbService} from '@src/services/DbService';
import {ExtendedRequest, ExtendedResponse} from '@src/types/type';
import httpStatus from 'http-status';
import bcrypt from "bcryptjs";
import {config} from '@src/config';
import {TelegramService} from "@src/services/TelegramService";

export const signIn = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const account = await DbService.getDocByFilter("AccountModel", {
    email: req.body.email,
  }, {select: "+password"});

  if (!bcrypt.compareSync(req.body.password, account.password)) throw new ApiError(httpStatus.BAD_REQUEST, "Invalid password");
  delete account._doc.password;
  const accessToken = AuthService.generateToken({userId: account._id, type: "access"});
  let refreshToken: string;
  if (req.body.remember) {
    refreshToken = AuthService.generateToken({userId: account._id, type: "refresh"});
    // AuthService.saveToken({ type: "refresh", token: refreshToken, accounts: accounts._id });
  }
  res.json({
    account,
    accessToken,
    refreshToken
  });
};

export const signUp = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const account = await DbService.addDoc("Account", req.body, {uniqueFields: ["username", "email"]});
  res.json({
    account
  })
};

export const logout = async (req: ExtendedRequest, res: ExtendedResponse) => {
  try {
    let deviceToken = await DbService.getDocByFilter("DeviceTokenModel", {
      account: req.account._id,
      tokens: req.body.deviceToken
    }, {nullable: true});
    if (deviceToken) {
      deviceToken.tokens.splice(deviceToken.tokens.indexOf(req.body.deviceToken), 1);
      await deviceToken.save();
    }
    res.json({message: true});
  } catch (e) {
    console.log(e);
    throw e;
  }
};

export const getAuthCode = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const code = req.query.code;
  if (!code) return res.redirect(`${config.oauth2.authorizationURL}?response_type=code&redirect_uri=${config.oauth2.callbackURL}&client_id=${config.oauth2.clientID}`);
  res.json({code});
}

export const getTokens = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const data = await AuthService.getTokensFromOAuth(config.oauth2.tokenURL, {
    grantType: req.body.grant_type,
    redirectUri: req.body.redirect_uri,
    clientID: config.oauth2.clientID,
    clientSecret: config.oauth2.clientSecret
  }, {
    code: req.body.code,
    refreshToken: req.body.refresh_token,
  });

  if (data.account.role !== "admin" && data.account.role !== "northstudio") {
    throw new ApiError(httpStatus.UNAUTHORIZED, 'Not authenticated');
  }
  if (data.account.isDeleted) throw new ApiError(httpStatus.UNAUTHORIZED, "This account is no longer available, please contact the admin for further information.");

  let account = await DbService.getDocByFilter("AccountModel", {id: data.account.id}, {nullable: true});
  if (!account) {
    let roleName = (data.account.email === config.system.adminEmail) ? "ADMIN" : "USER"
    let role = await DbService.getDocByFilter("RoleModel", {name: roleName}, {nullable: true});
    if (!role) role = await DbService.addDoc("RoleModel", {name: roleName});
    account = await DbService.addDoc("AccountModel", {
      id: data.account.id,
      fullName: data.account.fullName,
      email: data.account.email,
      role: role.slug,
    });
  }

  if (req.body.device_token || req.query.device_token) {
    const data = await DbService.getDocByFilter("DeviceTokenModel", {account: account._id,}, {nullable: true});
    if (!data) {
      const deviceTokenBody = Object.assign(req.body, {account: account._id});
      await DbService.addDoc("DeviceTokenModel", deviceTokenBody);
    } else if (data.tokens.includes(req.body.device_token)) {
      data.tokens.push(req.body.device_token);
      await data.save();
    }
  }

  res.json({
    // accounts: accounts,
    access: {
      token: data.access_token,
      expires: new Date().getTime() + data.expires_in * 1000
    },
    refresh: {
      token: data.refresh_token,
    }
  });
}

