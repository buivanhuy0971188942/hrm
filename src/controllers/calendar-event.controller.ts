import {ExtendedRequest, ExtendedResponse} from '@src/types/type';
import {DbService} from '@src/services/DbService';
import {queryFilter} from "@src/utils/filter";
import {ApiError} from "@src/utils/api-error";
import httpStatus from "http-status";
import moment from 'moment';

const getRecurrentTime = (result, event, filter, now) => {
  let start = new Date(parseInt(filter.startDate))
  let end = new Date(parseInt(filter.endDate))

  console.log(start, end, now)
  console.log(event['startTime'])
  switch (event["recurrenceType"]) {
    case "DAILY" :
      console.log('daily')
      while(event['startTime'].getTime() < end.getTime()) {
        if(event['startTime'].getTime() > now.getTime()) {
          result.push({
            ...event.toObject(),
            endTime: new Date(event['startTime'].getFullYear(), event['startTime'].getMonth(), event['startTime'].getDate(), event['endTime'].getHours(), event['endTime'].getMinutes())
          })
        }
        event['startTime'].setDate(event['startTime'].getDate() + 1)
      }
      break;
    case "WEEKLY":
      console.log('weekly')
      while(event['startTime'].getTime() < end.getTime()) {
        if(event['startTime'].getTime() > now.getTime() && event['startTime'].getTime() > start.getTime() && event['startTime'].getTime() < end.getTime()) {
          result.push({
            ...event.toObject(),
            endTime: new Date(event['startTime'].getFullYear(), event['startTime'].getMonth(), event['startTime'].getDate(), event['endTime'].getHours(), event['endTime'].getMinutes())
          })
        }
        event['startTime'].setDate(event['startTime'].getDate() + 7)
      }
      break;
    case "MONTHLY" :
      console.log('monthly')
      while (event['startTime'].getTime() < end.getTime()) {
        if (event['startTime'].getTime() > now.getTime() && event['startTime'].getTime() > start.getTime() && event['startTime'].getTime() < end.getTime()) {
          result.push({
            ...event.toObject(),
            endTime: new Date(event['startTime'].getFullYear(), event['startTime'].getMonth(), event['startTime'].getDate(), event['endTime'].getHours(), event['endTime'].getMinutes())
          })
        }
        event['startTime'].setMonth(event['startTime'].getMonth() + 1)
      }
      break;
    case "YEARLY" :
      console.log('yearly')
      while (event['startTime'].getTime() < end.getTime()) {
        if (event['startTime'].getTime() > now.getTime() && event['startTime'].getTime() > start.getTime() && event['startTime'].getTime() < end.getTime()) {
          result.push({
            ...event.toObject(),
            endTime: new Date(event['startTime'].getFullYear(), event['startTime'].getMonth(), event['startTime'].getDate(), event['endTime'].getHours(), event['endTime'].getMinutes())
          })
        }
        event['startTime'].setFullYear(event['startTime'].getFullYear() + 1)
      }
      break;
    default:
      break;
  }
}

export const getCalendarEvents = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const filter: any = queryFilter(req.query, ["startDate", "endDate", "range", "type", "user_id", "pagination", "privacy", "isRecurrent", "recurrenceType"]);
  // if (filter.user_id) {
  //   filter.owner = (await DbService.getDocByFilter("AccountModel", {id: filter.user_id}))["_id"];
  // } else if (filter.user_id === req.account.id || filter.user_id === "me") {
  //   delete filter.privacy;
  // }

  const events = await DbService.getDocByFilter("CalendarEventModel", {privacy: "PUBLIC"}, {many: true});
  let result = [];
  let now = new Date(moment().utcOffset(0).set({hour:0,minute:0,second:0,millisecond:0}).format())
  for (const event of events) {
    if (!event["isRecurrent"]) {
      if (event["startTime"].getTime() > now.getTime()) {
        result.push(event);
      }
    } else {
      getRecurrentTime(result, event, filter, now);
    }
  }
  res.json(result)
};

//[GET]: Event have privacy is PRIVATE
export const getCalendarEventPrivate = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const account = await DbService.getDocByFilter("AccountModel", {id: req.body.ownerId});
  const events = await DbService.getDocByFilter("CalendarEventModel", {id: req.params.eventId, owner: account}, {
    populate: {
      path: "owner",
      select: "avatar fullName id"
    },
  });
  res.json({data: events});
};

export const getCalendarEvent = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const events = await DbService.getDocByFilter("CalendarEventModel", {privacy: "PUBLIC", id: req.params.eventId}, {
    populate: {
      path: "owner", select: "avatar fullName id"
    },
  });
  res.json(events);
};

export const addCalendarEvent = async (req: ExtendedRequest, res: ExtendedResponse) => {
  if (req.body.startTime.getTime() > req.body.endTime.getTime()) throw new ApiError(httpStatus.BAD_REQUEST, "Invalid time");
  req.body.owner = req.account._id;
  if (req.body.type !== "PERSONAL-EVENT" && req.account.role === "user") throw new ApiError(httpStatus.BAD_REQUEST, "You not sub");
  const events = await DbService.addDoc("CalendarEventModel", req.body, {
    populate: {
      path: "owner",
      select: "avatar fullName id"
    },
  });
  res.json({data: events});
};

export const updateCalendarEvent = async (req: ExtendedRequest, res: ExtendedResponse) => {
  console.log(Object.prototype.toString.call(req.body.startTime))
  if (req.body.startTime.getTime() > req.body.endTime.getTime()) throw new ApiError(httpStatus.BAD_REQUEST, "Invalid time");
  req.body.owner = req.account._id;
  if (req.body.type !== "PERSONAL-EVENT" && req.account.role === "user") throw new ApiError(httpStatus.BAD_REQUEST, "You don't have permission to post");
  const account = await DbService.getDocByFilter("AccountModel", {id: req.body.ownerId});
  const event = await DbService.getDocByFilter("CalendarEventModel", {
    owner: account,
    id: req.params.eventId
  }, {
    populate: {
      path: "owner",
      select: "avatar fullName id"
    },
  });
  Object.assign(event, req.body);
  await event.save();
  res.json({data: event});
};

export const deleteCalendarEvent = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const account = await DbService.getDocByFilter("AccountModel", {id: req.body.ownerId});
  const events = await DbService.deleteDocByFilter("CalendarEventModel", {id: req.params.eventId, owner: account,}, {
    populate: {
      path: "owner",
      select: "avatar fullName id"
    },
  });
  res.json({data: events});
};