import {ExtendedRequest, ExtendedResponse} from '@src/types/type';
import {DbService} from '@src/services/DbService';
import {GitLabService} from "@src/services/GitLabService";
import {handleRefreshToken} from "@src/controllers/gitlab.controller";

export const getIssues = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const project = await DbService.getDocByFilter("ProjectModel", {id: req.params.projectId});
  const issues = await DbService.paginateDocsByFilter("IssueModel", {project: project._id}, req);

  for (const issue of issues.data) {
    issue["_doc"].taskCount = await DbService.countDocumentByFilter("TaskModel", {issue: issue._id});
    issue["_doc"].completedTaskCount = await DbService.countDocumentByFilter("TaskModel", {
      issue: issue._id,
      state: "COMPLETED"
    });
  }
  res.json(issues);
}

export const getIssue = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const issue = await DbService.getDocByFilter("IssueModel", {id: req.params.issueId});
  issue["_doc"].taskCount = await DbService.countDocumentByFilter("TaskModel", {issue: issue._id});
  issue["_doc"].completedTaskCount = await DbService.countDocumentByFilter("TaskModel", {
    issue: issue._id,
    state: "COMPLETED"
  });
  res.json({data: issue});
}

export const updateIssue = handleRefreshToken(async (req: ExtendedRequest, res: ExtendedResponse) => {
  let issue = await DbService.getDocByFilter("IssueModel", {id: req.params.issueId});
  const data = await GitLabService.updateProjectIssue(req.account.gitLabAccessToken, issue.gitLabProjectId, issue.iid, req.body);
  Object.assign(issue, {...req.body, ...data});
  issue = await issue.save();
  res.json({data: issue});
});

export const addIssue = handleRefreshToken(async (req: ExtendedRequest, res: ExtendedResponse) => {
  const project = await DbService.getDocByFilter("ProjectModel", {id: req.params.projectId});
  if (!project.gitLabData || !project.gitLabData.projectId) {
    throw new Error("This project was not linked to a GitLab project")
  }
  const data = await GitLabService.addProjectIssue(req.account.gitLabAccessToken, project.gitLabData.projectId, req.body);
  res.json({
    data: {
      id: data.id,
      iid: data.iid,
      title: data.title,
      description: data.description,
      state: data.state,
      project: project._id,
      createdTimeOnGitLab: data.created_at,
      closedAt: data.closed_at,
      gitLabProjectId: data.project_id,
      labels: data.labels
    }
  });
});
