import { queryFilter } from '@src/utils/filter';
import { ApiError } from '@src/utils/api-error';
import { ExtendedRequest, ExtendedResponse } from '@src/types/type';
import { DbService } from '@src/services/DbService';
import { WorkingHourModel } from '@src/models';
import { config } from '@src/config';
import httpStatus from 'http-status';
import moment from 'moment';

export const takeAttendance = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const account = await DbService.getDocByFilter("AccountModel", { id: req.params.accountId });
  const startTimeAsString = new Date(req.body.startTime);
  const endTimeAsString = new Date(req.body.endTime);

  if (req.body.startTime >= req.body.endTime || req.body.endTime > parseInt(moment(req.body.startTime).endOf('day').format("x"))) throw new ApiError(httpStatus.BAD_REQUEST, "Invalid attendance time")
  let workingHour = await DbService.getDocByFilter("WorkingHourModel", {
    startTime: {
      $gt: parseInt(moment(req.body.startTime).startOf('day').format("x")),
      $lt: parseInt(moment(req.body.startTime).endOf('day').format("x"))
    }
  }, { nullable: true });

  if (workingHour) throw new ApiError(httpStatus.BAD_REQUEST, `This employee has already been taken attendance on ${moment(startTimeAsString).format("MMMM Do, YYYY")}`)

  workingHour = new WorkingHourModel({
    startTime: req.body.startTime,
    endTime: req.body.endTime,
    employee: account._id,
    shift: req.body.shift,
    note: req.body.note
  });
  const hourDifference = (startTimeAsString.getHours() - parseInt(config.system.startWorkingTime.split(":")[0])) * 60;
  const minuteDifference = (hourDifference > 0 ? hourDifference : 0) + (startTimeAsString.getMinutes() - parseInt(config.system.startWorkingTime.split(":")[1]));
  const salary = await DbService.getDocByFilter("SalaryPaymentModel", { employee: account._id }, { sort: "-createdAt" });
  if (!salary) throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, "Please add a new salary record before taking attendance");


  let log, adjustment: number, atdRegulation;
  if (new Date(salary.createdAt).getMonth() === startTimeAsString.getMonth() && new Date(salary.createdAt).getFullYear() === startTimeAsString.getFullYear()) {
    const FULL_DAY_SALARY = (Math.round(salary.grossSalary / daysInMonth(new Date(salary.createdAt).getMonth(), new Date(salary.createdAt).getFullYear()) / 1000)) * 1000;
    const HALF_DAY_SALARY = FULL_DAY_SALARY / 2;

    if (minuteDifference > 0 && !req.body.withPermission) {
      atdRegulation = await DbService.getDocByFilter("PaymentRegulationModel", { "attendance.timeLimit": { $gt: minuteDifference } }, { sort: "attendance.timeLimit", nullable: true });
      if (req.body.shift === "ALL" && atdRegulation.attendance.amount === "FULL_DAY") {
        adjustment = FULL_DAY_SALARY;
        Object.assign(workingHour, { status: "ABSENT" });
      }
      else if ((req.body.shift !== "ALL" && atdRegulation.attendance.amount === "FULL_DAY") || atdRegulation.attendance.amount === "HALF_DAY") {
        adjustment = HALF_DAY_SALARY;
        Object.assign(workingHour, { status: "ABSENT" });
      }
      else {
        adjustment = atdRegulation.attendance.amount
        Object.assign(workingHour, { status: "LATE" });
      };
      log = await DbService.addDoc("PaymentAdjustmentModel", {
        salaryPayment: salary._id,
        regulation: atdRegulation._id,
        workingHour: workingHour._id,
        amount: adjustment,
        description: `Employee with id ${account.id} was late for ${minuteDifference} minutes, the fine was ${adjustment} VND`
      });
    }
  }

  await workingHour.save();
  res.json({ data: { workingHour, salary, log } });
}

export const getAttendanceRecords = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const filter = queryFilter(req.query, ["start", "end", "range"]);
  const employee = await DbService.getDocByFilter("AccountModel", { id: req.params.accountId });
  const data = await DbService.paginateDocsByFilter("WorkingHourModel", { ...filter, employee: employee._id }, req, {
    sort: "startTime",
    pagination: false,
    populate: "adjustment"
  });

  res.json({ data: data.data });
}

function daysInMonth(month: number, year: number) { // Use 1 for January, 2 for February, etc.
  return new Date(year, month, 0).getDate();
}

export const updateWorkingHour = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const employee = (await DbService.getDocByFilter("AccountModel", { id: req.query.accountId }))["_id"];
  const workingHour = await DbService.updateDocByFilter("WorkingHourModel", { employee }, req.body, { populate: "employee" });
  res.json({ data: workingHour });
}

export const addAdjustmentByWorkingHour = async (req: ExtendedRequest, res: ExtendedResponse) => {

}