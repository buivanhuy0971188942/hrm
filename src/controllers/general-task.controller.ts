import {ExtendedRequest, ExtendedResponse} from '@src/types/type';
import {DbService} from '@src/services/DbService';
import {deleteFile, processFile} from "@src/utils/google-storage";
import {queryFilter} from "@src/utils/filter";
import httpStatus from "http-status";
import {SubTaskModel, TaskModel} from "@src/models";
import moment from 'moment'
import mongoose from "mongoose";

interface TaskFilter {
  creator?: Account,
  members?: Account,
  project?: string,
  assigned?: string,
  owner?: string,
  state?: string,
  creationStart?: number,
  creationEnd?: number,
  startTime?: number,
  endTime?: number,
  sortBy?: string,
}

const addTrackingTask = async (task: any, req: ExtendedRequest) => {
  let changes = {}
  for(const key in req.body) {
    if(key == 'members' || key == 'comments' || key == 'board' || key == 'project' || key == 'assignedTo' || key == 'creator') continue
    if(key == 'startDate' || key == 'dueDate') {
      if(!moment(req.body[key]).isSame(task[key])) {
        changes[key] = [task[key], req.body[key]]
      }
    } else if(JSON.stringify(task[key]) !== JSON.stringify(req.body[key])) {
      changes[key] = [task[key], req.body[key]]
    }
  }

  Object.keys(changes).length > 0 && await DbService.addDoc('TrackingTaskModel', {task: task._id, changes: changes, action: 'UPDATE'})
  console.log(changes)
}

export const addGeneralTask = async (req: ExtendedRequest, res: ExtendedResponse) => {
  req.body.creator = req.account._id;
  const attachments = [];
  try {
    req.body.members = (await DbService.getDocByFilter("AccountModel", {id: {$in: ((!Array.isArray(req.body.members)) ? [req.body.members] : req.body.members).filter(data => !!data)}}, {
      nullable: true,
      many: true
    })).map(data => data._id);

    if (mongoose.isValidObjectId(req.body.project)) {
      // req.body.project = await DbService.getDocByFilter("")
    } else {
      delete req.body.project
    }

    const task = await DbService.addDoc("TaskModel", {...req.body, type: 'GENERAL'});

    if (req.files && req.files.length > 0) {
      for (const file of req.files) {
        attachments.push({
          ...(await processFile(file)),
          target: task._id,
          creator: req.account._id
        });
      }
      await DbService.addDoc("AttachmentModel", attachments, {many: true});
    }
    await task.populate([{
      path: "attachments", model: "Task"
    }, {
      path: "members", select: "id avatar fullName"
    }]);
    res.json({data: task});
  } catch (e) {
    if (attachments.length > 0) for (const data of attachments) await deleteFile(data.path);
    throw e;
  }
}

export const updateGeneralTask = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const attachments = [];
  try {
    const filter = {
      id: req.params.taskId,
      type: 'GENERAL'
    }
    // Object.assign(filter, req.authenticator.filterByOwnResource);

    let task = await DbService.getDocByFilter("TaskModel", filter, {
      populate: ["attachments", "members"]
    });

    req.body.members = (!Array.isArray(req.body.members)) ? [req.body.members] : req.body.members;
    req.body.members = (await DbService.getDocByFilter("AccountModel", {id: {$in: req.body.members.filter(data => !!data)}}, {
      nullable: true,
      many: true
    })).map(data => data._id);

    if (mongoose.isValidObjectId(req.body.project)) {
      // req.body.project = await DbService.getDocByFilter("")
    } else {
      delete req.body.project
    }
    // req.body.members = task.members.concat((Array.isArray(req.body.members))
    //   ? req.body.members.filter(data => !memberList.includes(data.toString()))
    //   : !memberList.includes(req.body.members) ? [req.body.members] : []);

    if (req.files && req.files.length > 0) {
      for (const file of req.files) {
        attachments.push({
          ...(await processFile(file)),
          target: task._id,
          creator: req.account._id
        });
      }
      await DbService.addDoc("AttachmentModel", attachments, {many: true});
    }

    let tempTask = {}
    Object.assign(tempTask, task._doc)
    Object.assign(task, req.body);
    task = task = await task.save();
    await task.populate([{
      path: "creator",
      select: "id fullName avatar"
    }, {
      path: "attachments", model: "Task"
    }, {
      path: "members", select: "id avatar fullName"
    }]);
    await addTrackingTask(tempTask, req)

    res.json({data: task});
  } catch (e) {
    if (attachments.length > 0) for (const data of attachments) await deleteFile(data.path);
    throw e;
  }
}

export const getGeneralTasks = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const taskList = await DbService.getDocByFilter("TaskModel", {}, {many: true, nullable: true});
  let filter: TaskFilter = queryFilter(req.query, ["name", "state", "owner", "creationStart", "creationEnd", "startTime", "endTime", "assigned"]);

  // Object.assign(filter, req.authenticator.filterByOwnResource);
  if (filter.state) {
    Object.assign(filter, (filter.state === "due")
      ? {
        dueDate: {$lte: (new Date()).toString()},
        state: {$ne: "COMPLETED"}
      }
      : {
        $or: [{
          dueDate: {$gte: (new Date()).toString()}
        }, {state: "COMPLETED"}]
      }
    );
  }

  if (filter.owner) {
    filter.creator = (await DbService.getDocByFilter("AccountModel", {id: filter.owner}))["_id"];
    delete filter.owner;
  }

  if (filter.assigned) {
    filter.members = (await DbService.getDocByFilter("AccountModel", {id: filter.assigned}))["_id"];
    delete filter.assigned;
  }

  if (filter.creationStart && filter.creationEnd) {
    filter = generateDateRangeFilter(filter, "creationStart", "creationEnd", "createdAt");
  }

  if (filter.startTime && filter.endTime) {
    filter = generateDateRangeFilter(filter, "startTime", "endTime", "dueDate");
  }

  const tasks = await DbService.paginateDocsByFilter("TaskModel", {
    ...filter,
    type: 'GENERAL'
  }, req, {
    populate: [{
      path: "creator",
      select: "id fullName avatar"
    }, {
      path: "attachments", model: "Attachment"
    }, {
      path: "members", select: "id avatar fullName"
    }]
  });

  for (const task of tasks.data) {
    if (new Date(task["dueDate"]) < new Date() && task["state"] !== "COMPLETED") {
      Object.assign(task["_doc"], {isDue: true});
    }
  }
  res.json(tasks);
}

function generateDateRangeFilter(filter, startProperty: string, endProperty: string, property: string) {
  if (filter.hasOwnProperty(startProperty) && filter.hasOwnProperty(endProperty)) {
    Object.assign(filter, {
      $and: [
        {[property]: {$gte: new Date(parseInt(filter[startProperty]))}},
        {[property]: {$lte: new Date(parseInt(filter[endProperty]))}}
      ]
    });
    delete filter[startProperty];
    delete filter[endProperty];
  }
  return filter;
}

export const getGeneralTask = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const filter = {
    id: req.params.taskId,
    type: 'GENERAL'
  }
  // Object.assign(filter, req.authenticator.filterByOwnResource);

  const task = await DbService.getDocByFilter("TaskModel", filter, {
    populate: [{
      path: "creator",
      select: "id fullName avatar"
    }, {
      path: "attachments", model: "Task"
    }, {
      path: "members", select: "id avatar fullName"
    }]
  });
  Object.assign(task["_doc"], {isDue: true});

  res.json({data: task});
}


export const deleteGeneralTask = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const filter = {
    id: req.params.taskId,
    type: 'GENERAL'
  }
  // Object.assign(filter, req.authenticator.filterByOwnResource);
  let task = await DbService.deleteDocByFilter("TaskModel", filter, {
    populate: ["attachments", "members"],
    customError: {
      code: httpStatus.FORBIDDEN,
      message: "Forbidden"
    },
  });

  res.json({data: task});
}

export const deleteTaskMembers = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const task = (await DbService.getDocByFilter("TaskModel", {id: req.params.taskId}, {lean: true}))["_id"];
  const member = await DbService.deleteDocByFilter("TaskMemberModel", {
    task: task,
    member: req.body.member
  }, {populate: {path: "member", select: "id fullName avatar"}});
  res.json({data: member});
}

export const deleteAttachment = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const task = (await DbService.getDocByFilter("TaskModel", {id: req.params.taskId}))["_id"];
  const atm = await DbService.deleteDocByFilter("AttachmentModel", {
    _id: req.body.attachment,
    target: task
  });
  await deleteFile(atm.path);
  res.json({data: atm});
}

//TODO: sub task
export const getSubTasks = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const task = await DbService.getDocByFilter('TaskModel', {id: req.params.taskId})
  const subTasks = await DbService.getDocByFilter('SubTaskModel', {task: task._id}, {nullable: true, many: true})
  res.json({data: subTasks})
}

export const addSubTask = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const task = await DbService.getDocByFilter('TaskModel', {id: req.params.taskId})
  let subTask = await DbService.addDoc('SubTaskModel', {task: task._id, name: req.body.name, creator: req.account._id})

  res.json({data: subTask})
} 

export const updateSubTask = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const task = await DbService.getDocByFilter('TaskModel', {id: req.params.taskId})
  let subTask = await DbService.updateDocByFilter('SubTaskModel', {task: task._id, id: req.params.subTaskId}, req.body)

  res.json({data: subTask})
}

export const deleteSubTask = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const task = await DbService.getDocByFilter('TaskModel', {id: req.params.taskId})
  const subTask = await DbService.deleteDocByFilter('SubTaskModel', {task: task._id, id: req.params.subTaskId})

  res.json({data: subTask})
}

// tracking task
export const getTrackingTask = async (req: ExtendedRequest, res: ExtendedResponse) => {
  let task = await DbService.getDocByFilter('TaskModel', {id: req.params.taskId})
  let trackingTask = await DbService.paginateDocsByFilter('TrackingTaskModel', {task: task._id}, req, {excludedFilterField: ['task']})
  
  res.json({data: trackingTask})
}