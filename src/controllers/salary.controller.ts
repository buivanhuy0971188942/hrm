import httpStatus from 'http-status';
import { ApiError } from '@src/utils/api-error';
import { queryFilter } from '@src/utils/filter';
import { ExtendedRequest, ExtendedResponse } from '@src/types/type';
import { DbService } from '@src/services/DbService';
import moment from "moment";

export const addSalaryPayment = async (req: ExtendedRequest, res: ExtendedResponse) => {
  await DbService.getDocByFilter("AccountModel", { _id: req.body.employee });
  const salary = await DbService.getDocByFilter("SalaryPaymentModel", {
    employee: req.body.employee,
    $or: [
      { createdAt: { $gt: moment().subtract(0, "months").startOf("month") } },
      { createdAt: { $lt: moment().subtract(0, "months").endOf("month") } },
    ]
  }, { nullable: true });

  if (salary) throw new ApiError(httpStatus.BAD_REQUEST, "This salary data has already been added this month");
  const data = await DbService.addDoc("SalaryPaymentModel", req.body, { populate: { path: "employee", select: "id fullName avatar" } });
  res.json({ data });
}

export const updateSalaryPayment = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const data = await DbService.updateDocByFilter("SalaryPaymentModel", { id: req.params.salaryId }, req.body, {
    populate: [
      { path: "employee", select: "id fullName avatar" },
      { path: "adjustments", populate: "regulation" }
    ]
  });
  const adjustments = await DbService.getDocByFilter("PaymentAdjustmentModel", { salaryPayment: data._id, workingHour: { $ne: null } }, { many: true, nullable: true, populate: "workingHour" })
  // if ()

  // TO-DO: Check payment adjustment that has amount equal to FULL_DAY
  res.json({ data: populatedDataProcess(data) });
}

export const addPaymentAdjustment = async (req: ExtendedRequest, res: ExtendedResponse) => {
  req.body.creator = req.account._id;
  let salary = await DbService.getDocByFilter("SalaryPaymentModel", { id: req.params.salaryId });
  const adjustment = await DbService.addDoc("PaymentAdjustmentModel", { ...req.body, salaryPayment: salary._id }, { populate: "regulation" });
  // if (salary.netSalary < adjustment.amount) throw new ApiError("")
  salary = await DbService.updateDocByFilter("SalaryPaymentModel", { _id: adjustment.salaryPayment }, {
    $inc: { netSalary: (adjustment.regulation.type !== "PENALTY") ? adjustment.amount : -adjustment.amount }
  }, {
    populate: [
      { path: "employee", select: "id fullName avatar" },
      { path: "adjustments", populate: "regulation" }
    ]
  });

  res.json({ data: populatedDataProcess(salary) });
}

export const getSalaryRecords = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const filter = queryFilter(req.query, ["start", "end", "range", "employee"]);
  const data = await DbService.paginateDocsByFilter("SalaryPaymentModel", filter, req, {
    populate: [
      { path: "employee", select: "id fullName avatar" },
      { path: "adjustments", populate: "regulation" }
    ]
  });

  for (let doc of data.data) doc = populatedDataProcess(doc);
  res.json(data);
}

export const getSalaryRecord = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const data = await DbService.getDocByFilter("SalaryPaymentModel", { id: req.params.salaryId }, {
    populate: [
      { path: "employee", select: "id fullName avatar" },
      { path: "adjustments", populate: "regulation" }
    ]
  });

  res.json({ data: populatedDataProcess(data) });
}

export const getPaymentAdjustments = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const filter = queryFilter(req.query, []);
  filter["salaryPayment"] = (await DbService.getDocByFilter("SalaryPaymentModel", { id: req.params.salaryId }))["_id"];
  if (req.query.type) filter["regulation"] = (await DbService.getDocByFilter("PaymentRegulationModel", { type: (req.query.type as string).toUpperCase() }))["_id"];
  const data = await DbService.paginateDocsByFilter("PaymentAdjustmentModel", filter, req, { populate: "regulation" });
  res.json(data);
}

export const getPaymentAdjustment = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const data = await DbService.getDocByFilter("PaymentAdjustmentModel", { id: req.params.adjustmentId }, {
    populate: [
      { path: "regulation" },
      { path: "salaryPayment", populate: { path: "employee", select: "id fullName avatar" } }
    ]
  });
  res.json({ data });
}

export const updatePaymentAdjustment = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const adjustment = await DbService.updateDocByFilter("PaymentAdjustmentModel", { id: req.params.adjustmentId }, req.body, { populate: "regulation" });
  const salary = await DbService.getDocByFilter("SalaryPaymentModel", { _id: adjustment.salaryPayment }, {
    populate: [
      { path: "employee", select: "id fullName avatar" },
      { path: "adjustments", populate: "regulation" }
    ]
  });

  res.json({
    data: {
      salary: populatedDataProcess(salary),
      adjustment
    }
  });
}

export const deletePaymentAdjustment = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const adjustment = await DbService.deleteDocByFilter("PaymentAdjustmentModel", { id: req.params.adjustmentId }, { populate: "regulation" });
  const salary = await DbService.getDocByFilter("SalaryPaymentModel", { _id: adjustment.salaryPayment }, {
    populate: [
      { path: "employee", select: "id fullName avatar" },
      { path: "adjustments", populate: "regulation" }
    ]
  });

  res.json({
    data: {
      salary: populatedDataProcess(salary),
      adjustment
    }
  });
}

function populatedDataProcess(doc) {
  doc["$$populatedVirtuals"]["adjustments"] = doc["adjustments"].reduce((previousValue, currentValue) => {
    return previousValue[currentValue.regulation.type] ? previousValue[currentValue.regulation.type] += currentValue.amount : previousValue[currentValue.regulation.type] = currentValue.amount, previousValue;
  }, {});
  doc["_doc"].netSalary = doc["grossSalary"];
  for (const field in doc["adjustments"]) doc["_doc"].netSalary += (field === "BONUS" || field === "ALLOWANCE") ? doc["adjustments"][field] : -doc["adjustments"][field];
  return doc;
}