import axios from "axios";

export const randomQuote = async (req, res) => {
  const { data } = await axios.get(
    "https://zenquotes.io/api/today?option1=value&option2=value"
  );
  res.json(data);
}
