import { queryFilter } from '@src/utils/filter';
import { ExtendedRequest, ExtendedResponse } from '@src/types/type';
import { DbService } from '@src/services/DbService';

export const addLabel = async (req: ExtendedRequest, res: ExtendedResponse) => {
  req.body.creator = req.account._id;
  const { _id: project } = await DbService.getDocByFilter("ProjectModel", { id: req.params.projectId.toString() });
  const label = await DbService.addDoc("LabelModel", { ...req.body, project }, { uniqueFields: ["name"] });
  res.json({ data: label });
};

export const getLabels = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const filter = queryFilter(req.query, []);
  const { _id: project } = await DbService.getDocByFilter("ProjectModel", { id: req.params.projectId.toString() });

  const labels = await DbService.getDocByFilter("LabelModel", { ...filter, project }, { many: true, nullable: true });
  res.json({ data: labels });
};

export const getLabel = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const labels = await DbService.getDocByFilter("LabelModel", { id: req.params.labelId });
  res.json({ data: labels });
};

export const updateLabel = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const label = await DbService.updateDocByFilter("LabelModel", { id: req.params.labelId }, req.body, { uniqueFields: ["name"] });
  res.json({ data: label });
};

export const deleteLabel = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const label = await DbService.deleteDocByFilter("LabelModel", { id: req.params.labelId });
  res.json({ data: label });
};