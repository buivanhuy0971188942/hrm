import {ExtendedRequest, ExtendedResponse} from '@src/types/type';
import {DbService} from '@src/services/DbService';
import {queryFilter} from "@src/utils/filter";

export const addPostCategory = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const category = await DbService.addDoc("PostCategoryModel", req.body, {uniqueFields: ["name"]});
  res.json({data: category});
}

export const getPostCategories = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const filter = queryFilter(req.query, ["slug"]);
  const categories = await DbService.paginateDocsByFilter("PostCategoryModel", filter, req);
  for (const category of categories.data) {
    category["_doc"].postCount = await DbService.countDocumentByFilter("PostModel", {categories: category._id});
  }
  res.json(categories);
}

export const getPostCategory = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const category = await DbService.getDocByFilter("PostCategoryModel", {id: req.params.categoryId});
  res.json({data: category});
}

export const updatePostCategory = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const category = await DbService.updateDocByFilter("PostCategoryModel", {id: req.params.categoryId}, req.body);
  res.json({data: category});
}

export const deletePostCategory = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const category = await DbService.deleteDocByFilter("PostCategoryModel", {id: req.params.categoryId});
  res.json({data: category});
}