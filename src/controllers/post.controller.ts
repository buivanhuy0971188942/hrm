import {ExtendedRequest, ExtendedResponse} from '@src/types/type';
import {DbService} from '@src/services/DbService';
import {handleBase64} from "@src/middlewares/general-file.middleware";
import {deleteFile, processFile} from "@src/utils/google-storage";
import {queryFilter} from "@src/utils/filter";

export const addPost = async (req: ExtendedRequest, res: ExtendedResponse) => {
  let contentWithFiles;
  try {
    if (req.files && req.files.length > 0) req.body.banner = (await processFile(req.files[0])).path;
    req.body.creator = req.account._id;
    req.body.type = "FEED";

    contentWithFiles = await handleBase64(req.body.content);
    req.body.content = contentWithFiles.data;
    const post = await DbService.addDoc("PostModel", req.body, {
      populate: [{
        path: "creator",
        select: "id avatar fullName"
      }, {
        path: "categories"
      }]
    });
    res.json({data: post});
  } catch (e) {
    if (req.body.banner) await deleteFile(req.body.banner);
    if (contentWithFiles.files) for (const file of contentWithFiles.files) await deleteFile(file);
    throw e;
  }
}

export const getPosts = async (req: ExtendedRequest, res: ExtendedResponse) => {
  let filter;
  if (!req.account) {
    filter = queryFilter(req.query, ["category", "slug"]);
    Object.assign(filter, {state: "OPENED"});
  } else {
    filter = queryFilter(req.query, ["category", "state", "slug"]);
    if (filter?.category) Object.assign(filter, {categories: (await DbService.getDocByFilter("PostCategoryModel", {slug: filter.category}))["_id"]})
  }
  Object.assign(filter, {type: "FEED"});

  const posts = await DbService.paginateDocsByFilter("PostModel", filter, req, {
    populate: [{
      path: "creator",
      select: "id avatar fullName"
    }, {
      path: "categories"
    }]
  });
  res.json(posts);
}

export const getPost = async (req: ExtendedRequest, res: ExtendedResponse) => {
  let filter: any;
  if (!req.account) {
    filter = queryFilter(req.query, ["category", "slug"]);
    Object.assign(filter, {
      state: "OPENED",
      type: "FEED",
      slug: req.params.postId
    });
  } else {
    filter = queryFilter(req.query, ["category", "state", "slug"]);
    if (filter?.category) Object.assign(filter, {categories: (await DbService.getDocByFilter("PostCategoryModel", {slug: filter.category}))["_id"]});
    Object.assign(filter, {
      type: "FEED",
      id: req.params.postId
    });
  }

  const post = await DbService.getDocByFilter("PostModel", filter, {
    populate: [{
      path: "creator",
      select: "id avatar fullName"
    }, {
      path: "categories"
    }]
  });
  res.json({data: post});
}

export const updatePost = async (req: ExtendedRequest, res: ExtendedResponse) => {
  let contentWithFiles;
  try {
    if (req?.files && req.files.length > 0) req.body.banner = (await processFile(req.files[0])).path;

    if (req.body.content) {
      contentWithFiles = await handleBase64(req.body.content);
      req.body.content = contentWithFiles.data;
    }

    const post = await DbService.updateDocByFilter("PostModel", {id: req.params.postId, type: "FEED"}, req.body, {
      populate: [{
        path: "creator",
        select: "id avatar fullName"
      }, {
        path: "categories"
      }]
    });
    res.json({data: post});
  } catch (e) {
    if (req.body.banner) await deleteFile(req.body.banner);
    if (contentWithFiles.files) for (const file of contentWithFiles.files) await deleteFile(file);
    throw e
  }
}

export const deletePost = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const post = await DbService.deleteDocByFilter("PostModel", {id: req.params.postId, type: "FEED"}, {
    populate: [{
      path: "creator",
      select: "id avatar fullName"
    }, {
      path: "categories"
    }]
  });

  res.json({data: post});
}