import {AuthService} from '@src/services/AuthService';
import {GitLabService} from "@src/services/GitLabService";
import {DbService} from '@src/services/DbService';
import {ExtendedNextFunction, ExtendedRequest, ExtendedResponse} from '@src/types/type';
import {ApiError} from "@src/utils/api-error";
import {config} from '@src/config';
import httpStatus from "http-status";
import {queryFilter} from "@src/utils/filter";
import {TelegramService} from "@src/services/TelegramService";

export const connectToGitLab = async (req: ExtendedRequest, res: ExtendedResponse) => {
  if (req.method.toLowerCase() === "get") {
    const code: string = req.query.code as string;
    if (!code) return res.redirect(`${config.gitLab.baseUrl}/oauth/authorize?response_type=code&redirect_uri=${config.gitLab.callbackURL}&client_id=${config.gitLab.clientID}&scope=${config.gitLab.scope}`);
    return res.json({code});
  }

  if (req.method.toLowerCase() === "post") {
    try {
      const data = await AuthService.getTokensFromOAuth(`${config.gitLab.baseUrl}/oauth/token`, {
        grantType: req.body.grant_type,
        redirectUri: req.body.redirect_uri,
        clientID: config.gitLab.clientID,
        clientSecret: config.gitLab.clientSecret
      }, {
        code: req.body.code,
        refreshToken: req.account?.gitLabRefreshToken,
      });
      let updateBody = {};
      if (req.body.grant_type === "authorization_code") {
        const {id: gitLabId} = await GitLabService.getSelfAccount(data.access_token);
        let account = await DbService.getDocByFilter("AccountModel", {
          $or: [{gitLabId}, {_id: req.account._id, gitLabId: {$ne: null}}]
        }, {nullable: true});
        if (account) throw new Error("accounts exists");
        Object.assign(updateBody, {gitLabId});
      }
      await DbService.updateDocByFilter("AccountModel", {
        _id: req.account._id,
      }, {
        ...updateBody,
        gitLabAccessToken: data.access_token,
        gitLabRefreshToken: data.refresh_token
      }, {select: "fullName id avatar"});

      res.json({
        data: data
      });

    } catch (e) {
      // console.log(e)
      throw new ApiError(httpStatus.BAD_REQUEST, "Failed to connect to GitLab");
    }
  }
}

export const disconnectToGitLab = async (req: ExtendedRequest, res: ExtendedResponse) => {
  await DbService.updateDocByFilter("AccountModel", {_id: req.account._id}, {
    gitLabAccessToken: null,
    gitLabRefreshToken: null,
    gitLabId: null
  });
  res.json({message: "Disconnected to GitLab successfully"});
}

export const getGroups = handleRefreshToken(async (req: ExtendedRequest, res: ExtendedResponse) => {
  const options = queryFilter(req.query, ["limit", "page"]);
  const result = await GitLabService.getGroups(req.account.gitLabAccessToken, options);
  res.json(result);
});

export const getProjectsByGroup = handleRefreshToken(async (req: ExtendedRequest, res: ExtendedResponse) => {
  const options = queryFilter(req.query, ["limit", "page"]);
  const result = await GitLabService.getProjectsByGroups(req.account.gitLabAccessToken, req.params.groupId, options);
  res.json(result);
});

export const getProjectHooks = handleRefreshToken(async (req: ExtendedRequest, res: ExtendedResponse) => {
  const options = queryFilter(req.query, ["limit", "page"]);
  const result = await GitLabService.getProjectHooks(req.account.gitLabAccessToken, req.params.projectId.toString(), options);
  res.json(result);
});

export const getProjectIssues = handleRefreshToken(async (req: ExtendedRequest, res: ExtendedResponse) => {
  const options = queryFilter(req.query, ["limit", "page"]);
  const result = await GitLabService.getProjectIssues(req.account.gitLabAccessToken, req.params.projectId.toString(), options);
  res.json(result);
});

export const getProjectCommits = handleRefreshToken(async (req: ExtendedRequest, res: ExtendedResponse) => {
  const options = queryFilter(req.query, ["limit", "page"]);
  const result = await GitLabService.getProjectCommits(req.account.gitLabAccessToken, req.params.projectId.toString(), options);
  res.json(result);
});

export const addProjectHooks = handleRefreshToken(async (req: ExtendedRequest, res: ExtendedResponse) => {
  const result = await GitLabService.addProjectHook(req.account.gitLabAccessToken, req.params.projectId.toString(), req.body);
  res.json({data: result});
});

export const handlePushEvent = (async (req: ExtendedRequest, res: ExtendedResponse) => {
  const receiveBody = req.body;
  try {
    switch (receiveBody["event_type"]) {
      case"issue": {
        const projectFromWebhook = receiveBody["project"];
        const issueFromWebhook = receiveBody["object_attributes"];
        const project = await DbService.getDocByFilter("ProjectModel", {
          "gitLabData.projectId": projectFromWebhook["id"]
        }, {nullable: true});
        const issueDoc = await DbService.getDocByFilter("IssueModel", {
          id: issueFromWebhook.id.toString(),
          gitLabProjectId: issueFromWebhook.project_id.toString()
        }, {nullable: true});

        if (issueDoc) {
          if (receiveBody["changes"]) {
            for (const field in receiveBody["changes"]) {
              issueFromWebhook[field] = receiveBody["changes"][field]["current"]
            }
          }

          await issueDoc.updateOne({
            ...issueFromWebhook,
            project: (project) ? project._id : issueDoc.project,
            createdTimeOnGitLab: issueFromWebhook.created_at,
            closedAt: issueFromWebhook.closed_at,
            gitLabProjectId: issueFromWebhook.project_id,
          });
        } else if (project) {
          await DbService.addDoc("IssueModel", {
            ...issueFromWebhook,
            project: project._id,
            createdTimeOnGitLab: issueFromWebhook.created_at,
            gitLabProjectId: issueFromWebhook.project_id
          });
        }
      }
    }
    await TelegramService.sendMessage(1362849660, receiveBody);
    res.json({message: "success"});
  } catch (e) {
    res.json({message: "failed"});
  }
});

export function handleRefreshToken(fn) {
  return async (req: ExtendedRequest, res: ExtendedResponse, next: ExtendedNextFunction) => {
    if (!req.account.gitLabRefreshToken) throw new ApiError(httpStatus.INTERNAL_SERVER_ERROR, "This accounts is not linked to any GitLab accounts");
    try {
      await GitLabService.getTokenInfo(req.account.gitLabAccessToken);
      await fn(req, res);
    } catch (err) {
      if (err?.response?.data?.error === "invalid_token") {
        await getGitLabTokens(req);
        handleRefreshToken(await fn(req, res));
      } else {
        console.log(err)
        next((err?.response?.data?.message.includes("401" || "403") || err?.response?.status === 403) ? new Error("Bạn không có quyền tạo liên kết với project này") : err);
      }
    }
  }
}

async function getGitLabTokens(req) {
  const data = await AuthService.getTokensFromOAuth(`${config.gitLab.baseUrl}/oauth/token`, {
    grantType: "refresh_token",
    clientID: config.gitLab.clientID,
    clientSecret: config.gitLab.clientSecret
  }, {
    refreshToken: req.account?.gitLabRefreshToken,
  });
  req.account = await DbService.updateDocByFilter("AccountModel", {
    _id: req.account._id,
  }, {
    gitLabAccessToken: data.access_token,
    gitLabRefreshToken: data.refresh_token
  });
}