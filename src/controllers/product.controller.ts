import {ExtendedRequest, ExtendedResponse} from '@src/types/type';
import {DbService} from '@src/services/DbService';
import {handleRefreshToken} from "@src/controllers/gitlab.controller";
import {queryFilter} from "@src/utils/filter";
import {deleteFile, processFile} from "@src/utils/google-storage";

export const addProduct = async (req: ExtendedRequest, res: ExtendedResponse) => {
  try {
    if (req.files) req.body.banner = (await processFile(req.files[0])).path;
    Object.assign(req.body, {author: req.account._id});
    const product = await DbService.addDoc("ProductModel", req.body, {
      populate: {
        path: "author",
        select: "id fullName avatar"
      }
    });
    res.json({data: product});
  } catch (e) {
    if (req.body.banner) await deleteFile(req.body.banner);
    throw e;
  }
};

export const getProducts = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const filter: any = queryFilter(req.query, ["slug", "category"]);
  if (filter.category) filter.category = (await DbService.getDocByFilter("ProjectCategoryModel", {slug: filter.category}))["_id"];
  const products = await DbService.paginateDocsByFilter("ProductModel", filter, req, {
    populate: {
      path: "author",
      select: "id fullName avatar"
    }
  });

  // for (const product of products.data) {
  //   product["_doc"].taskCount = await DbService.countDocumentByFilter("TaskModel", {product: product._id});
  //   product["_doc"].completedTaskCount = await DbService.countDocumentByFilter("TaskModel", {
  //     product: product._id,
  //     state: "COMPLETED"
  //   });
  // }
  res.json(products);
}

export const getProduct = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const product = await DbService.getDocByFilter("ProductModel", {$or: [{id: req.params.productId}, {slug: req.params.productId}]}, {
    populate: {
      path: "author",
      select: "id fullName avatar"
    }
  });
  res.json({data: product});
}

export const updateProduct = async (req: ExtendedRequest, res: ExtendedResponse) => {
  try {
    if (req.files) req.body.banner = (await processFile(req.files[0])).path;
    const product = await DbService.updateDocByFilter("ProductModel", {id: req.params.productId}, req.body, {
      populate: {
        path: "author",
        select: "id fullName avatar"
      }
    });
    res.json({data: product});
  } catch (e) {
    if (req.body.banner) await deleteFile(req.body.banner);
    throw e;
  }
};

export const deleteProduct = async (req: ExtendedRequest, res: ExtendedResponse) => {
  let product = await DbService.deleteDocByFilter("ProductModel", {id: req.params.productId}, {
    populate: {
      path: "author",
      select: "id fullName avatar"
    }
  });
  res.json({data: product});
};
