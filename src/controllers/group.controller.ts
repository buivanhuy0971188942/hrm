import {DbService} from '@src/services/DbService';
import {ExtendedRequest, ExtendedResponse} from '@src/types/type';
import {queryFilter} from "@src/utils/filter";
import {deleteFile, processFile} from "@src/utils/google-storage";
import {ApiError} from "@src/utils/api-error";
import httpStatus from "http-status";

export const addGroup = async (req: ExtendedRequest, res: ExtendedResponse) => {
  try {
    req.body.creator = req.account._id;
    if (req.files) req.body.avatar = (await processFile(req.files[0])).path;
    const group = await DbService.addDoc("GroupModel", req.body);
    if (req.body.members) {
      const members = (await DbService.getDocByFilter("GroupMemberModel", {
        _id: {$in: req.body.members},
        group: group._id
      }, {
        many: true,
        nullable: true
      })).map(data => data.member.toString());

      req.body.members = (Array.isArray(req.body.members))
        ? req.body.members.filter(data => !members.includes(data) && data !== req.account._id.toString()).map(data => {
          return {
            member: data, group: group._id
          }
        })
        : (members.includes(req.body.members) && req.body.members !== req.account._id.toString()) ? [{
          member: req.body.members,
          group: group._id
        }] : [];
    } else req.body.members = [];
    req.body.members.push({group: group._id, member: req.account._id, role: "ADMIN"});
    await DbService.addDoc("GroupMemberModel", req.body.members, {many: true});

    await group.populate("members");
    res.json({data: group});
  } catch (e) {
    if (req.body.coverPicture) await deleteFile(req.body.coverPicture);
    throw e;
  }
}

export const getGroups = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const filter: any = queryFilter(req.query, ["privacy", "slug"]);
  Object.assign(filter, req.authenticator.filterByOwnResource);
  const groups = await DbService.paginateDocsByFilter("GroupModel", filter, req);
  for (const group of groups.data) {
    group["_doc"].memberCount = await DbService.countDocumentByFilter("GroupMemberModel", {group: group._id});
    group["_doc"].postCount = await DbService.countDocumentByFilter("PostModel", {group: group._id});
  }
  res.json(groups);
}

export const getGroup = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const filter = {id: req.params.groupId};
  Object.assign(filter, req.authenticator.filterByOwnResource);
  const group = await DbService.getDocByFilter("GroupModel", filter);
  group["_doc"].memberCount = await DbService.countDocumentByFilter("GroupMemberModel", {group: group._id});
  group["_doc"].postCount = await DbService.countDocumentByFilter("PostModel", {group: group._id});
  res.json({data: group});
}

export const updateGroup = async (req: ExtendedRequest, res: ExtendedResponse) => {
  try {
    let data = await DbService.getDocByFilter("GroupModel", {id: req.params.groupId}), avatarForDelete;
    if (req.files) {
      if (data.avatar && data.avatar.includes("https://storage.googleapis.com")) avatarForDelete = data.avatar;
      req.body.avatar = (await processFile(req.files[0])).path;
    }
    Object.assign(data, req.body);
    data = await data.save();
    if (avatarForDelete) await deleteFile(avatarForDelete);

    res.json({data});
  } catch (e) {
    if (req.body.avatar) await deleteFile(req.body.avatar);
    throw e;
  }
}

export const deleteGroup = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const data = await DbService.deleteDocByFilter("GroupModel", {id: req.params.groupId});
  if (data.avatar) await deleteFile(req.body.avatar);
  if (data.coverPicture) await deleteFile(req.body.coverPicture);
  res.json({data});
}

export const addGroupPost = async (req: ExtendedRequest, res: ExtendedResponse) => {
  req.body.group = (await DbService.getDocByFilter("GroupModel", {id: req.params.groupId}))["_id"];
  req.body.creator = req.account._id;
  let uploadedFiles = [];
  try {
    req.body.type = "GROUP";
    const post = await DbService.addDoc("PostModel", req.body, {
      populate: [{
        path: "creator",
        select: "id avatar fullName"
      }]
    });

    if (req.files) {
      for (const file of req.files) {
        uploadedFiles.push({...(await processFile(file)), target: post._id, creator: req.account._id});
      }
      await DbService.addDoc("AttachmentModel", uploadedFiles);
    }
    await post.populate("attachments");
    res.json({data: post});
  } catch (e) {
    if (uploadedFiles) for (const file of uploadedFiles) await deleteFile(file.path);
    throw e;
  }
}

export const getGroupPosts = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const group = (await DbService.getDocByFilter("GroupModel", {id: req.params.groupId}))["_id"];
  const data = await DbService.paginateDocsByFilter("PostModel", {group}, req, {populate: "attachments"});
  res.json(data);
}

export const getGroupPost = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const group = (await DbService.getDocByFilter("GroupModel", {id: req.params.groupId}))["_id"];
  const data = await DbService.getDocByFilter("PostModel", {id: req.params.postId, group}, {populate: "attachments"});
  res.json({data});
}

export const updateGroupPost = async (req: ExtendedRequest, res: ExtendedResponse) => {
  let uploadedFiles = [];
  try {
    const group = (await DbService.getDocByFilter("GroupModel", {id: req.params.groupId}))["_id"];
    let post = await DbService.getDocByFilter("PostModel", {id: req.params.postId, group}, req.body);

    if (req.files) {
      for (const file of req.files) {
        uploadedFiles.push({...(await processFile(file)), target: post._id, creator: req.account._id});
      }
      await DbService.addDoc("AttachmentModel", uploadedFiles);
    }

    post = await DbService.updateDocByFilter("PostModel", {
      id: req.params.postId,
      group
    }, req.body, {populate: "attachments"});

    res.json({data: post});
  } catch (e) {
    if (uploadedFiles) for (const file of uploadedFiles) await deleteFile(file.path);
    throw e;
  }
}

export const deleteGroupPost = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const group = (await DbService.getDocByFilter("GroupModel", {id: req.params.groupId}))["_id"];
  const data = await DbService.deleteDocByFilter("PostModel", {id: req.params.postId, group});
  res.json({data});
}

export const addGroupMember = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const group = (await DbService.getDocByFilter("GroupModel", {id: req.params.groupId}))["_id"];
  const members = (await DbService.getDocByFilter("GroupMemberModel", {
    _id: {$in: req.body.members},
    group: group._id
  }, {
    many: true,
    nullable: true
  })).map(data => data.member.toString());

  req.body.members = req.body.members.filter(data => !members.includes(data)).map(data => {
    return {
      member: data, group: group._id
    }
  });

  const data = await DbService.addDoc("GroupMemberModel", req.body, {many: true});
  res.json({data});
}

export const updateGroupMember = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const group = (await DbService.getDocByFilter("GroupModel", {id: req.params.groupId}))["_id"];
  Object.assign(req.body, group);
  const data = await DbService.updateDocByFilter("GroupMemberModel", {
    member: req.body.member,
    group
  }, req.body, {uniqueFields: ["member"]});
  res.json({data});
}

export const getGroupMembers = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const group = (await DbService.getDocByFilter("GroupModel", {id: req.params.groupId}))["_id"];
  const filter = queryFilter(req.query, ["fullName", "id"]);
  const listMembers = (await DbService.paginateDocsByFilter("AccountModel", filter, req, {
    pagination: false,
    many: true,
    lean: true,
    nullable: true
  }))["data"].map(data => data._id);

  const members = await DbService.paginateDocsByFilter("GroupMemberModel", {
    member: {$in: listMembers},
    group
  }, req, {populate: {path: "member", select: "id fullName avatar"}});
  res.json(members);
}

export const deleteGroupMember = async (req: ExtendedRequest, res: ExtendedResponse) => {
  const group = await DbService.getDocByFilter("GroupModel", {id: req.params.groupId}, {populate: "members"});
  const currentMember = group.members.find(data => data.member.toString() === req.account._id.toString());
  let data;

  if (group.creator.toString() !== currentMember.member.toString()) {
    if (group.creator.toString() === req.body.member.toString() || (req.body.member.toString() !== req.account._id.toString() && currentMember.role !== "ADMIN")) {
      throw new ApiError(httpStatus.FORBIDDEN, "Failed to delete member");
    }
  } else if (req.body.authorizedMember && group.members.length > 1) {
    await DbService.updateDocByFilter("GroupMemberModel", {
      member: req.body.authorizedMember,
      group: group._id
    }, {role: "ADMIN"});
    Object.assign(group, {creator: req.body.authorizedMember});
    await group.save();
  } else {
    throw new ApiError(httpStatus.BAD_REQUEST, "Please delete the group instead");
  }

  data = await DbService.deleteDocByFilter("GroupMemberModel", {member: req.body.member, group: group._id});
  res.json({data});
}
