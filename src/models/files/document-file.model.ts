import mongoose, { Document } from "mongoose";
import paginate from "mongoose-paginate-v2";
import { toJSON } from "@src/models/plugins";

const schema = new mongoose.Schema<DocumentFile>(
  {
    creator: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Account",
    },
    target: {
      type: mongoose.Schema.Types.ObjectId,
    },
    fileName: {
      type: String,
    },
    fileType: {
      type: String,
      enum: ["doc", "sound", "image", "video", "link"],
      required: true,
    },
    path: String,
    size: String,
    metadata: mongoose.Schema.Types.Mixed,
    status: {
      type: String,
      enum: ["pending", "approved", "rejected"],
      default: "pending",
    },
    isDeleted: {
      type: mongoose.Schema.Types.Boolean,
      default: false,
    },
    folderParentIds: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "DocumentFolder",
      },
    ],
  },
  {
    timestamps: true,
    collection: "document-file",
  }
);

schema.plugin(paginate);
schema.plugin(toJSON);

export const DocumentFileModel = mongoose.model("DocumentFile", schema);



