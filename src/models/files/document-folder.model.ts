import mongoose, { Document } from "mongoose";
import paginate from "mongoose-paginate-v2";
import { toJSON } from "@src/models/plugins";
import { boolean } from "joi";

const schema = new mongoose.Schema<DocumentFolder>(
  {
    id: {
      type: String,
      required: true,
      unique: true,
    },
    folderName: {
      type: String,
      required: true,
    },
    isDeleted: {
      type: mongoose.Schema.Types.Boolean,
      default: false,
    },
    folders: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "DocumentFolder",
      },
    ],
    files: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "DocumentFile",
      },
    ],
    folderParentIds: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "DocumentFolder",
      },
    ],
  },
  {
    timestamps: true,
    collection: "document-folder",
  }
);

schema.plugin(paginate);
schema.plugin(toJSON);

export const DocumentFolderModel = mongoose.model("DocumentFolder", schema);
