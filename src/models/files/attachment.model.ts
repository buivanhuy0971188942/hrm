import mongoose, {Document} from "mongoose";
import paginate from "mongoose-paginate-v2";
import {toJSON} from "@src/models/plugins";

export interface AttachmentDocument extends Document, Attachment {
}

const schema = new mongoose.Schema<Attachment>({
  creator: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Account"
  },
  target: {
    type: mongoose.Schema.Types.ObjectId,
  },
  fileName: {
    type: String
  },
  fileType: {
    type: String,
    enum: ['doc', 'sound', 'image', 'video', 'link'],
    required: true
  },
  path: String,
  size: String,
  metadata: mongoose.Schema.Types.Mixed
}, {
  timestamps: true
});


schema.plugin(paginate);
schema.plugin(toJSON);

export const AttachmentModel = mongoose.model<AttachmentDocument>('Attachment', schema);