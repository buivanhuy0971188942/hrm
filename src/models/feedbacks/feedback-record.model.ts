import mongoose, {Document} from "mongoose";
import paginate from "mongoose-paginate-v2";
import { uniqueStringGenerator } from '@src/utils/slugify';
import { toJSON } from "../plugins";

interface FeedbackRecordDocument extends Document, FeedbackRecord { }

const schema = new mongoose.Schema<FeedbackRecord>({
  id: String,
	feedback: {
		type: mongoose.Schema.Types.ObjectId,
		ref: "Feedback",
	},
	criteria: {
		type: mongoose.Schema.Types.ObjectId,
		ref: "Criteria",
	},
	rating: Number,
	note: String,
}, {
	timestamps: true
});

schema.pre('save', async function (next) {
	const doc = this;
  if (!doc.id) doc.id = await uniqueStringGenerator("iId", "FeedbackRecordModel");

	next();
});

schema.plugin(paginate);
schema.plugin(toJSON);

export const FeedbackRecordModel = mongoose.model<FeedbackRecordDocument>('FeedbackRecord', schema);
