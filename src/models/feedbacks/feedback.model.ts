import mongoose, {Document} from "mongoose";
import paginate from "mongoose-paginate-v2";
import {uniqueStringGenerator} from '@src/utils/slugify';
import {toJSON} from "../plugins";

interface FeedbackDocument extends Document, Feedback {
}

const schema = new mongoose.Schema<Feedback>({
  id: String,
  employee: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Account",
  },
  assessor: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Account",
  },
  note: String,
  overallRating: String
}, {
  timestamps: true,
  toJSON: {virtuals: true},
  toObject: {virtuals: true}
});

schema.virtual('records', {
  ref: 'FeedbackRecord',
  localField: '_id',
  foreignField: 'feedback',
});

schema.pre('save', async function (next) {
  const doc = this;
  if (!doc.id) doc.id = await uniqueStringGenerator("iId", "FeedbackModel");

  next();
});

schema.plugin(paginate);
schema.plugin(toJSON);

export const FeedbackModel = mongoose.model<FeedbackDocument>('Feedback', schema);
