import mongoose, {Document} from "mongoose";
import paginate from "mongoose-paginate-v2";
import { uniqueStringGenerator } from '@src/utils/slugify';
import { toJSON } from "../plugins";

interface CriteriaDocument extends Document, Criteria { }

const schema = new mongoose.Schema<Criteria>({
  id: String,
	name: {
		type: String,
		required: true,
	},
	slug: {
		type: String,
	},
}, {
	timestamps: true
});

schema.pre('save', async function (next) {
	const doc = this;
  if (!doc.id) doc.id = await uniqueStringGenerator("iId", "CriteriaModel");

	next();
});

schema.plugin(paginate);
schema.plugin(toJSON);

export const CriteriaModel = mongoose.model<CriteriaDocument>('Criteria', schema);
