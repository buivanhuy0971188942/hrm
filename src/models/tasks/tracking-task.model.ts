import mongoose, {Document} from "mongoose";
import paginate from "mongoose-paginate-v2";
import {uniqueStringGenerator} from '@src/utils/slugify';
import {toJSON} from "../plugins";

export interface TrackingTaskDocument extends Document, TrackingTask {
}

const schema = new mongoose.Schema<TrackingTask>({
  creator: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Account'
  },
  task: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Task',
    required: true
  },
  changes: {},
  action: {
    type: String,
    enum: ['ADD', 'DELETE', 'UPDATE']
  }
}, {
  timestamps: true,
  collection: "tracking-tasks"
});

schema.plugin(paginate);
schema.plugin(toJSON);

export const TrackingTaskModel = mongoose.model<TrackingTaskDocument>('TrackingTask', schema);
