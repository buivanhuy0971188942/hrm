import mongoose, {Document} from "mongoose";
import paginate from "mongoose-paginate-v2";
import {uniqueStringGenerator} from '@src/utils/slugify';
import {toJSON} from "../plugins";
import {DbService} from "@src/services/DbService";
import {deleteFile} from "@src/utils/google-storage";
import { ApiError } from "@src/utils/api-error";
import httpStatus from "http-status";

export interface TaskDocument extends Document, Task {
}

const schema = new mongoose.Schema<Task>({
  id: String,
  name: {
    type: String
  },
  startDate: Date,
  dueDate: Date,
  index: Number,
  creator: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Account"
  },
  assignedTo: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Account"
  },
  board: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Board"
  },
  project: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Project"
  },
  cover: String,
  description: String,
  state: {
    type: String,
    enum: ["TODO", "IN_PROGRESS", "REVIEWING", "COMPLETED", "ARCHIVED"],
    default: "TODO"
  },
  labels: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: "Label"
  }],
  comments: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: "Comment"
  }],
  issue: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Issue",
  },
  type: {
    type: String,
    enum: ["PROJECT", "GENERAL"]
  },
  isDeleted: {
    type: Boolean,
    default: false
  },
  members: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: "Account"
  }]
}, {
  timestamps: true,
  toJSON: {virtuals: true},
  toObject: {virtuals: true}
});

schema.virtual('attachments', {
  ref: 'Attachment',
  localField: '_id',
  foreignField: 'target',
});

schema.pre('save', async function (next) {
  const doc = this;
  if (!doc.id) doc.id = await uniqueStringGenerator("iId", "TaskModel");
  const maxIndex = (await TaskModel.findOne({type: 'PROJECT'}, null, {sort: "-index"}));
  if (!doc.index) doc.index = (maxIndex) ? ++maxIndex.index : 0;
  if (!doc.startDate && doc["createdAt"]) doc.startDate = doc["createdAt"];
  var modified_paths = this.modifiedPaths();
  
  next();
});

schema.pre('deleteOne', {document: true, query: true}, async function (next) {
  const doc = this;
  let [attachments] = await Promise.all([
    DbService.getDocByFilter("AttachmentModel", {target: doc._id}, {
      many: true,
      nullable: true
    }),
    DbService.deleteDocByFilter("TaskMemberModel", {task: doc._id}, {many: true}),
  ]);
  for (const data of attachments) {
    await deleteFile(data.path);
  }
  await DbService.deleteDocByFilter("AttachmentModel", {target: doc._id}, {many: true});
  await DbService.deleteDocByFilter('SubTaskModel', {task: doc._id}, {many: true})
  next();
});

const typesFindQueryMiddleware = [
  'countDocuments',
  'find',
  'findOne',
  'findOneAndDelete',
  'findOneAndRemove',
  'findOneAndUpdate',
  'update',
  'updateOne',
  // 'updateMany',
];

const excludeInFindQueriesIsDeleted = async function (this, next) {
  this.where({isDeleted: {$ne: true}});
  next();
};

typesFindQueryMiddleware.forEach((type) => {
  schema.pre(type, excludeInFindQueriesIsDeleted);
});

schema.plugin(paginate);
schema.plugin(toJSON);

export const TaskModel = mongoose.model<TaskDocument>('Task', schema);
