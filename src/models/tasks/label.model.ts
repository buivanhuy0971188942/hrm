import mongoose, {Document} from "mongoose";
import paginate from "mongoose-paginate-v2";
import {uniqueStringGenerator} from '@src/utils/slugify';
import {toJSON} from "../plugins";
import {DbService} from "@src/services/DbService";

interface LabelDocument extends Document, Label {
}

const schema = new mongoose.Schema<Label>({
  id: String,
  creator: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Account"
  },
  name: {
    type: String,
    required: true,
  },
  project: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Project"
  },
  color: {
    type: String,
  },
}, {
  timestamps: true
});

schema.pre('save', async function (next) {
  const doc = this;
  if (!doc.id) doc.id = await uniqueStringGenerator("iId", "LabelModel");

  next();
});

schema.pre('deleteOne', {document: true, query: false}, async function (next) {
  const doc = this;
  await DbService.updateDocByFilter("IssueModel", {}, {
    $pull: {
      labels: {_id: doc._id}
    }
  }, {many: true});
  next();
});

schema.plugin(paginate);
schema.plugin(toJSON);

export const LabelModel = mongoose.model<LabelDocument>('Label', schema);
