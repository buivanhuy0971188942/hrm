import mongoose, {Document} from "mongoose";
import paginate from "mongoose-paginate-v2";
import {uniqueStringGenerator} from '@src/utils/slugify';
import {toJSON} from "../plugins";

export interface TaskMemberDocument extends Document, TaskMember {
}

const schema = new mongoose.Schema<TaskMember>({
  id: String,
  member: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Account"
  },
  task: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Task"
  },
}, {
  timestamps: true
});


schema.pre('save', async function (next) {
  const doc = this;
  if (!doc.id) doc.id = await uniqueStringGenerator("iId", "TaskMemberModel");
  next();
});

schema.plugin(paginate);
schema.plugin(toJSON);

export const TaskMemberModel = mongoose.model<TaskMemberDocument>('TaskMember', schema);
