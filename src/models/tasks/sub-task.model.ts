import mongoose, {Document} from "mongoose";
import paginate from "mongoose-paginate-v2";
import {uniqueStringGenerator} from '@src/utils/slugify';
import {toJSON} from "../plugins";
import { ApiError } from "@src/utils/api-error";
import httpStatus from "http-status";

const schema = new mongoose.Schema<SubTask>({
  id: String,
  task: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Task'
  },
  creator: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Account'
  },
  name: {
    type: String
  },
  status: {
    type: String,
    enum: ['IN_PROGRESS', 'COMPLETED'],
    default: 'IN_PROGRESS'
  }
}, {
  timestamps: true,
  collection: 'sub-tasks',
//   strict: false
});

schema.pre('save', async function (next) {
  const doc = this;
  if (!doc.id) doc.id = await uniqueStringGenerator("iId", "SubTaskModel");
  var modified_paths = this.modifiedPaths();
  if(modified_paths.length === 0) throw new ApiError(httpStatus.OK, 'Everything is up to date')

  next();
});

schema.plugin(paginate);
schema.plugin(toJSON);

const SubTaskModel = mongoose.model('SubTask', schema);
SubTaskModel.collection.dropIndex('task_1_name_1', function(err, result) {});

module.exports = {SubTaskModel}
