import mongoose, {PaginateModel, Document} from "mongoose";
import {toJSON} from "../plugins";
import paginate from "mongoose-paginate-v2";
import {uniqueStringGenerator} from "@src/utils/slugify";

interface RequestReceiverDocument extends Document, RequestReceiver {
}

const schema = new mongoose.Schema<RequestReceiver>({
  id: String,
  request: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Request",
    required: true
  },
  receiver: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Account",
    required: true
  },
}, {
  timestamps: true,
});

schema.pre('save', async function (next) {
  const doc = this;
  if (!doc.id) doc.id = await uniqueStringGenerator("iId", "RequestReceiverModel");
  next();
});

schema.plugin(paginate);
schema.plugin(toJSON);

export const RequestReceiverModel = mongoose.model<RequestReceiverDocument, PaginateModel<RequestReceiverDocument>>('RequestReceiver', schema);
