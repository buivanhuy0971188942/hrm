import mongoose, {PaginateModel, Document} from "mongoose";
import {toJSON} from "../plugins";
import paginate from "mongoose-paginate-v2";
import {uniqueStringGenerator} from "@src/utils/slugify";
import {DbService} from "@src/services/DbService";
import {deleteFile} from "@src/utils/google-storage";

interface RequestDocument extends Document, Request {
}

const schema = new mongoose.Schema<Request>({
  id: String,
  sender: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Account",
    required: true
  },
  content: String,
  type: {
    type: String,
    enum: ["STUDY", "OVERTIME", "ABSENCE", "OTHER"]
  },
  title: {
    type: String,
    required: true
  },
  slug: String,
  status: {
    type: String,
    enum: ["APPROVED", "REJECTED", "PENDING"],
    default: "PENDING"
  },
  response: String,
  department: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Department"
  },
  tasks: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: "Task"
  }],
  assessor: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Account",
  },
  isDeleted: {
    type: Boolean,
    private: true,
    default: false
  }
}, {
  timestamps: true,
  toJSON: {virtuals: true},
  toObject: {virtuals: true}
});

schema.pre('save', async function (next) {
  const doc = this;
  if (!doc.id) doc.id = await uniqueStringGenerator("iId", "RequestModel");
  if (!doc.slug) doc.slug = await uniqueStringGenerator("slug", "DepartmentModel", doc.title, "slug");
  next();
});

schema.pre('deleteOne', {document: true, query: true}, async function (next) {
  const doc = this;
  await DbService.deleteDocByFilter("RequestReceiverModel", {request: doc._id}, {many: true});
  await DbService.deleteDocByFilter("TimelineEventModel", {request: doc._id}, {many: true});
  const attachments = await DbService.getDocByFilter("AttachmentModel", {target: doc._id}, {
    many: true,
    nullable: true
  });
  for (const atm of attachments) await deleteFile(atm.path);
  await DbService.deleteDocByFilter("AttachmentModel", {target: doc._id}, {many: true});
  next();
});


const typesFindQueryMiddleware = [
  'countDocuments',
  'find',
  'findOne',
  'findOneAndDelete',
  'findOneAndRemove',
  'findOneAndUpdate',
  'update',
  'updateOne',
  // 'updateMany',
];

const excludeInFindQueriesIsDeleted = async function (this, next) {
  this.where({isDeleted: {$ne: true}});
  next();
};

typesFindQueryMiddleware.forEach((type) => {
  schema.pre(type, excludeInFindQueriesIsDeleted);
});

schema.virtual('receivers', {
  ref: 'RequestReceiver',
  localField: '_id',
  foreignField: 'request',
});

schema.virtual('timelineEvents', {
  ref: 'TimelineEvent',
  localField: '_id',
  foreignField: 'request',
});

schema.virtual('attachments', {
  ref: 'Attachment',
  localField: '_id',
  foreignField: 'target',
});

schema.plugin(paginate);
schema.plugin(toJSON);

export const RequestModel = mongoose.model<RequestDocument, PaginateModel<RequestDocument>>('Request', schema);
