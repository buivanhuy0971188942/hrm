import mongoose, {PaginateModel, Document} from "mongoose";
import {toJSON} from "../plugins";
import paginate from "mongoose-paginate-v2";
import {uniqueStringGenerator} from "@src/utils/slugify";

interface WorkingHourDocument extends Document, WorkingHour {
}

const schema = new mongoose.Schema<WorkingHour>({
  employee: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Account"
  },
  startTime: Number,
  endTime: Number,
  status: {
    type: String,
    enum: ["ON_TIME", "LATE", "ABSENT"],
    default: "ON_TIME"
  },
  shift: {
    type: String,
    enum: ["MORNING", "AFTERNOON", "ALL"]
  },
  note: String,
  id: String,
}, {
  timestamps: true,
  collection: "working-hours",
  toJSON: {virtuals: true},
  toObject: {virtuals: true}
});

schema.virtual('adjustment', {
  ref: 'PaymentAdjustment',
  localField: '_id',
  foreignField: 'workingHour',
});
schema.pre('save', async function (next) {
  const doc = this;
  if (!doc.id) doc.id = await uniqueStringGenerator("iId", "WorkingHourModel");
  next();
});

schema.plugin(paginate);
schema.plugin(toJSON);

export const WorkingHourModel = mongoose.model<WorkingHourDocument, PaginateModel<WorkingHourDocument>>('WorkingHour', schema);
