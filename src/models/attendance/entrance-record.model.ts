import mongoose, {PaginateModel, Document} from "mongoose";
import {toJSON} from "../plugins";
import paginate from "mongoose-paginate-v2";

interface EntranceRecordDocument extends Document, EntranceRecord {
}

const schema = new mongoose.Schema<EntranceRecord>({
  entranceId: {
    type: Number,
    unique: true
  },
  time: Number,
  cardId: String,
  type: Number,
}, {
  timestamps: true
});

schema.plugin(paginate);
schema.plugin(toJSON);

export const EntranceRecordModel = mongoose.model<EntranceRecordDocument, PaginateModel<EntranceRecordDocument>>('EntranceRecord', schema);
