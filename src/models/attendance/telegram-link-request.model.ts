import mongoose, {Document} from "mongoose";

const schema = new mongoose.Schema({
  telegramId: String,
  randomCode: String
});

export const TelegramLinkRequestModel =  mongoose.model('TelegramLinkRequest', schema);
