import mongoose, {PaginateModel, Document} from "mongoose";
import {toJSON} from "../plugins";
import paginate from "mongoose-paginate-v2";
import {uniqueStringGenerator} from "@src/utils/slugify";

interface EventDocument extends Document, TimelineEvent {
}

const schema = new mongoose.Schema<TimelineEvent>({
  id: String,
  owner: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Account"
  },
  type: {
    type: String,
    enum: ["STUDY", "OVERTIME", "CHECK_IN", "CHECK_OUT", "IDLE_TIME"]
  },
  note: String,
  startTime: Number,
  endTime: Number,
  request: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Request"
  },
  state: {
    type: String,
    enum: ["PENDING", "APPROVED", "REJECTED"],
    default: "PENDING"
  }
}, {
  timestamps: true,
  toJSON: {virtuals: true},
  toObject: {virtuals: true},
  collection: "timeline-events"
});

schema.pre('save', async function (next) {
  const doc = this;
  if (!doc.id) doc.id = await uniqueStringGenerator("iId", "TimelineEventModel");
  if (["CHECK_IN", "CHECK_OUT", "IDLE_TIME"].includes(doc.type)) doc.state = "APPROVED";
  next();
});

schema.plugin(paginate);
schema.plugin(toJSON);

export const TimelineEventModel = mongoose.model<EventDocument, PaginateModel<EventDocument>>('TimelineEvent', schema);
