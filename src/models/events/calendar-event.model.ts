import mongoose, {PaginateModel, Document} from "mongoose";
import {toJSON} from "../plugins";
import paginate from "mongoose-paginate-v2";
import {uniqueStringGenerator} from "@src/utils/slugify";

interface EventDocument extends Document, CalendarEvent {
}

const schema = new mongoose.Schema<CalendarEvent>({
  id: String,
  owner: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Account",
  },
  title: String,
  type: {
    type: String,
    enum: ["PERSONAL-EVENT", "COMPANY-EVENT", "HOLIDAY"]
  },
  privacy: {
    type: String,
    enum: ["PRIVATE", "PUBLIC"],
    default: "PUBLIC"
  },
  description: String,
  startTime: Date,
  endTime: Date,
  alarm: Number,
  isRecurrent: {
    type: Boolean,
    default: false
  },
  recurrenceType: {
    type: String,
    enum: ["DAILY", "WEEKLY", "MONTHLY", "YEARLY"]
  }
}, {
  timestamps: true,
  toJSON: {virtuals: true},
  toObject: {virtuals: true},
  collection: "calendar-events"
});

schema.pre('save', async function (next) {
  const doc = this;
  if (!doc.id) doc.id = await uniqueStringGenerator("iId", "CalendarEventModel");
  next();
});

schema.plugin(paginate);
schema.plugin(toJSON);

export const CalendarEventModel = mongoose.model<EventDocument, PaginateModel<EventDocument>>('CalendarEvent', schema);
