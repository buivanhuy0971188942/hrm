import mongoose, {Document} from "mongoose";
import {toJSON} from "../plugins";
import paginate from "mongoose-paginate-v2";

interface EmailDocument extends Document, Email {
}

const schema = new mongoose.Schema<Email>({
  emailType: {
    type: String,
    values: ['outgoing', 'incoming']
  },
  creationTime: Number,
  id: String,
  sender: String,
  from: String,
  recipient: String,
  subject: String,
  data: Object,
  strippedText: String,
  bodyHTML: String,
  hasAttachment: Boolean,
  attachments: Array,
  owner: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Account",
  },
  isStarred: {
    type: Boolean,
    default: false
  },
  isDeleted: {
    type: Boolean,
    default: false
  },
  isSeen: {
    type: Boolean,
    default: false
  },
  starredAt: String,
  deletedAt: String,
}, {
  timestamps: true,
});

schema.pre("save", function (next) {
  const doc = this;
  if (doc.isModified("isStarred")) doc.starredAt = new Date().toString();
  if (doc.isModified("isDeleted")) doc.deletedAt = new Date().toString();
  next();
});

schema.plugin(paginate);
schema.plugin(toJSON);

export const EmailModel = mongoose.model<EmailDocument>('Email', schema);


