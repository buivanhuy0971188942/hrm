import {toJSON} from '../plugins';
import paginate from 'mongoose-paginate-v2';
import mongoose, {Document} from "mongoose";
import "@src/models/files/attachment.model";
import "@src/models/web-info/post-category.model";
import "@src/models/accounts/account.model";
import {uniqueStringGenerator} from "@src/utils/slugify";
import {deleteFile} from "@src/utils/google-storage";
import {DbService} from "@src/services/DbService";

interface ProductInfoDocument extends Document, Product {
}

const schema = new mongoose.Schema<Product>({
  id: String,
  author: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Account"
  },
  name: String,
  slug: String,
  category: {
    type: String,
    // enum: [""]
  },
  description: String,
  banner: String,
  downLoadCount: Number,
  CHPlayUrl: String,
  AppStoreUrl: String,
}, {
  timestamps: true,
  toJSON: {virtuals: true},
  toObject: {virtuals: true}
});

schema.virtual("attachments", {
  ref: "Attachment",
  localField: "_id",
  foreignField: "target"
})

schema.pre('save', async function (next) {
  const doc = this;
  if (!doc.id) doc.id = await uniqueStringGenerator("iId", "ProductModel");
  if (doc.isModified("name")) doc.slug = await uniqueStringGenerator("slug", "ProductModel", doc.name, "slug");
  next();
});

schema.pre('deleteOne', {query: false, document: true}, async function (next) {
  const doc = this;
  if (doc.banner) await deleteFile(doc.banner);
  next();
});

schema.plugin(paginate);
schema.plugin(toJSON);

export const ProductModel = mongoose.model<ProductInfoDocument>('Product', schema);