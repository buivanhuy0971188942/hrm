import {toJSON} from '../plugins';
import paginate from 'mongoose-paginate-v2';
import mongoose, {Document} from "mongoose";
import "@src/models/files/attachment.model";
import "@src/models/web-info/post-category.model";
import "@src/models/accounts/account.model";
import {uniqueStringGenerator} from "@src/utils/slugify";
import {deleteFile} from "@src/utils/google-storage";
import {DbService} from "@src/services/DbService";

interface PostDocument extends Document, Post {
}

const schema = new mongoose.Schema<Post>({
  id: String,
  creator: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Account"
  },
  title: String,
  slug: String,
  content: String,
  description: String,
  banner: {
    type: String,
    default: "https://imgur.com/lb8Wet9.png"
  },
  state: {
    type: String,
    enum: ["CLOSED", "OPENED"],
    default: "OPENED"
  },
  type: {
    type: String,
    enum: ['GROUP', 'FEED']
  },
  source: String,
  categories: {
    type: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: "PostCategory"
    }],
    default: []
  },
  group: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Group"
  },
  isDeleted: {
    type: Boolean,
    private: true,
    default: false
  }
}, {
  timestamps: true,
  toJSON: {virtuals: true},
  toObject: {virtuals: true}
});

schema.virtual("attachments", {
  ref: "Attachment",
  localField: "_id",
  foreignField: "target"
})

schema.pre('save', async function (next) {
  const doc = this;
  if (!doc.id) doc.id = await uniqueStringGenerator("iId", "PostModel");
  if (doc.isModified("title")) doc.slug = await uniqueStringGenerator("slug", "PostModel", doc.title, "slug");
  next();
});

schema.pre('deleteOne', {document: true, query: false}, async function (next) {
  const doc = this;
  try {
    if (doc?.banner.includes("https://storage.googleapis.com")) await deleteFile(doc.banner);
    const attachments = await DbService.getDocByFilter("AttachmentModel", {target: doc._id}, {
      many: true,
      nullable: true
    });
    for (const atm of attachments) await deleteFile(atm.path);
    await DbService.deleteDocByFilter("AttachmentModel", {target: doc._id}, {many: true})
  } catch (e) {
    console.log(e)
  }
  next();
});

const typesFindQueryMiddleware = [
  'countDocuments',
  'find',
  'findOne',
  'findOneAndDelete',
  'findOneAndRemove',
  'findOneAndUpdate',
  'update',
  'updateOne',
  // 'updateMany',
];

const excludeInFindQueriesIsDeleted = async function (this, next) {
  this.where({isDeleted: {$ne: true}});
  next();
};

typesFindQueryMiddleware.forEach((type) => {
  schema.pre(type, excludeInFindQueriesIsDeleted);
});

schema.plugin(paginate);
schema.plugin(toJSON);

export const PostModel = mongoose.model<PostDocument>('Post', schema);