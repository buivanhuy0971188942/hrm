import mongoose, {Document} from "mongoose";
import paginate from "mongoose-paginate-v2";
import {toJSON} from "@src/models/plugins";

interface PostCommentDocument extends Document, PostComment {
}

const schema = new mongoose.Schema<PostComment>({
  creator: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Account"
  },
	creationTime: Date,
  content: String,
  attachments: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: "Attachment"
  }]
}, {
  timestamps: true
});

schema.plugin(paginate);
schema.plugin(toJSON);

export const PostCommentModel = mongoose.model<PostCommentDocument>('PostComment', schema);