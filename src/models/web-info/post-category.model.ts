import mongoose, {Document} from "mongoose";
import paginate from "mongoose-paginate-v2";
import {toJSON} from "@src/models/plugins";
import {uniqueStringGenerator} from "@src/utils/slugify";

interface PostCategoryDocument extends Document, PostCategory {
}

const schema = new mongoose.Schema<PostCategory>({
  id: String,
  hashtag: String,
  name: String,
  slug: String,
  description: String,
}, {
  timestamps: true
});

schema.pre("save", async function (next) {
  const doc = this;
  if (!doc.id) doc.id = await uniqueStringGenerator("iId", "PostCategoryModel");
  if (doc.isModified("name")) doc.slug = await uniqueStringGenerator("slug", "PostCategoryModel", doc.name, "slug");
  next();
});

schema.plugin(paginate);
schema.plugin(toJSON);

export const PostCategoryModel = mongoose.model<PostCategoryDocument>('PostCategory', schema);