import mongoose, {PaginateModel, Document} from "mongoose";
import {toJSON} from "../plugins";
import paginate from "mongoose-paginate-v2";
import {uniqueStringGenerator} from "@src/utils/slugify";

const schema = new mongoose.Schema<Report>({
  id: String,
  sender: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Account",
    required: true
  },
  content: String,
  title: String,
  feedback: String,
  status: {
    type: String,
    enum: ["LATE", "ON_TIME"],
    default: "ON_TIME"
  },
  isDeleted: {
    type: Boolean,
    private: true,
    default: false
  }
}, {
  timestamps: true,
  toJSON: {virtuals: true},
  toObject: {virtuals: true}
});

schema.virtual('receivers', {
  ref: 'Receiver',
  localField: '_id',
  foreignField: 'request',
});

schema.pre('save', async function (next) {
  const doc = this;
  if (!doc.id) doc.id = await uniqueStringGenerator("iId", "ReportModel");
  next();
});

interface ReportDocument extends Document, Report {
}

const typesFindQueryMiddleware = [
  'countDocuments',
  'find',
  'findOne',
  'findOneAndDelete',
  'findOneAndRemove',
  'findOneAndUpdate',
  'update',
  'updateOne',
  // 'updateMany',
];

const excludeInFindQueriesIsDeleted = async function (this, next) {
  this.where({isDeleted: {$ne: true}});
  next();
};

typesFindQueryMiddleware.forEach((type) => {
  schema.pre(type, excludeInFindQueriesIsDeleted);
});

schema.plugin(paginate);
schema.plugin(toJSON);

export const ReportModel = mongoose.model<ReportDocument, PaginateModel<ReportDocument>>('Report', schema);
