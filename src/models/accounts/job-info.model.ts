import mongoose, { PaginateModel, Document } from "mongoose";
import { toJSON } from "../plugins";
import paginate from "mongoose-paginate-v2";
import { uniqueStringGenerator } from "@src/utils/slugify";

interface JobInfoDocument extends Document, JobInfo { }

const schema = new mongoose.Schema<JobInfo>({
	id: String,
	creator: {
		type: mongoose.Schema.Types.ObjectId,
		ref: "Account"
	},
  title: {
    type: String,
    required: true
  },
  slug: String,
  minSalary: Number,
  maxSalary: Number,
}, {
	timestamps: true,
	collection: "job-info"
});

schema.pre('save', async function (next) {
	const doc = this;
	if (!doc.id) doc.id = await uniqueStringGenerator("iId", "JobInfoModel");
	next();
});

schema.plugin(paginate);
schema.plugin(toJSON);

export const JobInfoModel = mongoose.model<JobInfoDocument, PaginateModel<JobInfoDocument>>('JobInfo', schema);
