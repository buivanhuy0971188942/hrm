import mongoose, {PaginateModel, Document} from "mongoose";
import {RoleModel} from "@src/models";
import {toJSON} from "@src/models/plugins";
import paginate from "mongoose-paginate-v2";
import {uniqueStringGenerator} from "@src/utils/slugify";

export interface AccountDocument extends Document, Account {
}

const schema = new mongoose.Schema<Account>({
  id: {
    type: String,
    required: true
  },
  avatar: {
    type: String,
    default: 'https://i.imgur.com/Uoeie1w.jpg'
  },
  fullName: String,
  slug: String,
  email: {
    type: String,
    trim: true,
    required: true
  },
  address: String,
  phone: String,
  status: String,
  role: {
    type: String,
    required: true
  },
  department: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Department"
  },
  jobInfo: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "JobInfo"
  },
  exp: {
    type: Number,
    default: 0,
  },
  stars: {
    type: Number,
    default: 0,
  },
  joinedDate: Number,
  leavedDate: Number,
  workType: String,
  workModel: String,
  cardId: String,
  telegramId: String,
  isTelegramConnected: {
    type: Boolean,
    default: false
  },
  gender: String,
  dob: Number,
  isVerified: {
    type: Boolean,
    default: false
  },
  activeEmail: String,
  emails: [String],
  identityNumber: String,
  gitLabId: String,
  gitLabAccessToken: String,
  isDeleted: {
    type: Boolean,
    default: false,
    private: true
  },
  gitLabRefreshToken: String,
  wakaId: String,
}, {
  timestamps: true,
  toJSON: {virtuals: true},
  toObject: {virtuals: true}
});

schema.virtual('bank-accounts', {
  ref: 'BankAccount',
  localField: '_id',
  foreignField: 'owner',
});

schema.pre('save', async function (next) {
  const user = this;
  if (!user.role) {
    let userRole = await RoleModel.findOne({slug: 'user'});
    if (!userRole) {
      console.log("Creating new role");
      userRole = await RoleModel.create({
        name: "User",
        slug: "user"
      });
    }
    user.role = userRole.slug;
  }
  if (user.isModified("fullName")) user.slug = await uniqueStringGenerator("slug", "AccountModel", user.fullName, "slug");
  next();
});

const typesFindQueryMiddleware = [
  'countDocuments',
  'find',
  'findOne',
  'findOneAndDelete',
  'findOneAndRemove',
  'update',
  'updateOne',
  // 'findOneAndUpdate',
  // 'updateMany',
];

const excludeInFindQueriesIsDeleted = async function (this, next) {
  this.where({isDeleted: {$ne: true}});
  next();
};

typesFindQueryMiddleware.forEach((type) => {
  schema.pre(type, excludeInFindQueriesIsDeleted);
});

// schema.pre('deleteOne', {document: true, query: true}, async function (next) {
//   const doc = this;
//   await DbService.deleteDocByFilter("RequestReceiverModel", {request: doc._id}, {many: true});
//   await DbService.deleteDocByFilter("TimelineEventModel", {request: doc._id}, {many: true});
//   const attachments = await DbService.getDocByFilter("AttachmentModel", {target: doc._id}, {
//     many: true,
//     nullable: true
//   });
//   for (const atm of attachments) await deleteFile(atm.path);
//   await DbService.deleteDocByFilter("AttachmentModel", {target: doc._id}, {many: true});
//   next();
// });

schema.plugin(paginate);
schema.plugin(toJSON);

export const AccountModel = mongoose.model<AccountDocument, PaginateModel<AccountDocument>>('Account', schema);
