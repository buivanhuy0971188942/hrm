import {uniqueStringGenerator} from '@src/utils/slugify';
import {toJSON} from '../plugins';
import mongoose, {Document} from "mongoose";
import paginate from "mongoose-paginate-v2";

interface RoleDocument extends Document, Role {
}

const schema = new mongoose.Schema<Role>({
  id: String,
  name: String,
  slug: String,
  level: Number,
  permissions: [String],
  default: {
    type: Boolean,
    default: false
  },
}, {
  timestamps: true
});

schema.pre("save", async function () {
  const doc = this;
  if (doc.isModified("name")) doc.slug = await uniqueStringGenerator("slug", "RoleModel", doc.name, "slug");
})

schema.plugin(paginate);
schema.plugin(toJSON);

export const RoleModel = mongoose.models.Role || mongoose.model<RoleDocument>('Role', schema);