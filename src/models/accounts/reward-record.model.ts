import mongoose, {PaginateModel, Document} from "mongoose";
import paginate from "mongoose-paginate-v2";
import {toJSON} from "@src/models/plugins";

interface RewardRecordDocument extends Document, RewardRecord {
}

const schema = new mongoose.Schema<RewardRecord>({
  // source: {
  //   type: mongoose.Schema.Types.ObjectId,
  //   ref: "Account"
  // },
  target: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Account"
  },
  type: String,
  amount: Number,
  timestamp: Number
}, {
  timestamps: true,
  toJSON: {virtuals: true},
  toObject: {virtuals: true}
});

schema.plugin(paginate);
schema.plugin(toJSON);

export const RewardRecordModel = mongoose.model<RewardRecordDocument, PaginateModel<RewardRecordDocument>>('RewardRecord', schema);
