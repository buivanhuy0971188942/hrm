import {uniqueStringGenerator} from '@src/utils/slugify';
import mongoose, {PaginateModel, Document} from "mongoose";
import {toJSON} from "../plugins";
import paginate from "mongoose-paginate-v2";

interface DepartmentDocument extends Document, Department {
}

const schema = new mongoose.Schema<Department>({
  id: String,
  name: String,
  slug: String,
  avatar: {
    type: String,
    default: "https://i.imgur.com/Uoeie1w.jpg"
  },
}, {
  timestamps: true,
  toJSON: {virtuals: true},
  toObject: {virtuals: true}
});

schema.virtual("members", {
  ref: "Account",
  localField: "_id",
  foreignField: "department"
});

schema.pre('save', async function (next) {
  const doc = this;
  if (!doc.slug) doc.slug = await uniqueStringGenerator("slug", "DepartmentModel", doc.name, "slug");
  if (!doc.id) doc.id = await uniqueStringGenerator("iId", "DepartmentModel");
  next();
});

schema.plugin(paginate);
schema.plugin(toJSON);

export const DepartmentModel = mongoose.model<DepartmentDocument, PaginateModel<DepartmentDocument>>('Department', schema);
