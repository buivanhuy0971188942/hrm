import mongoose, {Document} from "mongoose";

const schema = new mongoose.Schema({
  source: {
    type: mongoose.Types.ObjectId,
    required: true,
    ref: "Account"
  },
  target: {
    type: mongoose.Types.ObjectId,
    required: true,
    ref: "Account"
  },
});

export const FriendShipModel = mongoose.model('FriendShip', schema);