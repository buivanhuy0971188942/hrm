import mongoose, {PaginateModel, Document} from "mongoose";

export interface WakaConnection {

}

interface WakaConnectionDoc extends Document, WakaConnection {
}

const schema = new mongoose.Schema<WakaConnection>({
  owner: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Account"
  },
  expirationTime: Date,
  wakaId: String,
  redirectUri: String,
  grantType: String,
  token: String,
  refreshToken: String,
  scope: String,
  tokenType: String
}, {
  timestamps: true,
});

export const WakaConnectionModel = mongoose.model<WakaConnectionDoc, PaginateModel<WakaConnectionDoc>>('WakaConnection', schema);
