import mongoose, {PaginateModel} from "mongoose";
import {toJSON} from "../plugins";
import paginate from "mongoose-paginate-v2";
import {uniqueStringGenerator} from "@src/utils/slugify";

interface BankAccountDocument extends Document, BankAccountModel {
}

const schema = new mongoose.Schema<BankAccountModel>({
  id: String,
  owner: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Account",
    required: true
  },
  bankName: String,
  cardNumber: Number,
  accountNumber: Number,
  isActive: {
    type: Boolean,
    default: true
  },
}, {
  timestamps: true,
  collection: "bank-accounts",
});

schema.pre('save', async function (next) {
  const doc = this;
  if (!doc.id) doc.id = await uniqueStringGenerator("iId", "BankAccountModel");
  next();
});

schema.plugin(paginate);
schema.plugin(toJSON);

export const BankAccountModel = mongoose.model<BankAccountDocument, PaginateModel<BankAccountDocument>>('BankAccount', schema);
