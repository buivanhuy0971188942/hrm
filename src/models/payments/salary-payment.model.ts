import {uniqueStringGenerator} from '@src/utils/slugify';
import mongoose, {PaginateModel, Document} from "mongoose";
import {toJSON} from "../plugins";
import paginate from "mongoose-paginate-v2";

export interface SalaryPaymentDocument extends Document, SalaryPayment {
}

const schema = new mongoose.Schema<SalaryPayment>({
  id: String,
  employee: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Account"
  },
  grossSalary: Number,
  period: Date,
}, {
  collection: "salary-payments",
  timestamps: true,
  toJSON: {virtuals: true},
  toObject: {virtuals: true}
});

schema.virtual('adjustments', {
  ref: 'PaymentAdjustment',
  localField: '_id',
  foreignField: 'salaryPayment',
});

schema.pre('save', async function (next) {
  const doc = this;
  if (!doc.id) doc.id = await uniqueStringGenerator("iId", "SalaryPaymentModel");
  next();
});

schema.plugin(paginate);
schema.plugin(toJSON);

export const SalaryPaymentModel = mongoose.model<SalaryPaymentDocument, PaginateModel<SalaryPaymentDocument>>('SalaryPayment', schema);
