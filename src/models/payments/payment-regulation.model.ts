import { DbService } from '@src/services/DbService';
import mongoose, { PaginateModel, Document } from "mongoose";
import { toJSON } from "../plugins";
import paginate from "mongoose-paginate-v2";
import { uniqueStringGenerator } from "@src/utils/slugify";

interface PaymentRegulationDocument extends Document, PaymentRegulation { }

const schema = new mongoose.Schema<PaymentRegulation>({
	id: String,
	title: {
		type: String,
	},
	price: {
		type: Number,
	},
	description: {
		type: String,
	},
	type: {
		type: String,
		enum: ["BONUS", "PENALTY", 'ALLOWANCE', 'ATTENDANCE']
	},
}, {
	timestamps: { currentTime: () => Math.floor(Date.now()) },
	collection: "payment-regulations"
});

schema.pre('save', async function (next) {
	const doc = this;
	if (!doc.id) doc.id = await uniqueStringGenerator("iId", "PaymentRegulationModel");
	next();
});

schema.pre('deleteOne', { document: true, query: false }, async function (next) {
	const doc = this;
	// console.log(doc);
	await DbService.deleteDocByFilter("PaymentAdjustmentModel", { regulation: doc._id }, { many: true });
	next();
});

schema.plugin(paginate);
schema.plugin(toJSON);

export const PaymentRegulationModel = mongoose.model<PaymentRegulationDocument, PaginateModel<PaymentRegulationDocument>>('PaymentRegulation', schema);

