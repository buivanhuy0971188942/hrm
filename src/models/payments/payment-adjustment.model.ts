import mongoose, {PaginateModel, Document, ObjectId} from "mongoose";
import {toJSON} from "../plugins";
import paginate from "mongoose-paginate-v2";
import {uniqueStringGenerator} from "@src/utils/slugify";

export interface PaymentAdjustmentDocument extends Document, PaymentAdjustment {
}

const schema = new mongoose.Schema<PaymentAdjustment>({
  creator: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Account"
  },
  salaryPayment: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "SalaryPayment",
    required: true
  },
  regulation: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "PaymentRegulation"
  },
  amount: Number,
  description: String,
  workingHour: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "WorkingHour"
  },
  id: String,
}, {
  timestamps: true,
  collection: "payment-adjustments"
});

schema.pre('save', async function (next) {
  const doc = this;
  if (!doc.id) doc.id = await uniqueStringGenerator("iId", "PaymentAdjustmentModel");
  next();
});

schema.plugin(paginate);
schema.plugin(toJSON);

export const PaymentAdjustmentModel = mongoose.model<PaymentAdjustmentDocument>('PaymentAdjustment', schema);


