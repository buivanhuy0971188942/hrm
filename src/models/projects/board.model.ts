import {DbService} from '@src/services/DbService';
import mongoose, {Document} from "mongoose";
import paginate from "mongoose-paginate-v2";
import {uniqueStringGenerator} from '@src/utils/slugify';
import {toJSON} from "../plugins";

interface BoardDocument extends Document, Board {
}

const schema = new mongoose.Schema<Board>({
  id: String,
  creator: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Account"
  },
  name: {
    type: String,
    required: true,
    trim: true
  },
  index: Number,
  project: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Project"
  }
}, {
  timestamps: true,
  toJSON: {virtuals: true},
  toObject: {virtuals: true}
});

schema.virtual('tasks', {
  ref: 'Task',
  localField: '_id',
  foreignField: 'board',
});

schema.pre('save', async function (next) {
  const doc = this;
  if (!doc.id) doc.id = await uniqueStringGenerator("iId", "BoardModel");
  const maxIndex = (await BoardModel.findOne({}, null, {sort: "-index"}));
  if (!doc.index) doc.index = (maxIndex) ? ++maxIndex.index : 1;
  next();
});

schema.pre('deleteOne', {document: true, query: false}, async function (next) {
  const doc = this;
  await DbService.deleteDocByFilter("TaskModel", {board: doc._id}, {many: true});
  next();
});

schema.plugin(paginate);
schema.plugin(toJSON);

export const BoardModel = mongoose.model<BoardDocument>('Board', schema);
