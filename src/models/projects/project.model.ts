import mongoose, {Document} from "mongoose";
import paginate from "mongoose-paginate-v2";
import {uniqueStringGenerator} from '@src/utils/slugify';
import {toJSON} from "../plugins";
import {accessibleRecordsPlugin} from "@casl/mongoose";
import {DbService} from "@src/services/DbService";

interface ProjectDocument extends Document, Project {
}

const schema = new mongoose.Schema<Project>({
  id: String,
  name: {
    type: String,
    required: true,
  },
  creator: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Account"
  },
  image: String,
  description: String,
  dueDate: Date,
  slug: String,
  gitLabData: {
    projectId: String,
    namespace: String,
    webhookId: String,
    webUrl: String,
    projectName: String,
    sshUrl: String,
    httpUrl: String,
    createdAt: Date
  },
  isDeleted: {
    type: Boolean,
    private: true,
    default: false
  }
}, {
  timestamps: true,
  toJSON: {virtuals: true},
  toObject: {virtuals: true}
});

schema.virtual('members', {
  ref: 'ProjectMember',
  localField: '_id',
  foreignField: 'project',
});

schema.virtual('tasks', {
  ref: 'Task',
  localField: '_id',
  foreignField: 'project',
});

schema.virtual('labels', {
  ref: 'Label',
  localField: '_id',
  foreignField: 'project',
});

schema.virtual('issues', {
  ref: 'Issue',
  localField: '_id',
  foreignField: 'project',
});

schema.pre('save', async function (next) {
  const doc = this;
  if (doc.isModified("name")) doc.slug = await uniqueStringGenerator("slug", "ProjectModel", doc.name, "slug");
  if (!doc.id) doc.id = await uniqueStringGenerator("iId", "ProjectModel");

  next();
});

schema.pre('deleteOne', {document: true, query: false}, async function (next) {
  const doc = this;
  await Promise.all([
    DbService.deleteDocByFilter("TaskModel", {project: doc._id}, {many: true}),
    DbService.deleteDocByFilter("BoardModel", {project: doc._id}, {many: true}),
    DbService.deleteDocByFilter("LabelModel", {project: doc._id}, {many: true}),
    DbService.deleteDocByFilter("IssueModel", {project: doc._id}, {many: true}),
    DbService.deleteDocByFilter("ProjectMemberModel", {project: doc._id}, {many: true}),
  ]);
  next();
});

const typesFindQueryMiddleware = [
  'countDocuments',
  'find',
  'findOne',
  'findOneAndDelete',
  'findOneAndRemove',
  'findOneAndUpdate',
  'update',
  'updateOne',
  // 'updateMany',
];

const excludeInFindQueriesIsDeleted = async function (this, next) {
  this.where({isDeleted: {$ne: true}});
  next();
};

typesFindQueryMiddleware.forEach((type) => {
  schema.pre(type, excludeInFindQueriesIsDeleted);
});

schema.plugin(paginate);
schema.plugin(toJSON);
schema.plugin(accessibleRecordsPlugin);

export const ProjectModel = mongoose.model<ProjectDocument>('Project', schema);
