import mongoose, {Document} from "mongoose";
import paginate from "mongoose-paginate-v2";
import {uniqueStringGenerator} from '@src/utils/slugify';
import {toJSON} from "../plugins";

interface ProjectMemberDocument extends Document, ProjectMember {
}

const schema = new mongoose.Schema<ProjectMember>({
  id: String,
  member: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Account",
  },
  project: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Project",
  },
  role: {
    type: String,
    enum: ["MAINTAINER", "MEMBER"],
    default: "MEMBER"
  }
}, {
  collection: "project-members",
  timestamps: true
});

schema.pre('save', async function (next) {
  const doc = this;
  if (!doc.id) doc.id = await uniqueStringGenerator("iId", "ProjectMemberModel");

  next();
});

schema.plugin(paginate);
schema.plugin(toJSON);

export const ProjectMemberModel = mongoose.model<ProjectMemberDocument>('ProjectMember', schema);
