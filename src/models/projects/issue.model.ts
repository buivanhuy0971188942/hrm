import mongoose, {Document} from "mongoose";
import paginate from "mongoose-paginate-v2";
import {toJSON} from "../plugins";

interface IssueDocument extends Document, Issue {
}

const schema = new mongoose.Schema<Issue>({
  id: String,
  iid: String,
  project: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Project"
  },
  gitLabProjectId: String,
  title: String,
  description: String,
  state: String,
  createdTimeOnGitLab: Date,
  closedAt: Date,
  labels: [],
}, {
  timestamps: true
});

schema.pre('save', async function (next) {
  const doc = this;
  next();
});

schema.plugin(paginate);
schema.plugin(toJSON);

export const IssueModel = mongoose.model<IssueDocument>('Issue', schema);
