import mongoose, {PaginateModel, Document} from "mongoose";
import {toJSON} from "../plugins";
import paginate from "mongoose-paginate-v2";
import {uniqueStringGenerator} from "@src/utils/slugify";

interface NotificationDocument extends Document, Notification {
}

const schema = new mongoose.Schema<Notification>({
  id: String,
  sender: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Account",
  },
  receiver: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Account",
    required: true
  },
  body: String,
  title: String,
  isDeleted: {
    type: Boolean,
    private: true,
    default: true
  },
  action: String,
}, {
  timestamps: true,
});

schema.pre('save', async function (next) {
  const doc = this;
  if (!doc.id) doc.id = await uniqueStringGenerator("iId", "NotificationModel");
  next();
});

const typesFindQueryMiddleware = [
  'countDocuments',
  'find',
  'findOne',
  'findOneAndDelete',
  'findOneAndRemove',
  'findOneAndUpdate',
  'update',
  'updateOne',
  // 'updateMany',
];

const excludeInFindQueriesIsDeleted = async function (this, next) {
  this.where({isDeleted: {$ne: true}});
  next();
};

typesFindQueryMiddleware.forEach((type) => {
  schema.pre(type, excludeInFindQueriesIsDeleted);
});

schema.plugin(paginate);
schema.plugin(toJSON);

export const NotificationModel = mongoose.model<NotificationDocument, PaginateModel<NotificationDocument>>('Notification', schema);
