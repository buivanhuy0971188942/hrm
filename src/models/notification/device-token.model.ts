import mongoose, {PaginateModel, Document} from "mongoose";
import {toJSON} from "../plugins";
import paginate from "mongoose-paginate-v2";
import {uniqueStringGenerator} from "@src/utils/slugify";

interface DeviceTokenDocument extends Document, DeviceToken {
}

const schema = new mongoose.Schema<DeviceToken>({
  id: String,
  account: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Account",
  },
  tokens: [String],
}, {
  timestamps: true,
});

schema.pre('save', async function (next) {
  const doc = this;
  if (!doc.id) doc.id = await uniqueStringGenerator("iId", "DeviceTokenModel");
  next();
});

schema.plugin(paginate);
schema.plugin(toJSON);

export const DeviceTokenModel = mongoose.model<DeviceTokenDocument, PaginateModel<DeviceTokenDocument>>('DeviceToken', schema);
