import {toJSON} from '../plugins';
import paginate from 'mongoose-paginate-v2';
import {uniqueStringGenerator} from "@src/utils/slugify";
import mongoose, {Document} from "mongoose";

interface GroupDocument extends Document, Group {
}

const schema = new mongoose.Schema<Group>({
  id: String,
  creator: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Account"
  },
  name: String,
  slug: String,
  avatar: {
    type: String,
    default: "https://i.imgur.com/Uoeie1w.jpg"
  },
  description: String,
  coverPicture: String,
  privacy: {
    type: String,
    enum: ["PUBLIC", "PRIVATE"],
    default: "PUBLIC"
  }
}, {
  timestamps: true,
  toJSON: {virtuals: true},
  toObject: {virtuals: true},
});

schema.virtual("members", {
  ref: "GroupMember",
  foreignField: "group",
  localField: "_id"
})

schema.virtual("posts", {
  ref: "Post",
  foreignField: "group",
  localField: "_id"
})

schema.pre('save', async function (next) {
  const doc = this;
  if (!doc.id) doc.id = await uniqueStringGenerator("iId", "GroupModel");
  if (doc.isModified("name")) doc.slug = await uniqueStringGenerator("slug", "GroupModel", doc.name, "slug");
  next();
});

schema.plugin(paginate);
schema.plugin(toJSON)

export const GroupModel = mongoose.model<GroupDocument>('Group', schema);