import {toJSON} from '../plugins';
import paginate from 'mongoose-paginate-v2';
import {uniqueStringGenerator} from "@src/utils/slugify";
import mongoose, {Document} from "mongoose";

interface GroupFileDocument extends Document, GroupFile {
}

const schema = new mongoose.Schema<GroupFile>({
  id: String,
  isFolder: String,
  level: {
    type: Number,
    default: 0
  },
  creator: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Account"
  },
  target: {
    type: mongoose.Schema.Types.ObjectId,
  },
  fileName: {
    type: String
  },
  fileType: {
    type: String,
    enum: ['doc', 'sound', 'image', 'video', 'link'],
    required: true
  },
  path: String,
  size: String,
  metadata: mongoose.Schema.Types.Mixed
}, {
  timestamps: true,
  toJSON: {virtuals: true},
  toObject: {virtuals: true},
});

schema.pre('save', async function (next) {
  const doc = this;
  if (!doc.id) doc.id = await uniqueStringGenerator("iId", "GroupFileModel");
  next();
});

schema.plugin(paginate);
schema.plugin(toJSON)

export const GroupFileModel = mongoose.model<GroupFileDocument>('GroupFile', schema);