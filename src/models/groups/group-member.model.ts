import mongoose, {PaginateModel, Document} from "mongoose";
import paginate from 'mongoose-paginate-v2';
import {toJSON} from "@src/models/plugins";

interface GroupParticipantDocument extends Document, GroupMember {
}

const schema = new mongoose.Schema<GroupMember>({
  member: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Account"
  },
  group: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Group"
  },
  role: {
    type: String,
    enum: ["ADMIN", "MEMBER"],
    default: "MEMBER"
  }
}, {
  timestamps: true,
  collection: "group-members"
});

schema.plugin(paginate);
schema.plugin(toJSON);

export const GroupMemberModel = mongoose.model<GroupParticipantDocument, PaginateModel<GroupParticipantDocument>>('GroupMember', schema);
