import Joi from 'joi';
import {pattern} from '@src/config/validation.config';

export const addGroupValidation = {
  body: Joi.object({
    name: Joi.string().required(),
    description: Joi.string(),
    privacy: Joi.string().allow("PUBLIC", "PRIVATE"),
    members: Joi.alternatives().try(Joi.string().custom(pattern.ObjectId), Joi.array().items(Joi.string().custom(pattern.ObjectId)))
    // coverPicture: Joi.string(),
  })
}

export const updateGroupValidation = {
  body: Joi.object({
    name: Joi.string(),
    description: Joi.string(),
    privacy: Joi.string().allow("PUBLIC", "PRIVATE")
  })
}

export const addGroupPostValidation = {
  body: Joi.object({
    content: Joi.string().required().required(),
  })
}
export const updateGroupPostValidation = {
  body: Joi.object({
    content: Joi.string().required(),
  })
}

export const addGroupMemberValidation = {
  body: Joi.object({
    members: Joi.array().items(Joi.string().custom(pattern.ObjectId).required()),
  })
}

export const groupMemberValidation = {
  body: Joi.object({
    member: Joi.string().custom(pattern.ObjectId).required(),
    role: Joi.string().allow("ADMIN", "MEMBER"),
    authorizedMember: Joi.string().custom(pattern.ObjectId)
  })
}