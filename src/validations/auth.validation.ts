import {pattern} from '@src/config/validation.config';
import Joi from 'joi';

export const getTokens = {
  body: Joi.object({
    grant_type: Joi.string().allow("authorization_code").required(),
    code: Joi.string().required(),
    redirect_uri: Joi.string().required(),
    device_token: Joi.string(),
  })
}
