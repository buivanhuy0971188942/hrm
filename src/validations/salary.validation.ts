import Joi from 'joi';
import { pattern } from '@src/config/validation.config';

export const addSalaryPaymentValidation = {
  body: Joi.object({
    employee: Joi.string().custom(pattern.ObjectId).required(),
    grossSalary: Joi.number().required(),
    period: Joi.date(),
  })
}

export const updateSalaryPaymentValidation = {
  body: Joi.object({
    grossSalary: Joi.number(),
    period: Joi.date(),
  })
}

export const addPaymentAdjustmentValidation = {
  body: Joi.object({
    regulation: Joi.string().custom(pattern.ObjectId).required(),
    amount: Joi.number().required(),
    description: Joi.string().required(),
  })
}


export const updatePaymentAdjustmentValidation = {
  body: Joi.object({
    regulation: Joi.string().custom(pattern.ObjectId),
    amount: Joi.number().required(),
    description: Joi.string().required(),
  })
}

export const updateWorkingHourValidation = {
  body: Joi.object({})
}