import Joi from 'joi';
import {pattern} from "@src/config/validation.config";

export const addPostValidation = {
  body: Joi.object({
    title: Joi.string().required(),
    content: Joi.string().required(),
    feed: Joi.string(),
    source: Joi.string(),
    state: Joi.string().allow("OPENED", "CLOSED"),
    description: Joi.string(),
    categories: Joi.alternatives().try(Joi.string().custom(pattern.ObjectId), Joi.array().items(Joi.string().custom(pattern.ObjectId))),
  })
}

export const updatePostValidation = {
  body: Joi.object({
    title: Joi.string(),
    content: Joi.string(),
    feed: Joi.string(),
    source: Joi.string(),
    description: Joi.string(),
    categories: Joi.alternatives().try(Joi.string().custom(pattern.ObjectId), Joi.array().items(Joi.string().custom(pattern.ObjectId))),
    state: Joi.string().allow("OPENED", "CLOSED"),
    attachments: Joi.array().items(Joi.string()),
  })
}