import { pattern } from '@src/config/validation.config';
import Joi from 'joi';

export const addBoardValidation = {
  body: Joi.object({
    name: Joi.string().required(),
  })
}

export const updateBoardValidation = {
  body: Joi.object({
    name: Joi.string()
  })
}

export const updateBoardIndexValidation = {
  body: Joi.object({
    index: Joi.array().items({
      board: Joi.string().custom(pattern.ObjectId).required(),
      index: Joi.number().required(),
    })
  })
}
