import {pattern} from '@src/config/validation.config';
import Joi from 'joi';

export const updateAccountValidation = {
  body: Joi.object({
    avatar: Joi.string(),
    fullName: Joi.string(),
    address: Joi.string(),
    gender: Joi.string(),
    dob: Joi.string(),
    phone: Joi.string(),
    status: Joi.string(),
    role: Joi.string(),
    department: Joi.string().custom(pattern.ObjectId),
    jobInfo: Joi.string().custom(pattern.ObjectId),
    joinedDate: Joi.number(),
    leavedDate: Joi.number(),
    workType: Joi.string(),
    workModel: Joi.string(),
    cardId: Joi.string(),
    identityNumber: Joi.string(),
    activeEmail: Joi.string(),
    emails: Joi.array().items(Joi.string()),
    // id: Joi.string()
  })
}

export const addJobInfoValidation = {
  body: Joi.object({
    title: Joi.string().required(),
    minSalary: Joi.number(),
    maxSalary: Joi.number(),
  })
}

export const takeAttendanceValidation = {
  body: Joi.object({
    startTime: Joi.number().required(),
    endTime: Joi.number(),
    withPermission: Joi.boolean(),
    shift: Joi.string().required(),
    note: Joi.string(),
  })
}
