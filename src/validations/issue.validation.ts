import Joi from 'joi';

export const addIssueValidation = {
  body: Joi.object({
    title: Joi.string().required(),
    description: Joi.string(),
  })
}

export const updateIssueValidation = {
  body: Joi.object({
    title: Joi.string(),
    description: Joi.string(),
    state_event: Joi.string().valid('close','reopen'),
  })
}
