import Joi from 'joi';
import { pattern } from '@src/config/validation.config';

export const addWorkingHourValidation = {
  body: Joi.object({
    employee: Joi.string().custom(pattern.ObjectId),
    startTime: Joi.date,
    endTime: Joi.date,
  })
}

export const updateWorkingHourValidation = {
  body: Joi.object({

  })
}