import {pattern} from './../config/validation.config';
import Joi from 'joi';

export const addProjectValidation = {
  body: Joi.object({
    name: Joi.string().required(),
    type: Joi.string(),
    description: Joi.string(),
    dueDate: Joi.string()
  })
}

export const linkProjectToGitLabValidation = {
  body: Joi.object({
    projectId: Joi.string().required(),
    namespace: Joi.string().required(),
    addHook: Joi.boolean(),
    web_url: Joi.string(),
    name: Joi.string(),
    ssh_url_to_repo: Joi.string(),
    http_url_to_repo: Joi.string(),
    created_at: Joi.string(),
  })
}
export const disconnectProjectFromGitLabValidation = {
  body: Joi.object({
    allowDeleteTasks: Joi.boolean(),
  })
}

export const updateProjectValidation = {
  body: Joi.object({
    name: Joi.string(),
    type: Joi.string(),
    description: Joi.string(),
    dueDate: Joi.string(),
    gitLabData: Joi.object({
      projectId: Joi.string(),
      namespace: Joi.string(),
      gitUrl: Joi.string(),
    })
  })
}

export const addMemberValidation = {
  body: Joi.object({
    member: Joi.string().custom(pattern.ObjectId).required(),
    role: Joi.string(),
  })
}

export const updateMemberValidation = {
  body: Joi.object({
    member: Joi.string().custom(pattern.ObjectId).required(),
    role: Joi.string(),
  })
}

export const deleteMemberValidation = {
  body: Joi.object({
    member: Joi.string().custom(pattern.ObjectId).required(),
    authorizedMember: Joi.string().custom(pattern.ObjectId),
  })
}