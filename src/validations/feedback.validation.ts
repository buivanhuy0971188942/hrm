import { pattern } from '@src/config/validation.config';
import Joi from 'joi';

export const addFeedbackValidation = {
  body: Joi.object({
    note: Joi.string(),
    overallRating: Joi.string(),
    feedbacks: Joi.array().items(
      Joi.object({
        criteria: Joi.string().custom(pattern.ObjectId).required(),
        rating: Joi.number().required(),
        note: Joi.string()
      }))
  })
}

export const updateFeedbackValidation = {
  body: Joi.object({
    note: Joi.string(),
    overallRating: Joi.string(),
    feedbacks: Joi.array().items(
      Joi.object({
        criteria: Joi.string().custom(pattern.ObjectId),
        rating: Joi.number(),
      }))
  })
}