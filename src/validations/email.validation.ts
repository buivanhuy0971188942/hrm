import { pattern } from '@src/config/validation.config';
import Joi from 'joi';

export const addEmailAddressesValidation = {
  body: Joi.object({
    account: Joi.string().custom(pattern.ObjectId).required(),
    emails: Joi.array().items(Joi.string())
  })
}

export const sendEmailValidation = {
  body: Joi.object({
    from: Joi.alternatives(
      Joi.string(),
      Joi.array().items(Joi.string())
    ),
    to: Joi.alternatives(Joi.string(), Joi.array().items(Joi.string()).min(1)).required(),
    cc: Joi.alternatives(Joi.string(), Joi.array().items(Joi.string()).min(1)),
    bcc: Joi.alternatives(Joi.string(), Joi.array().items(Joi.string()).min(1)),
    subject: Joi.string(),
    text: Joi.string(),
    html: Joi.string(),
  })
}
export const updateEmailValidation = {
  body: Joi.object({
    isDeleted: Joi.boolean(),
    isStarred: Joi.boolean(),
  })
}