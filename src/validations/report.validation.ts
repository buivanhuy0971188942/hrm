import Joi from 'joi';

export const addReportValidation = {
  body: Joi.object({
    content: Joi.string().required(),
    title: Joi.string(),
    feedback: Joi.string(),
  })
}