import Joi from 'joi';

export const addPostCategoryValidation = {
  body: Joi.object({
    name: Joi.string().required(),
    hashtag: Joi.string(),
    description: Joi.string(),
  })
}

export const updatePostCategoryValidation = {
  body: Joi.object({
    name: Joi.string(),
    hashtag: Joi.string(),
    description: Joi.string(),
  })
}