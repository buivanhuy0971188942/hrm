import Joi from 'joi';

export const addProductValidation = {
  body: Joi.object({
    name: Joi.string().required(),
    description: Joi.string(),
    category: Joi.string(),
    downLoadCount: Joi.string(),
    CHPlayUrl: Joi.string(),
    AppStoreUrl: Joi.string(),
  })
}

export const updateProductValidation = {
  body: Joi.object({
    name: Joi.string(),
    description: Joi.string(),
    category: Joi.string(),
    downLoadCount: Joi.string(),
    CHPlayUrl: Joi.string(),
    AppStoreUrl: Joi.string(),
  })
}
