import {pattern} from '@src/config/validation.config';
import Joi from 'joi';

export const addEventValidation = {
  body: Joi.object({
    startTime: Joi.date().required(),
    endTime: Joi.date().required(),
    title: Joi.string().required(),
    privacy: Joi.string(),
    type: Joi.string().allow("PERSONAL-EVENT", "HOLIDAY", "COMPANY-EVENT"),
    description: Joi.string(),
    isRecurrent: Joi.boolean(),
    recurrenceType: Joi.string().allow("DAILY", "WEEKLY", "MONTHLY", "YEARLY"),
  })
}

export const updateEventValidation = {
  body: Joi.object({
    ownerId: Joi.number(),
    startTime: Joi.date(),
    endTime: Joi.date(),
    title: Joi.string(),
    type: Joi.string().allow("PERSONAL-EVENT", "HOLIDAY", "COMPANY-EVENT"),
    description: Joi.string(),
    isRecurrent: Joi.boolean(),
    recurrenceType: Joi.string().allow("DAILY", "WEEKLY", "MONTHLY", "YEARLY"),
  })
}
