import Joi from 'joi';
import {pattern} from "@src/config/validation.config";

export const addRequestValidation = {
  body: Joi.object({
    department: Joi.string().custom(pattern.ObjectId),
    content: Joi.string().required(),
    title: Joi.string().required(),
    type: Joi.string().required().allow("STUDY", "OVERTIME", "ABSENCE", "OTHER"),
    time: Joi.alternatives().try(
      Joi.string(),
      Joi.array().items(Joi.string())
    ),
    tasks: Joi.alternatives().try(
      Joi.string().custom(pattern.ObjectId),
      Joi.array().items(Joi.string().custom(pattern.ObjectId))
    ),
    receivers: Joi.alternatives().try(
      Joi.string().custom(pattern.ObjectId),
      Joi.array().items(Joi.string().custom(pattern.ObjectId))
    ),
  })
}

export const updateRequestValidation = {
  body: Joi.object({
    department: Joi.string().custom(pattern.ObjectId),
    content: Joi.string(),
    title: Joi.string(),
    type: Joi.string().required().allow("STUDY", "OVERTIME", "ABSENCE", "OTHER"),
    time: Joi.alternatives().try(
      Joi.string(),
      Joi.array().items(Joi.string())
    ),
    tasks: Joi.alternatives().try(
      Joi.string().custom(pattern.ObjectId),
      Joi.array().items(Joi.string().custom(pattern.ObjectId))
    ),
    receivers: Joi.alternatives().try(
      Joi.string().custom(pattern.ObjectId),
      Joi.array().items(Joi.string().custom(pattern.ObjectId))
    ),
  })
}

export const getRequestByPlaceHolder = {
  params: Joi.object({
    placeholder: Joi.string().allow("sent", "inbox"),
  })
}
export const responseToRequestValidation = {
  body: Joi.object({
    status: Joi.string(),
    response: Joi.string(),
  })
}