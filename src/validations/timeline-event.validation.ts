import {pattern} from '@src/config/validation.config';
import Joi from 'joi';

export const addEventValidation = {
  body: Joi.object({
    startTime: Joi.number().required(),
    endTime: Joi.number().required(),
    type: Joi.string().allow("STUDY", "OVERTIME", "WORKING"),
    note: Joi.string(),
  })
}

export const updateEventValidation = {
  body: Joi.object({
    startTime: Joi.number(),
    endTime: Joi.number(),
    type: Joi.string().allow("STUDY", "OVERTIME", "WORKING"),
    note: Joi.string(),
  })
}
