import Joi from 'joi';

export const addLabelValidation = {
  body: Joi.object({
    name: Joi.string().required(),
    color: Joi.string().required()
  })
}

export const updateLabelValidation = {
  body: Joi.object({
    name: Joi.string(),
    color: Joi.string()
  })
}
