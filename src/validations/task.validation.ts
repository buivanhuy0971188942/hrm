import {pattern} from '@src/config/validation.config';
import Joi from 'joi';

export const addTaskValidation = {
  body: Joi.object({
    name: Joi.string().required(),
    dueDate: Joi.number(),
    assignedTo: Joi.string(),
    project: Joi.string(),
    labels: Joi.array().items(Joi.string().custom(pattern.ObjectId)),
    cover: Joi.string(),
    description: Joi.string(),
    attachment: Joi.array().items(Joi.string().custom(pattern.ObjectId)),
  })
}

export const updateTaskValidation = {
  body: Joi.object({
    name: Joi.string(),
    dueDate: Joi.number(),
    assignedTo: Joi.string(),
    labels: Joi.array().items(Joi.string().custom(pattern.ObjectId)),
    cover: Joi.string(),
    description: Joi.string(),
    attachment: Joi.array().items(Joi.string().custom(pattern.ObjectId)),
    issue: Joi.string(),
    state: Joi.string().allow("TODO", "IN_PROGRESS", "REVIEWING", "COMPLETED")
  })
}

export const updateTaskIndexValidation = {
  body: Joi.object({
    beforeBoard: Joi.string().custom(pattern.ObjectId).required(),
    afterBoard: Joi.string().custom(pattern.ObjectId).required(),
    task: Joi.string().custom(pattern.ObjectId).required(),
    index: Joi.number().required(),
  })
}
