import Joi from 'joi';

export const addPaymentRegulationValidation = {
  body: Joi.object({
    title: Joi.string().required(),
    price: Joi.number(),
    description: Joi.string(),
    content: Joi.string(),
    type: Joi.string().required(),
  })
}

export const addAttendanceRegulationValidation = {
  body: Joi.object({
    name: Joi.string().required(),
    price: Joi.number().required(),
    type: Joi.string().required(),
    attendance: Joi.object({
      timeLimit: Joi.string().required(),
      amount: Joi.string().required(),
    })
  })
}

export const updatePaymentRegulationValidation = {
  body: Joi.object({
    title: Joi.string(),
    price: Joi.number(),
    description: Joi.string(),
    content: Joi.string(),
    type: Joi.string(),
  })
}