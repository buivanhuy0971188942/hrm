import Joi from 'joi';
import {pattern} from "@src/config/validation.config";

export const sendNotificationValidation = {
  body: Joi.object({
    receivers: Joi.alternatives(Joi.string().allow("all"), Joi.array().items(Joi.string().custom(pattern.ObjectId)).min(1)).required(),
    title: Joi.string().required(),
    body: Joi.string().required(),
    action: Joi.string(),
  })
}

export const addDeviceTokenValidation = {
  body: Joi.object({
    tokens: Joi.array().items(Joi.string()).min(1),
  })
}