import Joi from 'joi';

export const addCriteriaValidation = {
  body: Joi.object({
    name: Joi.string().required(),
  })
}

export const updateCriteriaValidation = {
  body: Joi.object({
    name: Joi.string(),
  })
}