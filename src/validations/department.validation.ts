import Joi from 'joi';

export const addDepartmentValidation = {
  body: Joi.object({
    name: Joi.string().required(),
  })
}

export const updateDepartmentValidation = {
  body: Joi.object({
    name: Joi.string(),
  })
}
