import {pattern} from '@src/config/validation.config';
import Joi from 'joi';

export const addGeneralTaskValidation = {
  body: Joi.object({
    name: Joi.string().required(),
    startDate: Joi.string(),
    dueDate: Joi.string(),
    state: Joi.string().allow("TODO", "IN_PROGRESS", "REVIEWING", "COMPLETED"),
    members: Joi.alternatives().try(Joi.string(), Joi.array().items(Joi.string().custom(pattern.ObjectId))),
    description: Joi.string(),
  })
}

export const updateGeneralTaskValidation = {
  body: Joi.object({
    name: Joi.string(),
    startDate: Joi.string(),
    dueDate: Joi.string(),
    state: Joi.string().allow("TODO", "IN_PROGRESS", "REVIEWING", "COMPLETED"),
    members: Joi.alternatives().try(Joi.string(), Joi.array().items(Joi.string().custom(pattern.ObjectId))),
    description: Joi.string(),
  })
}

export const deleteAttachmentValidation = {
  body: Joi.object({
    attachment: Joi.string().custom(pattern.ObjectId),
  })
}
export const deleteTaskMemberValidation = {
  body: Joi.object({
    member: Joi.string().custom(pattern.ObjectId),
  })
}

export const addSubTask = {
  body: Joi.object({
    subTask: Joi.string()
  })
}